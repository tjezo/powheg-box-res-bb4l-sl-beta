c  -*- Fortran -*-
      write(*,*) "************************            AND             ************************"
      write(*,*) "****                                                                    ****"
      write(*,*) "**** The implementation of this process is discussed in                 ****"
      write(*,*) "****                                                                    ****"
      write(*,*) "****      Tomas Jezo, Jonas M. Lindert, Paolo Nason, Carlo Oleari,      ****"
      write(*,*) "****                Stefano Pozzorini, arXiv:1607.04538.                ****"
      write(*,*) "****                                                                    ****"
