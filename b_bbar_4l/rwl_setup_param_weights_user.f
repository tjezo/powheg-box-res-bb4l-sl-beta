      subroutine rwl_setup_params_weights_user(count)
!     Call set_parameter (openloop function) to vary tmass and twidth
!     in the computations of the matrix elements
      use openloops, only: set_parameter

      implicit none
      integer count
      integer, save :: old_pdf1,old_pdf2,old_facfact,old_renfact
      include 'pwhg_rwl.h'
      include 'pwhg_pdf.h'
      include 'pwhg_flg.h'
      include 'pwhg_st.h'
      include 'pwhg_physpar.h'
      include 'PhysPars.h'
      logical rwl_keypresent
      real * 8 val
      character * 5 scheme
      integer iorder,iret
      logical, save :: for_reweighting,novirtual
      real * 8 powheginput
      real * 8, save :: topmass
      real * 8 pwhg_alphas
      external pwhg_alphas
      real * 8 lotwidth, nlotwidth
      real * 8, save :: twidth
      logical fourToFiveMatching_orig,fourToFiveMatchingAlphaS_orig
      integer st_nlight_default_orig, st_nlight_as_orig
      integer :: width_correction_orig
      integer :: width_correction_flg
      common/width_correction_common/width_correction_flg
      include 'FourToFiveMatching.h'

      if(count==0) then
c-----Store the initial settings
         for_reweighting = flg_for_reweighting
         novirtual = flg_novirtual
         topmass = ph_tmass
         twidth  = ph_twidth  
         fourToFiveMatching_orig = fourToFiveMatching
         fourToFiveMatchingAlphaS_orig = fourToFiveMatchingAlphaS
         st_nlight_default_orig = st_nlight_default
         st_nlight_as_orig = st_nlight_as
         width_correction_orig = width_correction_flg
         print*,'Storing originals:'
         print*,'fourToFiveMatching_orig =',fourToFiveMatching_orig
         print*,'fourToFiveMatchingAlphaS_orig =',fourToFiveMatchingAlphaS_orig
         print*,'st_nlight_default_orig =',st_nlight_default_orig
         print*,'st_nlight_as_orig =',st_nlight_as_orig
         print*,'width_correction_orig =',width_correction_orig
      elseif(count == -1) then
c-----Restore the initial settings
         flg_for_reweighting = for_reweighting
         flg_novirtual = novirtual
         ph_tmass = topmass
         ph_twidth= twidth
         fourToFiveMatching = fourToFiveMatching_orig
         fourToFiveMatchingAlphaS = fourToFiveMatchingAlphaS_orig
         st_nlight_default = st_nlight_default_orig
         st_nlight = st_nlight_default
         st_nlight_as = st_nlight_as_orig
         width_correction_flg  = width_correction_orig
         print*,'Restoring originals:'
         print*,'fourToFiveMatching =',fourToFiveMatching
         print*,'fourToFiveMatchingAlphaS =',fourToFiveMatchingAlphaS
         print*,'st_nlight_default =',st_nlight_default
         print*,'st_nlight_as =',st_nlight_as
         print*,'width_correction_flg =',width_correction_flg
         call set_parameter("minnf_alphasrun", st_nlight_as)
c-----Do it also for openloops params
         call set_parameter("t_mass",  ph_tmass)
         call set_parameter("t_width", ph_twidth)
         call setpdgmasses
      else
         flg_for_reweighting = .false.
         flg_novirtual = .false.
c--------Set ph_tmass equal to the new read value and compute the 
c--------corresponding width.
         if(rwl_keypresent(count,'tmass',val)) then
            ph_tmass = val
            call set_parameter("t_mass", ph_tmass)
         endif
         if(rwl_keypresent(count,'tmass',val) .or.
     1        rwl_keypresent(count,'lhapdf',val)) then
c     Alpha_QCD might have changed, recompute top width
            call settopwidth
            call setpdgmasses
         endif
         if(rwl_keypresent(count,'fourToFiveMatch',val)) then
            fourToFiveMatching=(val.eq.1)
         endif
         if(rwl_keypresent(count,'fourToFiveMatchAS',val)) then
            fourToFiveMatchingAlphaS=(val.eq.1)
         endif
         if(rwl_keypresent(count,'st_nlight_default',val)) then
            st_nlight_default=val
            if (st_nlight_default.ne.4.and.st_nlight_default.ne.5) st_nlight_default=5
            st_nlight=st_nlight_default
         endif
         if(rwl_keypresent(count,'st_nlight_as',val)) then
            st_nlight_as=val
            if (st_nlight_as.ne.4.and.st_nlight_as.ne.5) st_nlight_as=5
            call set_parameter("minnf_alphasrun", st_nlight_as)
         endif
         if(rwl_keypresent(count,'st_nlight_ssv',val)) then
            st_nlight_ssv=val
            if (st_nlight_ssv.ne.4.and.st_nlight_ssv.ne.5) st_nlight_ssv=5
         endif
         if(rwl_keypresent(count,'width_correction',val)) then
            width_correction_flg=val
         endif
         print*,'New values for count=',count
         print*,'fourToFiveMatching =',fourToFiveMatching
         print*,'fourToFiveMatchingAlphaS =',fourToFiveMatchingAlphaS
         print*,'st_nlight_default =',st_nlight_default
         print*,'st_nlight_as =',st_nlight_as
         print*,'width_correction_flg =',width_correction_flg
       endif
      contains
      subroutine settopwidth
      call topwidth(ph_bmass,ph_tmass,ph_Wmass,ph_Zmass,ph_Wwidth,
     1     ph_gfermi,ph_alphaem,pwhg_alphas(ph_tmass**2,
     1     st_lambda5MSB,-1),lotwidth,nlotwidth)
      if (flg_bornonly) then
         ph_twidth=lotwidth
      else
         ph_twidth=nlotwidth
      end if
c-----Do it also for openloops params
      call set_parameter("t_width", ph_twidth) !ph_twidth have already been set     
      end subroutine
      subroutine setpdgmasses
c     the pdg masses are used by the resweight.f system, to compute the
c     factors for the separatin of the resonance regions.
      physpar_pdgmasses(6) = ph_tmass
      physpar_pdgmasses(-6) = ph_tmass
      physpar_pdgwidths(6) = ph_twidth
      physpar_pdgwidths(-6) = ph_twidth
      end subroutine
      end
