#include "Pythia8/Pythia.h"
#include "PowhegHooksBB4L.h"
using namespace Pythia8;

//==========================================================================

int main() {

  // Generator
  Pythia pythia;

  // Add PowhegHooksBB4L custom flags
  // onlyDistance1: this setting allows to veto only shower first emission (as is the case in b_bbar_4l/main-PYTHIA82-lhef)
  //   in principle all the emissions should be vetoed on (which is the current default), the difference between the two seems negligible
  pythia.settings.addFlag("POWHEG:bb4l:onlyDistance1", false);
  // useScaleResonanceInstead: false ... equivalent to `pythiaveto 0` option of b_bbar_4l/main-PYTHIA82-lhef
  //                           true  ... equivalent to `pythiaveto 1` option of b_bbar_4l/main-PYTHIA82-lhef 
  //   the difference between the two is summarized in Figure 19 of arXiv:1607.04538 
  pythia.settings.addFlag("POWHEG:bb4l:useScaleResonanceInstead", false);

  // Load configuration file
  pythia.readFile("main31.cmnd");

  // Read in main settings
  int nEvent      = pythia.settings.mode("Main:numberOfEvents");
  int nError      = pythia.settings.mode("Main:timesAllowErrors");
  // Read in key POWHEG merging settings

  // Add in user hooks for shower vetoing
  PowhegHooksBB4L *powhegHooks = new PowhegHooksBB4L();
  pythia.setUserHooksPtr((UserHooks *) powhegHooks);

  // Initialise and list settings
  pythia.init();

  // Begin event loop; generate until nEvent events are processed
  // or end of LHEF file
  int iEvent = 0, iError = 0;
  while (true) {

    // Generate the next event
    if (!pythia.next()) {

      // If failure because reached end of file then exit event loop
      if (pythia.info.atEndOfFile()) break;

      // Otherwise count event failure and continue/exit as necessary
      cout << "Warning: event " << iEvent << " failed" << endl;
      if (++iError == nError) {
        cout << "Error: too many event failures.. exiting" << endl;
        break;
      }

      continue;
    }

    /*
     * Process dependent checks and analysis may be inserted here
     */

    // If nEvent is set, check and exit loop if necessary
    ++iEvent;
    if (nEvent != 0 && iEvent == nEvent) break;

  } // End of event loop.

  // Statistics, histograms and veto information
  pythia.stat();
  cout << endl;

  // Done.
  if (powhegHooks) delete powhegHooks;
  return 0;
}
