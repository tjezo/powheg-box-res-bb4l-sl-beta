      subroutine write_counters_wdec
      implicit none
      include 'pwhg_rnd.h'
      integer iun
      character * 20 pwgprefix
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      real * 8 powheginput
      integer stage
      character * 3 cst
      call newunit(iun)
      if(rnd_cwhichseed.eq.'none') then
         open(unit=iun,file=pwgprefix(1:lprefix)//'counters_wdec.dat'
     1     ,status='unknown')
      else
         stage=powheginput("#parallelstage")
         if(stage.gt.0) then
            write(cst(3:3),'(i1)') stage
            cst(1:2)='st'
            open(unit=iun,file=pwgprefix(1:lprefix)//'counters'//'-'
     1       //cst//'-'//rnd_cwhichseed//'-wdec.dat',status='unknown')
         else
            open(unit=iun,file=pwgprefix(1:lprefix)//'counters'//
     1           rnd_cwhichseed//'-wdec.dat',status='unknown')
         endif
      endif
      call printcnt(iun)
      close(iun)
      end

      subroutine pwhg_exit_wdec(iret)
      integer iret
      real * 8 tmp,powheginput
      tmp=powheginput('print unused tokens')
      call write_counters_wdec
      call exit(iret)
      end


