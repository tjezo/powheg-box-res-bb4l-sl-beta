      subroutine init_couplings
      implicit none
      include 'pwhg_par.h'
      include 'pwhg_flg.h'
      include 'pwhg_st.h'
      include 'PhysPars.h'
      include 'pwhg_physpar.h'
      real * 8 powheginput
      external powheginput
c Avoid multiple calls to this subroutine. The parameter file is opened
c but never closed ...
      logical called
      data called/.false./
      save called
      integer ewscheme
      double precision  Two, Four, Rt2, Pi
      parameter( Two = 2.0d0, Four = 4.0d0 )
      parameter( Rt2   = 1.414213562d0 )
      parameter( Pi = 3.14159265358979323846d0 )
      real * 8 lotopdecaywidth,nloratiotopdecay,pwhg_alphas
      external lotopdecaywidth,nloratiotopdecay,pwhg_alphas
      real * 8 lotwidth, nlotwidth
      real * 8 tmass_phsp,twidth_phsp
      complex * 8 cwmass2, czmass2

      integer j

 
      if(called) then
         return
      else
         called=.true.
      endif

      if(powheginput("#verytinypars").eq.1) then
         par_isrtinycsi = 1d-12
         par_isrtinyy = 1d-12
         par_fsrtinycsi = 1d-12
         par_fsrtinyy = 1d-12
      endif

c the only parameters relevant for this process are set
c via powheginput.

      ph_Zmass=powheginput('#zmass')
      if(ph_Zmass<0d0) ph_Zmass = 91.188d0
      ph_tmass=powheginput('#tmass')
      if(ph_tmass<0d0) ph_tmass=172.5d0
c      ph_Wmass=sqrt(ph_Zmass**2/Two+
c     $     sqrt(ph_Zmass**4/Four-Pi/Rt2*ph_alphaem/ph_gfermi*ph_Zmass**2))
      ph_Wmass=powheginput('#wmass')
      if(ph_Wmass<0d0) ph_Wmass = 80.419d0
      ph_bmass=powheginput('#bmass')
      if(ph_bmass<0d0) ph_bmass = 4.75d0
      ph_Hmass=powheginput('#hmass')
      if(ph_Hmass<0d0) ph_Hmass = 125d0

      ph_Zwidth=powheginput('#zwidth')
      if(ph_Zwidth<0d0) ph_Zwidth=2.441d0
      ph_Wwidth=powheginput('#wwidth')
      if(ph_Wwidth<0d0) ph_Wwidth=2.04807d0
      ph_Hwidth=powheginput('#hwidth')
      if(ph_Hwidth<0d0) ph_Hwidth=0.403d-2

      ph_WmWw = ph_Wmass * ph_Wwidth
      ph_ZmZw = ph_Zmass * ph_Zwidth
      ph_Wmass2 = ph_Wmass**2
      ph_Zmass2 = ph_Zmass**2


      ewscheme=powheginput('#ewscheme')
      if(ewscheme<1.or.ewscheme>2) ewscheme = 2

      if (ewscheme.eq.1) then  ! MW, MZ, Gmu scheme
        ph_gfermi=powheginput('#gfermi')
        if(ph_gfermi<0d0) ph_gfermi = 1.1658029175194381d-5
        if (powheginput("#complexGFermi").eq.0) then
          ph_alphaem=sqrt(2d0)/pi*ph_gfermi*abs(ph_Wmass**2*(1d0-(ph_Wmass/ph_Zmass)**2))
        else
          cwmass2=CMPLX(ph_Wmass**2,-ph_Wwidth*ph_Wmass)
          czmass2=CMPLX(ph_Zmass**2,-ph_Zwidth*ph_Zmass)
          ph_alphaem=sqrt(2d0)/pi*ph_gfermi*abs(cwmass2*(1d0-cwmass2/czmass2))
        endif
      else  ! MW, MZ, alpha scheme
        ph_alphaem=powheginput('#alpha')
        if(ph_alphaem<0d0) ph_alphaem = 1/132.50698d0
        if (powheginput("#complexGFermi").eq.0) then
          ph_gfermi=ph_alphaem/sqrt(2d0)*pi/(1d0-(ph_Wmass/ph_Zmass)**2)/ph_Wmass**2
        else
          cwmass2=CMPLX(ph_Wmass**2,-ph_Wwidth*ph_Wmass)
          czmass2=CMPLX(ph_Zmass**2,-ph_Zwidth*ph_Zmass)
          ph_gfermi=ph_alphaem/sqrt(2d0)*pi/abs((1d0-cwmass2/czmass2)*cwmass2)
        endif
      end if

cDBG  Consistency check for the calculation of the width, if the values
cDBG    below are used one should find Gamma_t = 1.3534459
cDBG    what we find instead is 1.3534445177520109d0 (which is close enough)
cDBG      call topwidth(4.75d0,173.2d0,80.385d0,91.1876d0,2.096353817037575d0, 
cDBG     1 1.16637d-5,0.007562339228909679d0,0.1041737055483838d0,lotwidth,nlotwidth)
cDBG      print*,"Top width consistency check:"
cDBG      print*,"  calculated the top width as:",nlotwidth
cDBG      print*,"  according to email from S. Pozzorini from 21/03/2016 "//
cDBG     1       "it should be:",1.3534459
cDBG      print*,"  the best we can do:", 1.3534445177520109d0

      call topwidth(ph_bmass,ph_tmass,ph_Wmass,ph_Zmass,ph_Wwidth,ph_gfermi,ph_alphaem,
     1      pwhg_alphas(ph_tmass**2,st_lambda5MSB,-1),lotwidth,nlotwidth)
      write(*,*) 'matrix elements top width as computed by MCFM routine',
     1 ' LO:',lotwidth,' NLO:', nlotwidth

      ph_twidth=powheginput('#twidth')
      if(ph_twidth<0d0) then
        if (flg_bornonly) then
          ph_twidth=lotwidth
      else
          ph_twidth=nlotwidth
        end if
      end if
      ph_twidth_lo = lotwidth

c set up masses and widths for resonance damping factors
      physpar_pdgmasses(24) = ph_Wmass
      physpar_pdgmasses(23) = ph_Zmass
      physpar_pdgmasses(5) = ph_bmass
      physpar_pdgmasses(6) = ph_tmass
      physpar_pdgmasses(22) = 0
      physpar_pdgwidths(24) = ph_Wwidth
      physpar_pdgwidths(23) = ph_Zwidth
      physpar_pdgwidths(6) = ph_twidth
      physpar_pdgwidths(22) = 0

      do j=1,physpar_npdg
         physpar_pdgmasses(-j) = physpar_pdgmasses(j)
         physpar_pdgwidths(-j) = physpar_pdgwidths(j)
      enddo

      physpar_phspmasses = physpar_pdgmasses
      physpar_phspwidths = physpar_pdgwidths

c Override phsp mass parameters in case one does mass variations by reweighting
c     top mass for phase space:
      tmass_phsp=powheginput('#tmass_phsp')
      if(tmass_phsp<0d0) tmass_phsp=ph_tmass

c     top width for phase space:
      twidth_phsp=powheginput('#twidth_phsp')
      if(twidth_phsp<0d0) then
         if(powheginput('#rwl_add').eq.1) then
            write(*,*)'******************ERROR:***********************'
            write(*,*)'If rwl_add=1 it is preferable to set twidth_phsp'
            write(*,*)'equal to the with that has been used in the original'
            write(*,*)'run. This is because in a reweight that may affect'
            write(*,*)'alpha_QCD, the top width is also affected, and reweighting'
            write(*,*)'needs a fixed parametrization of the phase space.'
            write(*,*)'exiting...'
            write(*,*)'***********************************************'
            call exit(-1)
         else
            if(tmass_phsp .eq. ph_tmass) then
               twidth_phsp=ph_twidth
            else               
               call topwidth(ph_bmass,tmass_phsp,ph_Wmass,ph_Zmass,
     $              ph_Wwidth,ph_gfermi,ph_alphaem,pwhg_alphas(
     $              tmass_phsp**2,st_lambda5MSB,-1),lotwidth,nlotwidth)
               write(*,*) 'phase space top width as computed by MCFM 
     $              routine LO:',lotwidth,' NLO:', nlotwidth 
               if (flg_bornonly) then
                  twidth_phsp=lotwidth
               else
                  twidth_phsp=nlotwidth
               end if
            endif
         endif
      endif


      
      physpar_phspmasses(6)  = tmass_phsp
      physpar_phspmasses(-6) = tmass_phsp
      
      physpar_phspwidths(6)  = twidth_phsp
      physpar_phspwidths(-6) = twidth_phsp

      physpar_mq(5) = ph_bmass
      physpar_ml(3) = powheginput('#mtau')
      if (physpar_ml(3) < 0) physpar_ml(3) = 1.77682

*********************************************************
* Print out of all the relevant couplings
*   so that we have them in a log file
*********************************************************
      write(*,*) '**************************************************'
      write(*,*) '* init_couplings.f writeout'
      write(*,*) '**************************************************'
      write(*,*) 'alpha=', ph_alphaem
      write(*,*) 'gfermi=', ph_gfermi
      write(*,*) 'bmass=', ph_bmass
      write(*,*) 'zmass=', ph_Zmass
      write(*,*) 'zwidth=', ph_Zwidth
      write(*,*) 'wmass=', ph_Wmass
      write(*,*) 'wwidth=', ph_Wwidth
      write(*,*) 'tmass=', ph_tmass
      write(*,*) 'twidth=', ph_twidth
      write(*,*) 'tmass_phsp=', tmass_phsp
      write(*,*) 'twidth_phsp=', twidth_phsp
      write(*,*) 'hmass=',  ph_hmass
      write(*,*) 'hwidth=', ph_hwidth
      write(*,*) '**************************************************'
      end

      subroutine topwidth(bmass,tmass,wmass,zmass,wwidth,gfinp,alpha,asinp,lowidth,nlowidth)
      implicit none
      real * 8 bmass, tmass, wmass, zmass, wwidth, gfinp, alpha, asinp
      include 'qcdcouple.f'
      include 'ewcouple.f'
      real * 8 lowidth, nlowidth
      real * 8, parameter :: pi = 3.14159265358979323846d0
      real * 8 lotopdecaywidth,nloratiotopdecay
      external lotopdecaywidth,nloratiotopdecay
      as=asinp
      gsq=4d0*pi*as
      ason2pi=as/(2d0*pi)
      gf=gfinp
      gwsq=8d0*wmass**2*gf/sqrt(2d0)
      gw=sqrt(gwsq)
      lowidth=lotopdecaywidth(tmass,bmass,wmass,wwidth) 
      nlowidth=lowidth*(1+nloratiotopdecay(tmass,bmass, 
     $        wmass,wwidth)) 
      end
