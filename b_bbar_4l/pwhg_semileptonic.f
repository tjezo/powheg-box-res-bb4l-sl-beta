      program semileptonic
      use openloops
      implicit none
      integer iwhichseed,parallelstage
      include 'LesHouches.h'
      include 'LesHouches_uborn.h'
      include 'pwhg_pdf.h'
      include "pwhg_st.h"
      include "pwhg_flg.h"
      include "pwhg_rnd.h"
      include "pwhg_physpar.h"
      include "pwhg_par.h"
      include "nlegborn.h"
      include "pwhg_kn.h"
      include "pwhg_flst.h"
      include "pwhg_rad.h"
      include "pwhg_rwl.h"
      integer maxevin
      integer j,nev
      character * 5 scheme
      character * 512 fout
      integer iun, iorder, iret, oun
      real * 8 powheginput
      external powheginput
      integer lprefix
      character * 20 pwgprefix
      common/cpwgprefix/pwgprefix,lprefix
      integer iarg, ios
      real * 8 tmp
      character * 100 arg
      integer, parameter :: maxnup_local = 16
      integer reslistborn(maxnup_local), em, iwp, iwpf, iwpa, iwm, iwmf, iwma, i, ubornflav(maxnup_local)
      real * 8 csi, y, azi, xb1, xb2, masses(maxnup_local), pborn(0:3, maxnup_local), cmpborn(0:3, maxnup_local), beta, vec(3)
      integer nlegr
      real * 8 x1, x2, preal(0:3, maxnup_local), cmpreal(0:3, maxnup_local), jacreal
      integer parallelstages,xgriditeration,iunstat
      character * 10 parlabel
      character * 40 mergelabels
      real * 8 xx(ndiminteg)      
      integer mcalls,icalls,ind
      real * 8 ptemp(0:3)
      integer, save :: whichw
      logical, save :: ini = .true.
      logical checkMomentumConservation, checkMomentumConservationLabLHE, checkMomentumConservationLab
      integer newcolor
      integer :: nskip = 0
      integer iii,ncalls
      real * 8 xxx(ndiminteg)
      real * 8 br_correction
      br_correction = 6.234054875
c     any number of symbol=value on the command line
c     is interpreted as overriding the powheg.input value.
c     Furthermore, the special keyword iwhichseed=<integer>
c     assigns the seed number of parallel runs, preventing
c     the program to inquire for it later.
      iwhichseed = -1
      iarg=1
      call get_command_argument(iarg,arg)
      do while(arg /= '')
         j=index(arg,'=')
         if(j>0) then
            read(arg(j+1:),fmt=*,iostat=ios) tmp
         else
            ios = -1
         endif
         if(ios == 0) then
            select case (arg(1:j-1))
            case ('iwhichseed')
               iwhichseed = nint(tmp)
            case default
               call powheginputoverride(arg(1:j-1),tmp)
            end select
         else
            write(*,*) 'pwhg_main: error in command line argument ',iarg
            call exit(-1)
         endif
         iarg=iarg+1
         call get_command_argument(iarg,arg)
      enddo
c When the event is read, the resonance history separation is unnecessary
c disable here the built in resonance history separation
      flg_user_reshists_sep = .true.
c Setup based on powheg.input
c   manyseeds and maxseeds (required for loading grids)
      if(powheginput("#manyseeds").eq.1) then
         par_maxseeds=powheginput("#maxseeds")
         if(par_maxseeds < 0) then
            par_maxseeds = 200
         endif
      endif

c Initialize
      if (ini) then
         whichw = powheginput("whichW")
         if (whichw .ne. -1 .and. whichw .ne. 1) then
            print*,"init_processes_wdec: Illegal value for whichW = ",whichw
            print*,"Allowed values are +1 and -1. Exiting!"
            call exit(-1)
         endif
         ini = .false.
      endif
      call semileptonicinit

c Parallelstages:
c 3   prepare the upper bound for the generation of radiation from W decay
c 4   generate radiation from W decay
      call getparallelparms(parallelstages,xgriditeration)
      call setparallellabel(parallelstages,xgriditeration,parlabel)

      if(parallelstages < 4) then
         call newunit(iunstat)
         if(powheginput("#manyseeds").eq.1) then
            open(unit=iun,status='old',iostat=ios,
     1           file=pwgprefix(1:lprefix)//'seeds.dat')
             if(ios.ne.0) then
                write(*,*) 'option manyseeds required but '
                write(*,*) 'file ',pwgprefix(1:lprefix)/
     $               /'seeds.dat not found'
               call exit(-1)
            endif 
            do j=1,1000000
               read(iun,*,iostat=ios)  rnd_initialseed
               if(ios.ne.0) goto 10
            enddo
 10         continue
            rnd_numseeds=j-1
            if(iwhichseed > 0) then
               rnd_iwhichseed = iwhichseed
            else
               write(*,*) 'enter which seed'
               read(*,*) rnd_iwhichseed
            endif
            if(rnd_iwhichseed.gt.rnd_numseeds) then
               write(*,*) ' no more than ',rnd_numseeds, ' seeds in ',
     1              pwgprefix(1:lprefix)//'seeds.dat'
               call exit(-1)
            endif
            if(rnd_iwhichseed.gt.par_maxseeds) then
               write(*,*)
     1              ' maximum seed value exceeded ',
     2              rnd_iwhichseed, '>', par_maxseeds
               write(*,*) ' Add to the powheg.input file a line like'
               write(*,*) ' maxseeds <maximum seed you need>'
               call exit(-1)
            endif
            rewind(iun)
            do j=1,rnd_iwhichseed
c Commented line to be used instead, for testing that manyseed runs
c yield the same results as single seed runs, provided the total number
c of calls is the same.
c        read(iun,*) rnd_initialseed,rnd_i1,rnd_i2
               read(iun,*) rnd_initialseed
               rnd_i1=0
               rnd_i2=0
            enddo
            close(iun)
            write(rnd_cwhichseed,'(i4)') rnd_iwhichseed
            do j=1,4
               if(rnd_cwhichseed(j:j).eq.' ') rnd_cwhichseed(j:j)='0'
            enddo
         else
            rnd_cwhichseed='none'
         endif
      endif
      open(unit=iunstat,file=mergelabels(pwgprefix,rnd_cwhichseed,
     1     parlabel,'stat_wdec.dat'),status='unknown')
      if(rnd_cwhichseed.ne.'none')
     1     call setrandom(rnd_initialseed,rnd_i1,rnd_i2)

c Execute stage specific code
c      if (parallelstages .ne. 3 .and. parallelstages .ne. 4) then
c         print*, "Please run either in parallelstage 3, in order to pre-calculate the upper bound for radiation from W-decay, "//
c     1 "or in parallel stage 4, to generate radiation from W-decay. Exiting!"
c         call exit(-1)
c      endif

c Exit if LO events are requested
      if (flg_LOevents) then
         print*, "LOevents=1: nothing to do. Exiting!"
         call exit(-1)
      endif

      if(parallelstages.eq.1) then
         flg_nlotest = .false.
         call setrandomseed(xgriditeration)
         if(xgriditeration.gt.1) then
            call loadxgridsn_wdec('btilde',xgriditeration-1,iret)
            if(iret.lt.0) then
               write(*,*) ' semuleptonic: cannot find grid files'
               write(*,*) ' for iteration ',xgriditeration-1
               write(*,*) ' exiting ...'
               call pwhg_exit_wdec(iret)
            endif
         endif
c imode=0,doregrid=.false.
         call mintwrapper('btilde',0,.false.,iunstat)
         call storexgrids_wdec('btilde',xgriditeration)
         call pwhg_exit_wdec(0)
      endif

      if(parallelstages.eq.2) then
c     The line below should fail. If you are trying to redo a stage 2 run, the fullgrid
c     file should be regenerated
         call loadgrids_wdec('btilde','fullgrid',iret)
         if(iret.eq.0) then
            write(*,*) ' bbinit: '//
     1           ' you are redoing the stage 2 run for btilde',
     2           ' but the fullgrid file from a previous run is there.'
            write(*,*) ' Do: rm *fullgrid*.dat and restart. Exiting ...'
            call pwhg_exit_wdec(-1)
         endif
         call loadxgridsn_wdec('btilde',-1,iret)
         if(iret.ne.0) then
            write(*,*) ' cannot load xgrid or gridinfo files for btilde'
            write(*,*) ' cannot perform stage 2'
            call pwhg_exit_wdec(-1)
         endif

         call mintwrapper('btilde',1,.false.,iunstat)
         call storegrids_wdec('btilde','grid')
         call writestat(iunstat)
         call pwhg_exit_wdec(0)
      endif


      if(parallelstages.eq.3) then
CTMPc the program will sleep until a lock is created
CTMP         call createlock('btilde-fullgrid-wdec.lock')
CTMPc now the lock is in place; try to load a fullgrid
CTMP         call loadgrids_wdec('btilde','fullgrid',iret)
CTMP         if(iret.lt.0) then
CTMP            call loadgrids_wdec('btilde','grid',iret)
CTMP            if(iret.lt.0) then
CTMP               write(*,*) ' cannot load grid files for btilde'
CTMP               call pwhg_exit_wdec(-1)
CTMP            endif
CTMP            call storegrids_wdec('btilde','fullgrid')
CTMP         endif
CTMP         call deletelock('btilde-fullgrid-wdec.lock')
CTMP      
CTMP         call writestat(iunstat)

c Compute radiation upper bounds
c do_maxrat with third argument = 0
c forces compute upper bound for radiation (iret ignored)
c        The following seem unnecessary, because it's done 
c        already before
c         call setrandom(rnd_initialseed,rnd_i1,rnd_i2)
         call do_maxrat_wdec(mcalls,icalls,0,iret)
         call pwhg_exit_wdec(0)
      
      else if(parallelstages.eq.4) then
c TODO figure out whether this is necessary
c      call set_parameter("psp_tolerance", 0.0000001)

c Load radiation upper bounds
c do_maxrat with third argument = 1
c forces loading upper bound for radiation
         call do_maxrat_wdec(mcalls,icalls,1,iret)
c exit if we fail
         if (iret==-1) then
            print*,"Failed to load the radiation upper bound for W-decay. Please run stage 3 first. Exiting!"
            call exit(-1)
         endif

c Set compression flag
         if (powheginput('#compress_lhe').eq.1d0) then
            flg_compress_lhe=.true.
         else
            flg_compress_lhe=.false.
         endif

c Load the input LHE file
         call opencountunit(nev,iun)
         write(*,*) 'EVENTS FOUND : ',nev
         call lhefreadhdr(iun)

c Setup the output LHE file
         fout=pwgprefix(1:lprefix)//'events-'//rnd_cwhichseed//'-semileptonic.lhe'
         call pwhg_io_open_write(trim(fout),oun,
     1        flg_compress_lhe,iret)
         flg_rwl=.true. ! to make sure the rwg related part of the header gets written out
         call lhefwritehdr(oun)

c Allow user to decay less events than in the input LHE file
         maxevin = powheginput('#maxev')
         if (maxevin>0.and.maxevin<=nev) then
           nev = maxevin
         endif

c Event loop
         do j=1,nev
            call lhefreadev(iun)
            if(nup.eq.0) then
               write(*,*) ' nup = 0 skipping event'
               goto 111
            endif

cDBGPRNT            print*,'========================================================================================================'
cDBGPRNT            print*,'processing event=',j
cDBGPRNT            print*,'========================================================================================================'
cDBGPRNT
cDBGPRNT            print*,'nup=',nup
cDBGPRNT            print*,'nup_uborn=',nup_uborn
cDBGPRNT            print*,'nradorig=',nup-nup_uborn
cDBGPRNT            print*,'rad_type=',rad_type

CDBGc           There are some events for which the underlying born is clearly wrong, dunno how this happens.
CDBGc           Let's check the top and anti-top virtualities and if the underlying born is wrong, let's skip the event
CDBG            if ( abs(pup(5,3) - pup_uborn(5,3))/pup(5,3) > 0.5 .or. abs(pup(5,4) - pup_uborn(5,4))/pup(5,4) > 0.5 ) then
CDBG               print*,'Wrong underlying born, skipping event'
CDBG               print*,"pup:"
CDBG               print*,pup(:,3)
CDBG               print*,pup(:,4)
CDBG               print*,pup_uborn(:,3)
CDBG               print*,pup_uborn(:,4)
CDBG               nskip = nskip + 1
CDBG               print*,'nskip=',nskip
CDBG               goto 999
CDBG            endif

            if (rad_type == 1) then
c              Check the Les Houches record on input (momentum, charge and colours)
cDBGPRNT               print*,"INPUT check_leshouches..."
               call check_leshouches
cDBGPRNT               print*,"...done (INPUT)"

c              Reconstruct the index of the underlying born flavour
c              first reconstruct the resonance structure
               do i=1,nup_uborn
                  reslistborn(i) = mothup(1,i)
                  if (reslistborn(i) < 3) reslistborn(i) = 0
               enddo
c              Find the flst_born index corresponding to the bb4l flavour structure read in from the LHE file
c              First relabel the input flavour structure from a bb4l event (stored in idup) into a semileptonic event
               call relabel(nup_uborn, idup_uborn(1:nup_uborn), reslistborn(1:nup_uborn), whichw, ubornflav(1:nup_uborn))
c              (and convert 21 to 0)
               call translateGluonID(nup_uborn, ubornflav(1:nup_uborn))
               rad_ubornsubp = -1
c              Iterate over all the flst_born flavour structures and compare
               do i=1,flst_nborn
                  if (all(flst_born(1:nup_uborn,i) == ubornflav(1:nup_uborn))) then
                     rad_ubornsubp = i
                     flst_ibornlength = flst_bornlength(rad_ubornsubp)
                     flst_bornrescurr(1:nup_uborn) = flst_bornres(1:nup_uborn,rad_ubornsubp)
                     exit
                  endif
               enddo
               if (rad_ubornsubp == -1) then
c                  print*, "Couldn't find a born process corresponding to this event. Exiting!"
c                  call exit(-1)
                  print*, "Couldn't find a born process corresponding to this event. Skipping event!"
                  cycle
               else
cDBGPRNT                  print*, "The underlying born flavour structure of the dileptonic event on input is:"
cDBGPRNT                  print*, idup_uborn(1:nup_uborn) 
cDBGPRNT                  print*, "The corresponding born flavour structure of the semileptonic event, found at entry id = ",rad_ubornsubp," is:"
cDBGPRNT                  print*, flst_born(1:nup_uborn,rad_ubornsubp)
cDBGPRNT                  print*, "flst_ibornlength=",flst_ibornlength
cDBGPRNT                  print*, "flst_bornrescurr=",flst_bornrescurr
cDBGPRNT                  print*, "reslistborn=     ",reslistborn
               endif

c              Process the kinematics of the event read in
c              fill in the underlying born momenta and the x1, x2 fractions, masses
               kn_xb1=pup_uborn(4,1)/ebmup(1)
               kn_xb2=pup_uborn(4,2)/ebmup(2)
               kn_sborn = 4*pup_uborn(4,1)*pup_uborn(4,2)
               kn_masses = 0
               do i=1,nup_uborn
                  kn_masses(i) = physpar_phspmasses(idup(i))
               enddo
               kn_pborn(1:3,1:nup_uborn) = pup_uborn(1:3,1:nup_uborn)
               kn_pborn(0,1:nup_uborn) = pup_uborn(4,1:nup_uborn)
c              check momentum conservation in the born momenta
               ptemp(:) = kn_pborn(:,1) + kn_pborn(:,2)
               do i=3,nup_uborn
                  if (istup(i) /= 2) ptemp(:) = ptemp(:) - kn_pborn(:,i)
               enddo
               if (.not.checkMomentumConservationLab(ptemp, 1d-3)) then
                  print*,"semileptonic: input kn_pborn momenta not conserved. Exiting!"
                  call exit(-1)
               endif
c              TODO use compcmkin instead of this nonsense
c              boost lab momenta read in from LHE into the CM momenta
               beta = ( kn_pborn(3,1)+kn_pborn(3,2) ) / ( kn_pborn(0,1) + kn_pborn(0,2) )
               vec(1)=0
               vec(2)=0
               vec(3)=1 
               call mboost(nup_uborn,vec,-beta,kn_pborn(:,1:nup_uborn),kn_cmpborn(:,1:nup_uborn))
               ptemp(:) = kn_cmpborn(:,1) + kn_cmpborn(:,2)
               do i=3,nup_uborn
                  if (istup(i) /= 2) ptemp(:) = ptemp(:) - kn_cmpborn(:,i)
               enddo
               if (.not.checkMomentumConservation(ptemp, 1d-3)) then
                  print*,"semileptonic: input kn_cmpborn momenta not conserved. Exiting!"
                  call exit(-1)
               endif

c              Finally, modify the LesHouches record
c              relabel leptons to quarks, according to the channel
               call relabel(nup_uborn, idup(1:nup_uborn), reslistborn(1:nup_uborn), whichw, idup(1:nup_uborn))
c              fix the colours (the leptons in the original event record had no colours)
               call getnewcolor(newcolor)
               do i=3,nup_uborn
c                 if the particle is a quark
                  if (abs(idup(i)) <= 6 .and. abs(idup(i)) > 0) then
                     if (idup(i) > 0 .and. icolup(1,i) == 0) then
                        icolup(1,i) = newcolor
                     else if (idup(i) < 0 .and. icolup(2,i) == 0) then
                        icolup(2,i) = newcolor
                     endif
                  endif
               enddo

c              Check the transformed Les Houches record
cDBGPRNT               print*,"RELABLED check_leshouches ..."
               call check_leshouches
cDBGPRNT               print*,"...done (RELABELED)"

c              Attach radiation using the Sudakov veto algorithm
               call pwhgevent_wdec()

c              Reweight with the correct btilde weight. 
c               call genwrapper('btilde',2,mcalls,ncalls,xxx,iii)
c               call mintwrapper_result('btilde',rwl_index,newweight)

c              apply branching ratio correction
               xwgtup = br_correction*xwgtup
               if (rwl_num_weights.gt.0) then
                  rwl_weights(1:rwl_num_weights)=
     $               br_correction * rwl_weights(1:rwl_num_weights)
               endif

            else if (rad_type == 2) then

c              Reconstruct the index of the underlying born flavour
c              first reconstruct the resonance structure
               do i=1,nup
                  reslistborn(i) = mothup(1,i)
                  if (reslistborn(i) < 3) reslistborn(i) = 0
               enddo

c              Finally, modify the LesHouches record
c              relabel leptons to quarks, according to the channel
               call relabel(nup, idup(1:nup), reslistborn(1:nup), whichw, idup(1:nup))
c              fix the colours (the leptons in the original event record had no colours)
               call getnewcolor(newcolor)
               do i=3,nup
c                 if the particle is a quark
                  if (abs(idup(i)) <= 6 .and. abs(idup(i)) > 0) then
                     if (idup(i) > 0 .and. icolup(1,i) == 0) then
                        icolup(1,i) = newcolor
                     else if (idup(i) < 0 .and. icolup(2,i) == 0) then
                        icolup(2,i) = newcolor
                     endif
                  endif
               enddo

c              apply branching ratio correction
               xwgtup = br_correction*xwgtup
               if (rwl_num_weights.gt.0) then
                  rwl_weights(1:rwl_num_weights)=
     $               br_correction * rwl_weights(1:rwl_num_weights)
               endif

            endif

c           Write the event out
999         call lhefwritev(oun)
            if (mod(j,1000).eq.0) then
               write(*,*) "# of events processed =",j
            endif
111         continue
         enddo

         call lhefwritetrailer(oun)

         call pwhg_io_close(iun)
         call pwhg_io_close(oun)
      endif
      end      

      logical function checkMomentumConservationLabLHE(p,eps)
      implicit none
      real * 8 p(0:3), eps
      logical equal
      if ( equal(p(0),0d0,eps) .and. equal(p(1),0d0,eps)) then
         checkMomentumConservationLabLHE = .true.
      else
         checkMomentumConservationLabLHE = .false.
      endif
      return
      end

      logical function checkMomentumConservationLab(p,eps)
      implicit none
      real * 8 p(0:3), eps
      logical equal
      if ( equal(p(1),0d0,eps) .and. equal(p(2),0d0,eps)) then
         checkMomentumConservationLab = .true.
      else
         checkMomentumConservationLab = .false.
      endif
      return
      end

      subroutine relabel(length, ain, ares, whichW, aout)
      implicit none
      integer length, ain(*), ares(*), whichW, aout(*)
      integer j
      aout(1:length) = ain(1:length)
      do j=1,length
c     this is required for both, the internal flavour lists and those read in
c     from LHE files
         if (ares(j) > 0) then
            if (ain(ares(j)) == whichW*24) then
               if (MOD(ABS(ain(j)),2) == 0) then
                  aout(j)= +whichW*4 ! c or c~
               else 
                  aout(j) = -whichW*3 ! s~ or s (depending on W charge)
               endif
            endif
         endif
      enddo
      end subroutine

      subroutine translateGluonID(length, a)
      implicit none
      integer length, a(*)
      integer j
      do j=1,length
         if (a(j) == 21) a(j) = 0
      enddo
      end subroutine

c      logical function equal(a,b,eps)
c      implicit none
c      real * 8 a, b, eps
cc      real, parameter :: eps=4e-3
c      if ( abs((a - b)/a) < eps ) then
c         equal=.true.
c      else if ( b == 0d0 .and. ( abs(a) < eps )) then
c         equal=.true.
c      else 
c         equal=.false.
c      endif
c      return
c      end function
c
c      logical function checkMomentumConservation(p,eps)
c      implicit none
c      real * 8 p(0:3), eps
c      logical equal
c      if ( equal(p(1),0d0,eps) .and. equal(p(2),0d0,eps) .and. equal(p(3),0d0,eps) ) then
c         checkMomentumConservation = .true.
c      else
c         checkMomentumConservation = .false.
c      endif
c      return
c      end

