
FeynArtsProcess = {-F[1, {1}], F[2, {1}]} -> {-F[3, {1}], F[3, {1}], -F[3, {2}], F[4, {2}], -F[4, {3}], F[4, {3}]};

SortExternal = True;

OpenLoopsModel = "SM";

CreateTopologiesOptions = {
  ExcludeTopologies -> {Snails, WFCorrectionCTs, TadpoleCTs, Loops[7 | 8]},
  Adjacencies -> {3, 4}
};

InsertFieldsOptions = {
  Model -> {"SMQCD", "SMQCDR2"},
  GenericModel -> "Lorentz",
  InsertionLevel -> {Particles},
  Restrictions -> {ExcludeParticles -> {S[2 | 3]}, NoQuarkMixing}
};

UnitaryGauge = True;

ColourCorrelations = False;

SubProcessName = Automatic;

SelectCoupling = MemberQ[{2, 4, 6}, Exponent[#1, eQED]] & ;

SelectInterference = {
  eQED -> {8}
};

SelectTreeDiagrams = HasResonance[V[3], {1, 2}][##1] && HasResonance[-V[3], {5, 6}][##1] & ;

SelectLoopDiagrams = True & ;

SelectCTDiagrams = True & ;

ReplaceOSw = False;

SetParameters = {
  ME -> 0,
  MM -> 0,
  CKMORDER -> 0,
  nc -> 3,
  nf -> 6,
  MU -> 0,
  MD -> 0,
  MS -> 0,
  MC -> 0,
  LeadingColour -> 0,
  POLSEL -> 1
};


ChannelMap = {
  {"nexeuccxdxbbx<1,2,5,3,4,6,7,8,9>"}
};

Approximation = "pole_ww";

ForceLoops = Automatic;

ForceLoopsInclude = Automatic;

NonZeroHels = Null;

OnTheFlyMode = 0;
