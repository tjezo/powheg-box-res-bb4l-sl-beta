
module ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND
  use KIND_TYPES, only: REALKIND
  implicit none
  logical, save           :: colmat_not_initialised = .true.
  complex(REALKIND), save :: K1(4,4)
  contains
  subroutine colourmatrix_init
    use ol_parameters_decl_/**/REALKIND, only: CI
    implicit none
    colmat_not_initialised = .false.
    ! colour matrix

  K1(1,:) = [ 12,  4,  4,  0]
  K1(2,:) = [  4, 12,  0,  4]
  K1(3,:) = [  4,  0, 12,  4]
  K1(4,:) = [  0,  4,  4, 12]

  end subroutine colourmatrix_init
end module ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND



module ol_forced_parameters_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND
  implicit none
  contains
  subroutine check_forced_parameters
    use ol_parameters_decl_/**/REALKIND
    use ol_loop_parameters_decl_/**/REALKIND
#ifndef PRECISION_dp
    use ol_loop_parameters_decl_/**/DREALKIND, only: LeadingColour, nc, nf, CKMORDER
#endif
    implicit none
    logical, save :: checks_not_written = .true.

    if (checks_not_written) then
    ! e.g.
    ! if (ME /= 0) write(*,101) 'ME = 0'
  if (ME /= 0) write(*,101) 'ME = 0'
  if (MM /= 0) write(*,101) 'MM = 0'
  if (CKMORDER /= 0) write(*,101) 'CKMORDER = 0'
  if (nc /= 3) write(*,101) 'nc = 3'
  if (nf /= 6) write(*,101) 'nf = 6'
  if (MU /= 0) write(*,101) 'MU = 0'
  if (MD /= 0) write(*,101) 'MD = 0'
  if (MS /= 0) write(*,101) 'MS = 0'
  if (MC /= 0) write(*,101) 'MC = 0'
  if (LeadingColour /= 0) write(*,101) 'LeadingColour = 0'
  if (wMB /= 0) write(*,101) 'wMB = 0'
  if (wMB /= 0) write(*,101) 'wMB = 0'


    checks_not_written = .false.
    end if

    101 format('[OpenLoops] === WARNING ===',/,'[OpenLoops] code was generated with ',A,/,'[OpenLoops] ===============')
  end subroutine check_forced_parameters
end module ol_forced_parameters_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND

module ol_tree_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND
  use KIND_TYPES, only: REALKIND, DREALKIND, intkind1, intkind2
  implicit none
  integer,           save :: factors_status = -1
  complex(REALKIND), save :: f(2)
  complex(REALKIND), save :: den(36)
#ifdef PRECISION_dp
  integer(intkind1), save :: ntry = 1
  integer(intkind2), parameter :: nheltot = 512 ! number of helicity configurations
  integer(intkind2), save :: nhel = 512 ! number of non-vanishing helicity configurations (adapted at runtime)
  integer(intkind2), save :: Hel(512) ! physical helicity states
  complex(DREALKIND) :: M1helarr(4,512) ! cache
#endif

  contains

subroutine factors_init()
  use ol_parameters_decl_/**/REALKIND
  use ol_loop_parameters_decl_/**/REALKIND, only: loop_parameters_status
  implicit none
  factors_status = parameters_status
  ! factors of the diagrams
    f(1) = (CI*eQED**4*gQCD**3)/(4._/**/REALKIND*sw**4)
    f(2) = (eQED**4*gQCD**3)/(sw**4*4._/**/REALKIND)

end subroutine factors_init


subroutine born_denominators_init()
  use ol_parameters_decl_/**/REALKIND ! masses
  use ol_momenta_decl_/**/REALKIND, only: Q
  implicit none

  ! propagators

  den(1) = 1 / (Q(5,5) - MW2)
  den(2) = 1 / (Q(5,10) - MW2)
  den(3) = 1 / (Q(5,320) - MB2)
  den(5) = 1 / (Q(5,48))
  den(6) = 1 / (Q(5,133) - MT2)
  den(8) = 1 / (Q(5,330) - MT2)
  den(13) = 1 / (Q(5,368) - MB2)
  den(17) = 1 / (Q(5,112) - MB2)
  den(22) = 1 / (Q(5,389) - MT2)
  den(25) = 1 / (Q(5,304))
  den(29) = 1 / (Q(5,272))
  den(33) = 1 / (Q(5,288))

  ! denominators

  den(4) = den(2)*den(3)
  den(7) = den(1)*den(6)
  den(9) = den(4)*den(8)
  den(10) = den(5)*den(7)
  den(11) = den(9)*den(10)
  den(12) = den(3)*den(5)
  den(14) = den(12)*den(13)
  den(15) = den(2)*den(7)
  den(16) = den(14)*den(15)
  den(18) = den(5)*den(17)
  den(19) = den(13)*den(18)
  den(20) = den(15)*den(19)
  den(21) = den(2)*den(18)
  den(23) = den(7)*den(22)
  den(24) = den(21)*den(23)
  den(26) = den(5)*den(25)
  den(27) = den(13)*den(26)
  den(28) = den(15)*den(27)
  den(30) = den(25)*den(29)
  den(31) = den(13)*den(30)
  den(32) = den(15)*den(31)
  den(34) = den(25)*den(33)
  den(35) = den(13)*den(34)
  den(36) = den(15)*den(35)

end subroutine born_denominators_init


! **********************************************************************
#ifdef PRECISION_dp
subroutine amp2(P_scatt, M2, I, MOM, nextcombs, extcombs, M2munu) &
    & bind(c,name="ol_f_amp2_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2(P_scatt, M2, I, MOM, nextcombs, extcombs, M2munu)
  use ol_tree_ppllllbb_split_nexnmemxuuxbbxg_4_/**/DREALKIND, only: &
    & ntry, nhel, Hel, M1helarr
#endif
! P_scatt(0:3,Npart) = incoming external momenta
! M2  = helicity-summed squared matrix element for anti-nu_e nu_mu e- mu+ up anti-up bottom anti-bottom glue -> 0
! I   = emitter, 0 means none (replace wave function I in the current crossing by the momentum MOM),
!       for I < 0 emitter for PowHeg's B^mu,nu
! MOM = external "polarisation vector" for gluon emitter
! nextcombs is the length of the array extcombs
! The elements of the array extcombs specify for which external particle combinations
!   the colour correlations will be calculated. For particles i,j: i*(i-1)/2+j
!   i=j=0 -> 0 means no colour insertion.
! M2munu = Spin correlated born squared amplitude in PowHeg format B^mu,nu for emitter -I
! **********************************************************************
  use ol_parameters_decl_/**/REALKIND !, only: ci, parameters_status, ZERO, scalefactor, >masses<
  use ol_parameters_init_/**/REALKIND, only: ensure_mp_init
  use ol_kinematics_/**/REALKIND, only: conv_mom_scatt2in, internal_momenta
  use ol_momenta_decl_/**/DREALKIND, only: momenta_nan_check
  use ol_momenta_decl_/**/REALKIND, only: Q
  use ol_data_types_/**/REALKIND
  use ol_helicity_bookkeeping_/**/REALKIND, only: &
    & helbookkeeping_wf, helsync, flip_phase
  use ol_helicity_init, only: helbookkeeping_flip, helsync_flip
  use ol_h_propagators_/**/REALKIND
  use ol_h_wavefunctions_/**/REALKIND
  use ol_wavefunctions_/**/REALKIND, only: wf_V_Std
  use ol_h_vertices_/**/REALKIND
  use ol_h_contractions_/**/REALKIND
  use ol_external_ppllllbb_split_nexnmemxuuxbbxg_4, only: &
    & external_perm_ppllllbb_split_nexnmemxuuxbbxg_4, &
    & external_perm_inv_ppllllbb_split_nexnmemxuuxbbxg_4, &
    & extcomb_perm_ppllllbb_split_nexnmemxuuxbbxg_4, &
    & average_factor_ppllllbb_split_nexnmemxuuxbbxg_4
  use ol_external_ppllllbb_split_nexnmemxuuxbbxg_4, only: &
    & H, hel_not_initialised, hel_init, POLSEL
  use ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND, only: &
    & colmat_not_initialised, colourmatrix_init
  use ol_forced_parameters_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND, only: &
    & check_forced_parameters
  use ol_heltables_ppllllbb_split_nexnmemxuuxbbxg_4
  use ol_kinematics_/**/REALKIND, only: LC2Std_Rep_cmplx
  implicit none

  real(DREALKIND), intent(in)  :: P_scatt(0:3,9)
  real(REALKIND),  intent(out) :: M2(0:1-1)
  real(REALKIND),  intent(out) :: M2munu(4,4)
  integer,         intent(in)  :: I
  real(REALKIND),  intent(in)  :: MOM(0:3)
  integer,         intent(in)  :: nextcombs
  integer,         intent(in)  :: extcombs(nextcombs)

  integer           :: ReplacePol, JBmunu, extcombs_permuted(nextcombs), shift, k, r, m, n
  real(REALKIND)    :: P(0:3,9)
  real(REALKIND)    :: extmasses2(9)
  real(REALKIND)    :: M2add(0:1-1)
  complex(REALKIND) :: M2munuadd(2)
  complex(REALKIND) :: MOM_LC(4), M1(4), M1helarray(4,512)
  real(REALKIND)    :: P_scatt_intern(0:3,9)
  complex(REALKIND) :: epLC(1:4), epStd(1:4,2)
  real(REALKIND), save :: scalebackfactor, old_scalefactor = 0
  integer(intkind1) :: nsync

  ! type(wfun) :: ex1(h1), ex2(h2), ... ! external wave functions for h<n> helicities
  type(wfun) :: ex1(2), ex2(2), ex3(2), ex4(2), ex5(2), ex6(2), ex7(2), ex8(2), ex9(2)

  ! type(wfun) :: wf<h>(h,n), ... ! n wave functions with h helicity configurations
  type(wfun) :: wf4(4,11), wf8(8,7), wf16(16,14), wf32(32,3), wf512(512,7)

  type(polcont) :: A(512,7)
  complex(REALKIND) :: Aj(7)

  complex(REALKIND) :: omega(2) ! phases for helicity correlations

  call ensure_mp_init()

  if (hel_not_initialised) call hel_init()
  if (colmat_not_initialised) call colourmatrix_init()
  if (factors_status /= parameters_status) then
    ! Note: if factors_init would only be called when parameters changed which are relevant for the factors,
    ! a different 'status' would have to be used, because check_forced_parameters should be called after every parameter change.
    call check_forced_parameters()
    call factors_init()
  end if

  if (momenta_nan_check(P_scatt) /= 0) then
    M2 = 0
    return
  end if

  ! Convert 2 -> n-2 PS-point to n -> 0 (so that P(1) + ... + P(n) = 0)
  extmasses2 = [ rZERO2, rZERO2, rZERO2, rZERO2, rZERO2, rZERO2, rMB2, rMB2, rZERO2 ]
  call conv_mom_scatt2in(P_scatt, extmasses2, P, &
    & external_perm_inv_ppllllbb_split_nexnmemxuuxbbxg_4,9)

  ! internal-propagator momenta in light-cone representation
  call internal_momenta(P,9)

  ! denominators
  call born_denominators_init()

  ReplacePol = 0
  JBmunu = 0
  if (I > 0) then
    ReplacePol = external_perm_ppllllbb_split_nexnmemxuuxbbxg_4(I)
  else if (I < 0) then
    JBmunu = external_perm_ppllllbb_split_nexnmemxuuxbbxg_4(-I)
  end if

  do k = 1, nextcombs
    extcombs_permuted(k) = extcomb_perm_ppllllbb_split_nexnmemxuuxbbxg_4(extcombs(k))
  end do

  if (heltables_not_init) call init_heltables()

  42 continue
  ! external WFs
  call pol_wf_A(P(:,1), rZERO, H1, ex1, POLSEL(1))
  call pol_wf_Q(P(:,2), rZERO, H2, ex2, POLSEL(2))
  call pol_wf_Q(P(:,3), rZERO, H3, ex3, POLSEL(3))
  call pol_wf_A(P(:,4), rZERO, H4, ex4, POLSEL(4))
  call pol_wf_Q(P(:,5), rZERO, H5, ex5, POLSEL(5))
  call pol_wf_A(P(:,6), rZERO, H6, ex6, POLSEL(6))
  call pol_wf_Q(P(:,7), rMB, H7, ex7, POLSEL(7))
  call pol_wf_A(P(:,8), rMB, H8, ex8, POLSEL(8))
  call pol_wf_V(P(:,9), rZERO, H9, ex9, POLSEL(9))


  if (ntry == 1) then
    shift = 1
    ! call helbookkeeping_flip(H1, 1, shift, eflip, exthel, firstpol)
    ! call helbookkeeping_wf(H1, ex1, shift) ...

    if (any(POLSEL /= 0)) then

      call pol_wf_A(P(:,1), rZERO, H1, ex1, 0)
      call pol_wf_Q(P(:,2), rZERO, H2, ex2, 0)
      call pol_wf_Q(P(:,3), rZERO, H3, ex3, 0)
      call pol_wf_A(P(:,4), rZERO, H4, ex4, 0)
      call pol_wf_Q(P(:,5), rZERO, H5, ex5, 0)
      call pol_wf_A(P(:,6), rZERO, H6, ex6, 0)
      call pol_wf_Q(P(:,7), rMB, H7, ex7, 0)
      call pol_wf_A(P(:,8), rMB, H8, ex8, 0)
      call pol_wf_V(P(:,9), rZERO, H9, ex9, 0)

    end if

    call helbookkeeping_flip(H1, 1, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H1, ex1, shift)
    call helbookkeeping_flip(H2, 2, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H2, ex2, shift)
    call helbookkeeping_flip(H3, 3, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H3, ex3, shift)
    call helbookkeeping_flip(H4, 4, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H4, ex4, shift)
    call helbookkeeping_flip(H5, 5, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H5, ex5, shift)
    call helbookkeeping_flip(H6, 6, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H6, ex6, shift)
    call helbookkeeping_flip(H7, 7, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H7, ex7, shift)
    call helbookkeeping_flip(H8, 8, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H8, ex8, shift)
    call helbookkeeping_flip(H9, 9, shift, eflip, exthel, firstpol)
    call helbookkeeping_wf(H9, ex9, shift)

  end if

  ! internal WFs
  ! e.g. call vert_VQ_A(ntry, ex3, ex1, wf1, n1, t1) ...
  call vert_QA_W(ntry, ex3, ex1, wf4(:,1), n3(:,1), t3x4(:,:,1))
  call vert_QA_W(ntry, ex2, ex4, wf4(:,2), n3(:,2), t3x4(:,:,2))
  call vert_QA_V(ntry, ex5, ex6, wf4(:,3), n3(:,3), t3x4(:,:,3))
  call vert_VQ_A(ntry, ex9, ex7, wf4(:,4), n3(:,4), t3x4(:,:,4))
  call prop_W_W(ntry, wf4(:,1), Q(:,5), MW, 1_intkind1, wf4(:,5), n2(1))
  call prop_W_W(ntry, wf4(:,2), Q(:,10), MW, 1_intkind1, wf4(:,6), n2(2))
  call prop_Q_A(ntry, wf4(:,4), Q(:,320), MB, 1_intkind1, wf4(:,7), n2(3))
  call vert_AW_Q(ntry, ex8, wf4(:,5), wf8(:,1), n3(:,5), t3x8(:,:,1))
  call vert_WQ_A(ntry, wf4(:,6), wf4(:,7), wf16(:,1), n3(:,6), t3x16(:,:,1))
  call prop_A_Q(ntry, wf8(:,1), Q(:,133), MT, 1_intkind1, wf8(:,2), n2(4))
  call prop_Q_A(ntry, wf16(:,1), Q(:,330), MT, 1_intkind1, wf16(:,2), n2(5))
  call vert_AV_Q(ntry, wf8(:,2), wf4(:,3), wf32(:,1), n3(:,7), t3x32(:,:,1))
  call vert_VQ_A(ntry, wf4(:,3), wf4(:,7), wf16(:,3), n3(:,8), t3x16(:,:,2))
  call prop_Q_A(ntry, wf16(:,3), Q(:,368), MB, 1_intkind1, wf16(:,4), n2(6))
  call vert_AW_Q(ntry, wf8(:,2), wf4(:,6), wf32(:,2), n3(:,9), t3x32(:,:,2))
  call vert_VQ_A(ntry, wf4(:,3), ex7, wf8(:,3), n3(:,10), t3x8(:,:,2))
  call prop_Q_A(ntry, wf8(:,3), Q(:,112), MB, 1_intkind1, wf8(:,4), n2(7))
  call vert_VQ_A(ntry, ex9, wf8(:,4), wf16(:,5), n3(:,11), t3x16(:,:,3))
  call prop_Q_A(ntry, wf16(:,5), Q(:,368), MB, 1_intkind1, wf16(:,6), n2(8))
  call vert_AV_Q(ntry, wf8(:,2), ex9, wf16(:,7), n3(:,12), t3x16(:,:,4))
  call vert_WQ_A(ntry, wf4(:,6), wf8(:,4), wf32(:,3), n3(:,13), t3x32(:,:,3))
  call prop_A_Q(ntry, wf16(:,7), Q(:,389), MT, 1_intkind1, wf16(:,8), n2(9))
  call vert_UV_W(ntry, wf4(:,3), Q(:,48), ex9, Q(:,256), wf8(:,5), n3(:,14), t3x8(:,:,3))
  call vert_VQ_A(ntry, wf8(:,5), ex7, wf16(:,9), n3(:,15), t3x16(:,:,5))
  call prop_Q_A(ntry, wf16(:,9), Q(:,368), MB, 1_intkind1, wf16(:,10), n2(10))
  call vert_VQ_A(ntry, ex9, ex5, wf4(:,8), n3(:,16), t3x4(:,:,5))
  call prop_Q_A(ntry, wf4(:,8), Q(:,272), ZERO, 0_intkind1, wf4(:,9), n2(11))
  call vert_QA_V(ntry, wf4(:,9), ex6, wf8(:,6), n3(:,17), t3x8(:,:,4))
  call vert_VQ_A(ntry, wf8(:,6), ex7, wf16(:,11), n3(:,18), t3x16(:,:,6))
  call prop_Q_A(ntry, wf16(:,11), Q(:,368), MB, 1_intkind1, wf16(:,12), n2(12))
  call vert_AV_Q(ntry, ex6, ex9, wf4(:,10), n3(:,19), t3x4(:,:,6))
  call prop_A_Q(ntry, wf4(:,10), Q(:,288), ZERO, 0_intkind1, wf4(:,11), n2(13))
  call vert_QA_V(ntry, ex5, wf4(:,11), wf8(:,7), n3(:,20), t3x8(:,:,5))
  call vert_VQ_A(ntry, wf8(:,7), ex7, wf16(:,13), n3(:,21), t3x16(:,:,7))
  call prop_Q_A(ntry, wf16(:,13), Q(:,368), MB, 1_intkind1, wf16(:,14), n2(14))


  ! colour-stripped amplitudes
  do nsync = ntry+ntry-1, ntry+1  !  nsync = 1,2  for 1st point and nsync = 3 later
    call diagrams()
    if (nsync == 1) then
      call helsync(nsync, A, nhel, Hel)
      call helsync_flip(nsync, nhel, Hel, eflip, exthel)
      if (any(POLSEL /= 0)) then
        ntry = 2
        goto 42
      end if
    end if
  end do

  do k = 1, nhel
    call colourvector(A, k, M1helarray(:,k))
  end do
  M1helarray(:,nhel+1:) = 0
  M1helarr = M1helarray ! fill cache

  M2 = 0
  if (ReplacePol == 0) then ! no helicity correlation

    do k = 1, nhel
      call colint(M1helarray(:,k), M2add, extcombs_permuted)
      M2 = M2 + M2add
    end do

  else ! helicity correlation

    call flip_phase(P(:,ReplacePol), firstpol(ReplacePol), MOM, omega)
    do k = 1, nhel
      M1 = M1helarray(:,k)
      r = eflip(k, ReplacePol) ! Flip helicity of external particle ReplacePol (gluon emitter).
      if (r <= nhel) then      ! Only add flipped helicity configuration if it doesn't vanish.
        M1 = M1 + omega(exthel(k,ReplacePol)) * M1helarray(:,r)
      end if
      call colint2(M1helarray(:,k), M1, M2add, extcombs_permuted)
      M2 = M2 + M2add
    end do
    M2 = 0.5_/**/REALKIND * M2

  end if

  if ( JBmunu /= 0 ) then ! POWHEG's Bmunu helicity correlation
    P_scatt_intern = P_scatt
    M2munu = 0
    ! get wfs
    call wf_V_Std(P_scatt_intern(:,-I), rZERO, -1, epLC)
    call LC2Std_Rep_cmplx(epLC, epStd(:,1))
    call wf_V_Std(P_scatt_intern(:,-I), rZERO, 1, epLC)
    call LC2Std_Rep_cmplx(epLC, epStd(:,2))

    ! sum over helicities
    do k = 1, nhel
      !same helicity case
      call colintmunu(M1helarray(:,k), M1helarray(:,k), M2munuadd(1))
      !opposite helicity case
      r = eflip(k, JBmunu) ! Flip helicity of external particle JBmunu (gluon emitter).
      if (r <= nhel) then  ! Only add opposite helicity configuration if it doesn't vanish.
        call colintmunu(M1helarray(:,k), M1helarray(:,r), M2munuadd(2))
      else
        M2munuadd(2) = 0
      end if
      ! Fill B^(mu,nu) = sum_(k) sum_(l1,l2) M^*_(k) M_(k) (eps^(mu)_(l1))^* eps^(nu)_(l2)
      do m = 1,4
        do n = 1,4
          M2munu(m,n) = M2munu(m,n) + M2munuadd(1)*conjg(epStd(m,exthel(k,JBmunu)))*epStd(n,exthel(k,JBmunu))
          if (r <= nhel) then
            M2munu(m,n) = M2munu(m,n) + M2munuadd(2)*conjg(epStd(m,exthel(k,JBmunu)))*epStd(n,exthel(r,JBmunu))
          end if
        end do
      end do
    end do

    M2munu = M2munu / average_factor_ppllllbb_split_nexnmemxuuxbbxg_4
  end if

  ! Colour and helicity average and symmetry factor of outgoing particles
  M2add = M2 / average_factor_ppllllbb_split_nexnmemxuuxbbxg_4

  do k = 0, 1-1
    M2(k) = M2add(extcomb_perm_ppllllbb_split_nexnmemxuuxbbxg_4(k))
  end do

  if (ntry == 1) ntry = 2

  if (scalefactor /= old_scalefactor) then
    scalebackfactor = scalefactor**(2*9-8)
    old_scalefactor = scalefactor
  end if
  M2 = scalebackfactor * M2

  contains

subroutine diagrams()
  implicit none
  ! e.g. call cont_VV(nsync, wf3, wf6, A(:,1), n64, t64, nhel, den(5)) ...

    call cont_QA(nsync, wf16(:,2), wf32(:,1), A(:,1), n3(:,22), t3x512(:,:,1), nhel, den(11))
    call cont_QA(nsync, wf16(:,4), wf32(:,2), A(:,2), n3(:,23), t3x512(:,:,2), nhel, den(16))
    call cont_QA(nsync, wf32(:,2), wf16(:,6), A(:,3), n3(:,24), t3x512(:,:,3), nhel, den(20))
    call cont_QA(nsync, wf32(:,3), wf16(:,8), A(:,4), n3(:,25), t3x512(:,:,4), nhel, den(24))
    call cont_QA(nsync, wf32(:,2), wf16(:,10), A(:,5), n3(:,26), t3x512(:,:,5), nhel, den(28))
    call cont_QA(nsync, wf32(:,2), wf16(:,12), A(:,6), n3(:,27), t3x512(:,:,6), nhel, den(32))
    call cont_QA(nsync, wf32(:,2), wf16(:,14), A(:,7), n3(:,28), t3x512(:,:,7), nhel, den(36))

end subroutine diagrams


elemental function diagmap(j, n)
  implicit none
  integer, intent(in) :: j, n
  complex(REALKIND) :: diagmap
  diagmap = A(j,n)%j
end function diagmap

function diagsum(j, pos, neg)
  implicit none
  integer, intent(in) :: j, pos(:), neg(:)
  complex(REALKIND) :: diagsum
  diagsum = sum(diagmap(j, pos)) - sum(diagmap(j, neg))
end function diagsum

subroutine colourvector(A, j, M1)
  implicit none
  type(polcont) :: A(:,:)
  integer, intent(in) :: j
  complex(REALKIND), intent(out) :: M1(4) ! M1helarray(4,512)
  integer :: empty(0)

  M1(1) = ((-A(j,6)%j-A(j,7)%j)*f(1))/6._/**/REALKIND
  M1(2) = ((A(j,1)%j+A(j,2)%j+A(j,7)%j)*f(1))/2._/**/REALKIND+(CI*A(j,5)%j*f(2))/2._/**/REALKIND
  M1(3) = ((A(j,3)%j+A(j,4)%j+A(j,6)%j)*f(1))/2._/**/REALKIND-(CI*A(j,5)%j*f(2))/2._/**/REALKIND
  M1(4) = ((-A(j,1)%j-A(j,2)%j-A(j,3)%j-A(j,4)%j)*f(1))/6._/**/REALKIND

end subroutine colourvector


! **********************************************************************
subroutine colint(M, M2colint, extcombs)
! M(i)   = <M|Ci> colour component of matrix element
! COLINT = <M|M>
!        = Sum_{i,j} <M|Ci> * <Ci|Cj> * <Cj|M>
!        = colour-summed squared matrix element
! K1(i,j) = <Ci|Cj>
! M2colint is an array which contains the colour interference for each colour matrix
! The elements of the array extcombs specifies for which external particle
! combinations the colour correlations will be calculated. For particles i,j: i*(i-1)/2+j
! i=j=0 -> 0 means no colour insertion.
! **********************************************************************
  use ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND, only: K1
  implicit none

  complex(REALKIND), intent(in)  :: M(4)
  real(REALKIND),    intent(out) :: M2colint(0:1-1)
  integer,           intent(in)  :: extcombs(:)
  integer :: extcomb, colmatpos, i, j, eco

  M2colint = 0

  do eco = 1, size(extcombs)
    extcomb = extcombs(eco)
    colmatpos = 4*extcomb
    do i = 1, 4
      do j = 1, 4
        M2colint(extcomb) = M2colint(extcomb) + real(conjg(M(i))*K1(i+colmatpos,j)*M(j))
      end do
    end do
  end do

end subroutine colint


! **********************************************************************
subroutine colint2(M1, M2, M2colint, extcombs)
! M1(i)   = <M1|Ci> colour component of matrix element
! M2(i)   = <M2|Ci> colour component of matrix element
! COLINT = <M|M>
!        = Sum_{i,j} <M1|Ci> * <Ci|Cj> * <Cj|M2>
!        = colour-summed squared matrix element
! K1(i,j) = <Ci|Cj>
! M2colint is an array which contains the colour interference for each colour matrix
! The elements of the array extcombs specifies for which external particle
! combinations the colour correlations will be calculated. For particles i,j: i*(i-1)/2+j
! i=j=0 -> 0 means no colour insertion.
! **********************************************************************
  use ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND, only: K1
  implicit none

  complex(REALKIND), intent(in)  :: M1(4), M2(4)
  real(REALKIND),    intent(out) :: M2colint(0:1-1)
  integer,           intent(in)  :: extcombs(:)
  integer :: extcomb, colmatpos, i, j, eco

  M2colint = 0

  do eco = 1, size(extcombs)
    extcomb = extcombs(eco)
    colmatpos = 4*extcomb
    do i = 1, 4
      do j = 1, 4
        M2colint(extcomb) = M2colint(extcomb) + real(conjg(M1(i))*K1(i+colmatpos,j)*M2(j))
      end do
    end do
  end do

end subroutine colint2


! **********************************************************************
subroutine colintmunu(M1, M2, M2colint)
! M1(i)    = <M1|Ci> colour component of matrix element
! M2(i)    = <M2|Ci> colour component of matrix element
! M2colint = <M1|M2>
!          = Sum_{i,j} <M1|Ci> * <Ci|Cj> * <Cj|M2>
!          = colour-summed squared matrix element
! K1(i,j) = <Ci|Cj>
! **********************************************************************
  use ol_colourmatrix_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND, only: K1
  implicit none

  complex(REALKIND), intent(in)  :: M1(4)
  complex(REALKIND), intent(in)  :: M2(4)
  complex(REALKIND),    intent(out) :: M2colint
  integer :: i, j

  M2colint = 0

  do i = 1, 4
    do j = 1, 4
      M2colint = M2colint + M1(i)*K1(i,j)*conjg(M2(j))
    end do
  end do

end subroutine colintmunu

end subroutine amp2


#ifdef PRECISION_dp
subroutine colourvector(M1out, nhelout) &
  & bind(c,name="ol_tree_colvect_ppllllbb_split_nexnmemxuuxbbxg_4")
  ! Retrieve M1helarr from cache. Cache must have been filled before by calling amp2().
  ! [out] M1out(:,h): colour vector for each helicity configuration h
  ! [out] nhelout: number of non-vanishing helicity configurations
  implicit none
  complex(DREALKIND) :: M1out(4,512)
  integer, intent(out) :: nhelout
  M1out = M1helarr
  nhelout = nhel
end subroutine colourvector
#endif


! =================================================== !
! Only interfaces for easier usage of AMP2_<procname> !
! =================================================== !

#ifdef PRECISION_dp
subroutine amp2tree(P, M2) &
    & bind(c,name="ol_f_amp2tree_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2tree(P, M2)
#endif
  ! This is an interface for AMP2.
  ! Calculates the squared matrix element without fuss.
  implicit none
  real(DREALKIND), intent(in)  :: P(0:3,9)
  real(REALKIND),  intent(out) :: M2
  real(REALKIND) :: M2tmp(0:1-1)
  real(REALKIND) :: M2munu(4,4)
  call amp2(P, M2tmp, 0, [ 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND ], 1, [ 0 ], M2munu)
  M2 = M2tmp(0)
end subroutine amp2tree


#ifdef PRECISION_dp
subroutine amp2ccone(P, M2, I, J) &
    & bind(c,name="ol_f_amp2ccone_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2ccone(P, M2, I, J)
#endif
  ! This is an interface for AMP2.
  ! Calculates the squared matrix for the colour correlation matrix for particles I and J.
  implicit none
  real(DREALKIND), intent(in)  :: P(0:3,9)
  real(REALKIND),  intent(out) :: M2
  integer,         intent(in)  :: I, J
  integer        :: extcomb
  real(REALKIND) :: M2tmp(0:1-1)
  real(REALKIND) :: M2munu(4,4)
  if (J <= I) then
    extcomb = I*(I-1)/2 + J
  else
    extcomb = J*(J-1)/2 + I
  end if
  call amp2(P, M2tmp, 0, [ 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND ], 1, [ extcomb ], M2munu)
  M2 = M2tmp(extcomb)
end subroutine amp2ccone


#ifdef PRECISION_dp
subroutine amp2ccall(P, M2) &
    & bind(c,name="ol_f_amp2ccall_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2ccall(P, M2)
#endif
  ! This is an interface for AMP2.
  ! Calculates the squared matrix for all colour correlation matrices.
  ! The correlation between particles i and j is at position i*(i-1)/2+j of the array M2.
  ! M2(0) is AMP2tree
  implicit none
  real(DREALKIND), intent(in)  :: P(0:3,9)
  real(REALKIND),  intent(out) :: M2(0:1-1)
  real(REALKIND) :: M2munu(4,4)
  integer :: k
  call amp2(P, M2, 0, [ 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND, 0._/**/REALKIND ], &
    1, [ (k, k = 0, 1-1) ], M2munu)
end subroutine amp2ccall


#ifdef PRECISION_dp
subroutine amp2hcone(P, M2, I, J, MOM) &
    & bind(c,name="ol_f_amp2hcone_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2hcone(P, M2, I, J, MOM)
#endif
  ! This is an interface for AMP2.
  ! Calculates the helicity correlation for emitter I with momentum MOM and spectator J
  implicit none
  real(DREALKIND), intent(in)  :: P(0:3,9)
  real(REALKIND),  intent(out) :: M2
  integer,         intent(in)  :: I, J
  real(REALKIND),  intent(in)  :: MOM(0:3)
  integer        :: extcomb
  real(REALKIND) :: M2tmp(0:1-1)
  real(REALKIND) :: M2munu(4,4)
  if (J <= I) then
    extcomb = I*(I-1)/2 + J
  else
    extcomb = J*(J-1)/2 + I
  end if
  call amp2(P, M2tmp, I, MOM, 1, [ extcomb ], M2munu)
  M2 = M2tmp(extcomb)
end subroutine amp2hcone


#ifdef PRECISION_dp
subroutine amp2hcall(P, M2, I, MOM) &
    & bind(c,name="ol_f_amp2hcall_ppllllbb_split_nexnmemxuuxbbxg_4")
#else
subroutine amp2hcall(P, M2, I, MOM)
#endif
  ! This is an interface for AMP2.
  ! Calculates all helicity correlations for emitter I with momentum MOM.
  ! The correlator for spectator j is at position j of the array M2.
  implicit none
  real(DREALKIND), intent(in)  :: P(0:3,9)
  real(REALKIND),  intent(out) :: M2(9)
  integer,         intent(in)  :: I
  real(REALKIND),  intent(in)  :: MOM(0:3)
  real(REALKIND) :: M2tmp(0:1-1)
  real(REALKIND) :: M2munu(4,4)
  integer        :: J, extcombs(9)
  do J = 1, 9
    if (J <= I) then
      extcombs(J) = I*(I-1)/2 + J
    else
      extcombs(J) = J*(J-1)/2 + I
    end if
  end do
  call amp2(P, M2tmp, I, MOM, 9,extcombs, M2munu)
  do J = 1, 9
    M2(J) = M2tmp(extcombs(J))
  end do
end subroutine amp2hcall


#ifdef PRECISION_dp

subroutine amp2tree_c(p, m2) &
    & bind(c,name="ol_amp2tree_ppllllbb_split_nexnmemxuuxbbxg_4")
  use, intrinsic :: iso_c_binding, only: c_double
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  f_p = p
  call amp2tree(f_p, f_m2)
  m2 = f_m2
end subroutine amp2tree_c

subroutine amp2ccone_c(p, m2, i, j) &
    & bind(c,name="ol_amp2ccone_ppllllbb_split_nexnmemxuuxbbxg_4")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  integer(c_int), intent(in) :: i, j
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  integer :: f_i, f_j
  f_p = p
  f_i = i
  f_j = j
  call amp2ccone(f_p, f_m2, f_i, f_j)
  m2 = f_m2
end subroutine amp2ccone_c

subroutine amp2ccall_c(p, m2) &
    & bind(c,name="ol_amp2ccall_ppllllbb_split_nexnmemxuuxbbxg_4")
  use, intrinsic :: iso_c_binding, only: c_double
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2(0:1-1)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2(0:1-1)
  f_p = p
  call amp2ccall(f_p, f_m2)
  m2 = f_m2
end subroutine amp2ccall_c

subroutine amp2hcone_c(p, m2, i, j, mom) &
    & bind(c,name="ol_amp2hcone_ppllllbb_split_nexnmemxuuxbbxg_4")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  integer(c_int), intent(in) :: i, j
  real(c_double), intent(in) :: mom(0:3)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  integer :: f_i, f_j
  real(DREALKIND) :: f_mom(0:3)
  f_p = p
  f_i = i
  f_j = j
  f_mom = mom
  call amp2hcone(f_p, f_m2, f_i, f_j, f_mom)
  m2 = f_m2
end subroutine amp2hcone_c

subroutine amp2hcall_c(p, m2, i, mom) &
    & bind(c,name="ol_amp2hcall_ppllllbb_split_nexnmemxuuxbbxg_4")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2(9)
  integer(c_int), intent(in) :: i
  real(c_double), intent(in) :: mom(0:3)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2(9)
  integer :: f_i
  real(DREALKIND) :: f_mom(0:3)
  f_p = p
  f_i = i
  f_mom = mom
  call amp2hcall(f_p, f_m2, f_i, f_mom)
  m2 = f_m2
end subroutine amp2hcall_c



! Only for compatibility with the old interface
subroutine amp2tree_legacy(p, m2) &
    & bind(c,name="amp2tree_ppllllbb_split_nexnmemxuuxbbxg_4_")
  use, intrinsic :: iso_c_binding, only: c_double
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  f_p = p
  call amp2tree(f_p, f_m2)
  m2 = f_m2
end subroutine amp2tree_legacy

subroutine amp2ccone_legacy(p, m2, i, j) &
    & bind(c,name="amp2ccone_ppllllbb_split_nexnmemxuuxbbxg_4_")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  integer(c_int), intent(in) :: i, j
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  integer :: f_i, f_j
  f_p = p
  f_i = i
  f_j = j
  call amp2ccone(f_p, f_m2, f_i, f_j)
  m2 = f_m2
end subroutine amp2ccone_legacy

subroutine amp2ccall_legacy(p, m2) &
    & bind(c,name="amp2ccall_ppllllbb_split_nexnmemxuuxbbxg_4_")
  use, intrinsic :: iso_c_binding, only: c_double
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2(0:1-1)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2(0:1-1)
  f_p = p
  call amp2ccall(f_p, f_m2)
  m2 = f_m2
end subroutine amp2ccall_legacy

subroutine amp2hcone_legacy(p, m2, i, j, mom) &
    & bind(c,name="amp2hcone_ppllllbb_split_nexnmemxuuxbbxg_4_")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2
  integer(c_int), intent(in) :: i, j
  real(c_double), intent(in) :: mom(0:3)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2
  integer :: f_i, f_j
  real(DREALKIND) :: f_mom(0:3)
  f_p = p
  f_i = i
  f_j = j
  f_mom = mom
  call amp2hcone(f_p, f_m2, f_i, f_j, f_mom)
  m2 = f_m2
end subroutine amp2hcone_legacy

subroutine amp2hcall_legacy(p, m2, i, mom) &
    & bind(c,name="amp2hcall_ppllllbb_split_nexnmemxuuxbbxg_4_")
  use, intrinsic :: iso_c_binding, only: c_double, c_int
  implicit none
  real(c_double), intent(in)  :: p(0:3,9)
  real(c_double), intent(out) :: m2(9)
  integer(c_int), intent(in) :: i
  real(c_double), intent(in) :: mom(0:3)
  real(DREALKIND) :: f_p(0:3,9)
  real(DREALKIND) :: f_m2(9)
  integer :: f_i
  real(DREALKIND) :: f_mom(0:3)
  f_p = p
  f_i = i
  f_mom = mom
  call amp2hcall(f_p, f_m2, f_i, f_mom)
  m2 = f_m2
end subroutine amp2hcall_legacy

#endif

end module ol_tree_ppllllbb_split_nexnmemxuuxbbxg_4_/**/REALKIND
