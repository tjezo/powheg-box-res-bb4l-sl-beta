      subroutine init_processes_wdec
      use openloops, only: set_parameter
      implicit none
      include "nlegborn.h"
      include "pwhg_flst.h"
      integer i,j
      logical, save :: ini = .true.
      integer, save :: whichw
      real * 8 powheginput
      external powheginput
      integer flstnreal, flstreallen(maxprocreal),flstreal(nlegreal,maxprocreal),flstrealres(nlegreal,maxprocreal) ! copies of flst_(n)real*
      integer reallen
      call set_parameter("flavour_mapping", 1)
      if (ini) then
         whichw = powheginput("whichW")
         if (whichw .ne. -1 .and. whichw .ne. 1) then
            print*,"init_processes_wdec: Illegal value for whichW = ",whichw
            print*,"Allowed values are +1 and -1. Exiting!"
            call exit(-1)
         endif
         ini = .false.
      endif
c     take the existing flst_born and replace one W decay products with quarks
      do i=1,flst_nborn
         call relabel(flst_bornlength(i), flst_born(:,i), flst_bornres(:,i), whichW, flst_born(:,i))
      enddo

c     do the same for flst_real, but also discard topologies in which the extra emission originates from top or anti-top while
c     turning the emission from production into an emission from the W, discard also the gq channel, in which the extra emission
c     cannot possibly originate from the W
      flstnreal = 0
      flstreal = 0
      flstrealres = 0
      flstreallen = 0
      do i=1,flst_nreal
         reallen = flst_reallength(i)
c        flavour lists with emission originating from the production (and excluding gq channels, in which case the emission cannot come from W)
         if ( flst_realres(reallen,i) == 0 .and. ABS(flst_real(1,i)) == ABS(flst_real(2,i)) ) then
c           relabel leptons to quarks
            call relabel(reallen-1, flst_real(:,i), flst_realres(:,i), whichW, flst_real(:,i)) !W decay products never participate in the qq > g -- qg > g crossing
c           find the W and point the emission to it (if it's a gluon)
            if (flst_real(reallen,i) == 0) then
               do j=3,reallen-1
                  if (flst_real(j,i) == whichW*24) then
                     flst_realres(reallen,i) = j
                     exit
                  endif
               enddo
            endif
            flstnreal = flstnreal + 1
            flstreal(:,flstnreal) = flst_real(:,i)
            flstrealres(:,flstnreal) = flst_realres(:,i)
            flstreallen(flstnreal) = flst_reallength(i)
         endif
      enddo
      flst_nreal = flstnreal
      flst_real = flstreal
      flst_realres = flstrealres
      flst_reallength = flstreallen
      end
