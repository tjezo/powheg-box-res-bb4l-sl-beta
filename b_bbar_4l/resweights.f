      subroutine realresweight_soft(alr,weight)
      integer alr
      real * 8 weight
      call realresweight0(0,alr,weight)
      end



      subroutine realresweight(alr,weight)
      integer alr
      real * 8 weight
      call realresweight0(1,alr,weight)
      end



      subroutine realresweight0(isoftflag,alr,weight)
      implicit none
      integer isoftflag,alr
      real * 8 weight
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      integer ihist
      integer numreshist,numsingreg,jsing,alrlen,ptr
      real * 8 res_pfactors(flst_numrhreal),res_pdfactors(flst_numrhreal),
     1     res_dijfactors(maxregions,flst_numrhreal)
      integer ubflav(flst_numfsb+2),is_fs(flst_numfsb+2),isfslength,k
      real * 8 puborn(0:3,flst_numfsb+2),pubornsum(0:3)
      integer isoftflagc
      integer itmp(1)
      common/local_isoftflagc/isoftflagc
      real * 8  dijtermx
      external :: dijtermx
      logical, save :: ini = .true.
      integer, save :: resHistStrategy
      real * 8 powheginput
      external powheginput
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      if (ini) then
         resHistStrategy = powheginput("#RHStrategy")
         if (resHistStrategy < 0) resHistStrategy = 0
         ini = .false.
      endif
      isoftflagc = isoftflag
      alrlen = flst_alrlength(alr)
      numreshist = flst_alrnumrhptrs(alr)
      if (resHistStrategy == 1) then
         call getisfsparticles(alrlen-1,flst_uborn(:,alr),flst_ubornres(:,alr),isfslength,is_fs)
         if(isfslength /= flst_numfsb+2) then
            write(*,*) ' Error in realresweight0: length mismatch'
            call exit(-1)
         endif
         pubornsum=0
         do k=1,isfslength
            ubflav(k)=flst_uborn(is_fs(k),alr)
            puborn(:,k)=kn_cmpborn(:,is_fs(k))
            pubornsum=pubornsum+puborn(:,k)
         enddo
      endif
c go through all resonance histories
      do ihist = 1,numreshist
         ptr = flst_alrrhptrs(ihist,alr)
         numsingreg = flst_rhrealnumreg(ptr)
         if(numsingreg.eq.0.and.isoftflagc.eq.0) cycle
         call fillpcmblock(alrlen,flst_rhreallength(ptr),
     1                    flst_rhreal(:,ptr),
     2                    flst_rhrealres(:,ptr))
         if (numreshist > 1) then
            if (resHistStrategy == 0) then
               call comprespfactor(flst_rhreal(:,ptr),flst_rhrealres(:,ptr),res_pfactors(ihist))
            else if (resHistStrategy == 1) then
               call comprespfactorMEI(flst_rhreal(:,ptr),flst_rhrealres(:,ptr),ubflav(:isfslength),
     1           res_pfactors(ihist))
            else
               print*,'Invalid value for RHStrategy = ', resHistStrategy
               print*,'Exiting!'
               call exit(-1)
            endif
         else 
            res_pfactors(ihist) = 1
         endif
         if(numsingreg.gt.0) then
            call compresdijfactors(flst_rhreal(:,ptr),
     1           flst_rhrealres(:,ptr),
     2           numsingreg,
     3           flst_rhrealreg(:,:,ptr),
     4           res_dijfactors(:,ihist))
            res_pdfactors(ihist) = res_pfactors(ihist) *
     1           sum(res_dijfactors(1:numsingreg,ihist))
         else
            res_pdfactors(ihist) = res_pfactors(ihist) /
     1                             dijtermx(-1,-1,itmp,itmp)
         endif
      enddo

      ptr = flst_alrrhptrs(1,alr)
      numsingreg = flst_rhrealnumreg(ptr)

      do jsing = 1,numsingreg
         if((flst_rhrealreg(1,jsing,ptr) .eq. flst_emitter(alr).and.flst_rhrealreg(2,jsing,ptr).eq.alrlen)
     1  .or.(flst_rhrealreg(2,jsing,ptr) .eq. flst_emitter(alr).and.flst_rhrealreg(1,jsing,ptr).eq.alrlen)) then
            exit
         endif
      enddo
      if(jsing.eq.numsingreg+1) then
         write(*,*) ' realresweight0: cannot find current region'
         write(*,*) ' exiting ...'
         call exit(-1)
      endif
c the first element in flst_alrrhptrs corresponds to the resonance structure of
c the current alr.
      weight = res_pfactors(1)*res_dijfactors(jsing,1)/sum(res_pdfactors(1:numreshist))

      end



      subroutine regularresweight(ireg,weight)
      implicit none
      integer ireg
      real * 8 weight
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_par.h'
      integer ihist
      integer numreshist,numsingreg,reglen,ptr
      real * 8 res_pfactors(flst_numrhreal),res_pdfactors(flst_numrhreal),
     1     res_dijfactors(maxregions,flst_numrhreal)
      integer isoftflagc
      integer itmp(1)
      common/local_isoftflagc/isoftflagc
      real * 8  dijtermx
      procedure() :: dijtermx
      logical, save :: ini = .true.
      real * 8 powheginput
      external powheginput
      if (ini) then
         if ( powheginput("#RHStrategy") /= 0 ) then
            print*,'Regular resweights for strategies other than the original RHStrategy=0 have not been implemented because there are no regulars in bb4l. Exiting!'
            call exit(-1)
         endif
      endif
c This has side effects, now we need full real, no soft
      isoftflagc = 1
      reglen = flst_regularlength(ireg)
      numreshist = flst_regularnumrhptrs(ireg)
c go through all resonance histories
      do ihist = 1,numreshist
         ptr = flst_regularrhptrs(ihist,ireg)
         numsingreg = flst_rhrealnumreg(ptr)
         call fillpcmblock(reglen,flst_rhreallength(ptr),
     1                    flst_rhreal(:,ptr),
     2                    flst_rhrealres(:,ptr))
         call comprespfactor(flst_rhreal(:,ptr),flst_rhrealres(:,ptr),res_pfactors(ihist))
         call compresdijfactors(flst_rhreal(:,ptr),
     1                          flst_rhrealres(:,ptr),
     2                          numsingreg,
     3                          flst_rhrealreg(:,:,ptr),
     4                          res_dijfactors(:,ihist))
         if(numsingreg.gt.0) then
            res_pdfactors(ihist) = res_pfactors(ihist) *
     1           sum(res_dijfactors(1:numsingreg,ihist))
         else
c dijterm called witjh -1 yields a typical scale of the process
            res_pdfactors(ihist) = res_pfactors(ihist) /
     1                       dijtermx(-1,-1,itmp,itmp)
         endif
      enddo

c the first element in flst_regularrhptrs corresponds to the resonance structure of
c the current reg.
      weight = res_pfactors(1)/ dijtermx(-1,-1,itmp,itmp)/
     1         sum(res_pdfactors(1:numreshist))

      end



      subroutine fillpcmblock(alrlen,alen,a,ares)
      implicit none
      integer alrlen,alen,a(alen),ares(alen)
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'      
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer isoftflagc
      common/local_isoftflagc/isoftflagc
      integer firstfs,firstfsalr,k,moth
      pcmlen = alen
      firstfs = alen - flst_numfsr + 1
      firstfsalr = alrlen - flst_numfsr + 1
      if(isoftflagc.eq.0) then
c soft limit case
         pcm(:,1:2) = kn_cmpborn(:,1:2)
         pcm(:,firstfs:alen-1) = kn_cmpborn(:,firstfsalr:alrlen-1)
         pcm(:,alen) = kn_softvec
         if(firstfs >=4) then
            pcm(:,3:firstfs-1) = 0         
         endif
      else
         pcm(:,1:2) = kn_cmpreal(:,1:2)
         pcm(:,firstfs:alen) = kn_cmpreal(:,firstfsalr:alrlen)
         if(firstfs >=4) then
            pcm(:,3:firstfs-1) = 0
         endif
      endif
c Sum momenta of each final state particle to all its ancestors.
      do k=firstfs,alen
         if(isoftflagc.eq.0.and.k.eq.alen) cycle
         moth = ares(k)
         do while(moth.ne.0)
            pcm(:,moth) = pcm(:,moth) + pcm(:,k)
            moth = ares(moth)
         enddo
      enddo
      end



      subroutine comprespfactor(a,ares,factor)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_physpar.h'
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer a(pcmlen), ares(pcmlen)
      integer firstfs,firstfs0,k
      real * 8 factor
      real * 8 mass,width,virt
      real * 8 momsq03
      external momsq03
      logical, save :: ini = .true.
      logical, save :: improvedProjector
      real * 8, save :: improvedProjectorR
      logical, save :: improvedProjectorUB
      real * 8 powheginput
      external powheginput
      real * 8 et
      integer i_bot, i_abot
      real * 8 pbsys(0:3)
      integer moth
      real * 8 pcmsave(0:3,1:nlegreal)
      integer isoftflagc
      common/local_isoftflagc/isoftflagc
      if (ini) then
         improvedProjector = powheginput("#RHImprovedProj").eq.1
         improvedProjectorR = powheginput("#RHImprovedProjR")
         if (improvedProjectorR < 0) improvedProjectorR = 8d0
         improvedProjectorUB = powheginput("#RHImprovedProjUB").eq.1
         ini = .false.
      endif
      factor = 1
      firstfs = pcmlen - flst_numfsr + 1
c User can choose to use underlying born momenta at this step
      if (improvedProjectorUB) then
c save pcm away in pcmsave
         pcmsave = pcm
         firstfs0 = flst_ibornlength - flst_numfsb + 1
         pcm(:,1:2) = kn_cmpborn(:,1:2)
         pcm(:,firstfs:pcmlen-1) = kn_cmpborn(:,firstfs0:flst_ibornlength)
         pcm(:,3:firstfs-1) = 0
         pcm(:,pcmlen) = 0 
c Sum momenta of each final state particle to all its ancestors.
         do k=firstfs,pcmlen-1
            moth = ares(k)
            do while(moth.ne.0)
               pcm(:,moth) = pcm(:,moth) + pcm(:,k)
               moth = ares(moth)
            enddo
         enddo
      endif
c Now all resonances have the correct 4-momentum
      do k = 3,firstfs-1
         mass = physpar_pdgmasses(a(k))
         width = physpar_pdgwidths(a(k))
         virt = momsq03(pcm(:,k))
         factor = factor*mass**4/((virt-mass**2)**2 + (mass*width)**2)
      enddo
      if (improvedProjector) then
c find bottom
         i_bot = 0
         i_abot = 0
         do k = firstfs,pcmlen
            if (a(k) == 5) i_bot = k
            if (a(k) == -5) i_abot = k
         enddo
         if (a(3) == -6 .and. a(4) == 6) then
c do nothing
         else if (a(3) == -6 .and. a(4) == -24) then
            pbsys = pcm(:,i_bot)
c           add radiation momentum to it, unless we are calculating the soft limit
c           if that is the case the kn_softvec is not correctly rotated and spoils 
c           the soft limit
            if (isoftflagc .gt. 0) then
               if (ares(pcmlen) == 3) then
                  pbsys = pbsys + pcm(:,pcmlen)
               endif
            endif
            call gettransmass(physpar_pdgmasses(5), pbsys, et)
            factor = factor*physpar_pdgmasses(6)**2/et**2/improvedProjectorR
         else if (a(3) == 6 .and. a(4) == -24) then
            pbsys = pcm(:,i_abot)
c           add radiation momentum to it, unless we are calculating the soft limit
c           if that is the case the kn_softvec is not correctly rotated and spoils 
c           the soft limit
            if (isoftflagc .gt. 0) then
               if (ares(pcmlen) == 3) then
                  pbsys = pbsys + pcm(:,pcmlen)
               endif
            endif
            call gettransmass(physpar_pdgmasses(5), pbsys, et)
            factor = factor*physpar_pdgmasses(6)**2/et**2/improvedProjectorR
         else 
            print*,'Unimplemented case. Exiting!'
            call exit(-1)
         endif
      endif
c restore the pcm array
      if (improvedProjectorUB) pcm=pcmsave
      end



      subroutine comprespfactorMEI(a,ares,ubflav,factor)
      use openloops_powheg, only: openloops_born
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_physpar.h'
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer a(pcmlen),ares(pcmlen)
      integer ubflav(flst_numfsb+2), k
      real * 8 puborn(0:3,flst_numfsb+2)
      real * 8 factor
      integer isoftflagc
      common/local_isoftflagc/isoftflagc
      integer nextpuborn, i
c     Are we in the soft limit? In that case we don't need to do anything
      if (isoftflagc == 0) then
c        the underlying born pcm is simply a copy of the real pcm, with the extra emission removed
         puborn = 0
         nextpuborn = 0
         do i=1,pcmlen-1
            if ( abs(a(i)) /= 23 .and. abs(a(i)) /= 24 .and. abs(a(i)) /= 6 ) then
               nextpuborn = nextpuborn + 1
               puborn(:,nextpuborn) = pcm(:,i)
            endif
         enddo
c     Otherwise let us map ourselves back to the underlying momentum (via an inverse mapping, which must be dependent on the real kinematics)
      else 
         if (ares(pcmlen) == 0) then
            call inverseMappingISR(a, puborn)
         else
            call inverseMappingFSR(a, ares, puborn)
         endif
      endif
      if (a(3) == -6 .and. a(4) == 6) then
         call openloops_born(puborn,ubflav,factor,approx='pole_tt')
      else if (a(3) == 6 .and. a(4) == -24) then
         call openloops_born(puborn,ubflav,factor,approx='pole_tw')
      else if (a(3) == -6 .and. a(4) == -24) then
         call openloops_born(puborn,ubflav,factor,approx='pole_tbw')
      else 
         call exit(-1)
      endif
      end



      subroutine compresdijfactors(a,ares,nregions,regions,factors)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer a(pcmlen),ares(pcmlen),nregions,regions(2,nregions)
      real * 8 factors(nregions)
      real * 8  dijtermx
      external dijtermx
      integer iregion,em1,em2
      integer isoftflagc
      common/local_isoftflagc/isoftflagc
      do iregion = 1,nregions
         em1 = regions(1,iregion)
         em2 = regions(2,iregion)
         if(isoftflagc.eq.0) then
            if(em2 .eq. pcmlen) then
               factors(iregion) = 1/dijtermx(em1,em2,a,ares)
            elseif(em1.eq.pcmlen) then
               factors(iregion) = 1/dijtermx(em2,em1,a,ares)
            else
               factors(iregion) = 0
            endif
         else
            factors(iregion) = 1/dijtermx(em1,em2,a,ares)
         endif
      enddo
      end



      subroutine bornresweight(iborn,weight)
      implicit none
      integer iborn
      real * 8 weight
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      integer len0,len,numreshist,ihist,ptr
      real * 8 factor,factortmp,factortot
      logical, save :: ini = .true.
      integer, save :: resHistStrategy, interfInProjs
      real * 8 powheginput
      external powheginput
      real * 8 pborn(0:3,flst_numfsb+2)
      integer bflav(nlegborn),is_fs(nlegborn),isfslength
      integer firstfs,firstfs0,moth,k
      real * 8 pcm(0:3,1:nlegreal)
      if (ini) then
         resHistStrategy= powheginput("#RHStrategy")
         interfInProjs = powheginput("#RHInterfInProjs")
         ini = .false.
      endif

      factortot = 0
c      len0 = flst_bornlength(iborn)
c     the above is wrong, here we need the length of the born for which the phase space has been calculated
c     and not the one for which we are currently calculating the resweight 
      len0 = flst_ibornlength
      numreshist = flst_bornnumrhptrs(iborn)
      if (numreshist > 1) then
         do ihist=1,numreshist
            ptr = flst_bornrhptrs(ihist,iborn)
            len = flst_rhbornlength(ptr)
            if (resHistStrategy == 0) then
               call comprespfactorborn(len0,len,flst_rhborn(:,ptr),flst_rhbornres(:,ptr),factortmp)
            else if (resHistStrategy == 1) then
               call comprespfactorbornMEI(len0,len,flst_rhborn(:,ptr),flst_rhbornres(:,ptr),factortmp)
            endif
            if(ihist.eq.1) factor = factortmp
            if (resHistStrategy == 1 .and. interfInProjs == 2) then
               call comprespfactorbornMEITOT(len0,len,flst_rhborn(:,ptr),flst_rhbornres(:,ptr),factortot)
            else
               factortot = factortot + factortmp
            endif
         enddo
         weight = factor / factortot
      else 
         weight = 1
      endif
      end



      subroutine comprespfactorborn(len0,lenb,a,ares,factor)
      implicit none
      integer len0,lenb,a(lenb),ares(lenb)
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_physpar.h'
      real * 8 factor
      integer firstfs,firstfs0,moth,k
      real * 8 mass,width,virt,pcm(0:3,1:lenb)
      real * 8 momsq03
      external momsq03
      real * 8 powheginput
      external powheginput
      logical, save :: ini = .true.
      logical, save :: improvedProjector
      real * 8, save :: improvedProjectorR
      real * 8 et
      integer i_bot, i_abot
      if (ini) then
         improvedProjector = powheginput("#RHImprovedProj").eq.1
         improvedProjectorR = powheginput("#RHImprovedProjR")
         if (improvedProjectorR < 0) improvedProjectorR = 8d0
         ini = .false.
      endif
      factor = 1
      firstfs = lenb - flst_numfsb + 1
      firstfs0 = len0 - flst_numfsb + 1
      pcm(:,1:2) = kn_cmpborn(:,1:2)
      pcm(:,firstfs:lenb) = kn_cmpborn(:,firstfs0:len0)
      pcm(:,3:firstfs-1) = 0
c Sum momenta of each final state particle to all its ancestors.
      do k=firstfs,lenb
         moth = ares(k)
         do while(moth.ne.0)
            pcm(:,moth) = pcm(:,moth) + pcm(:,k)
            moth = ares(moth)
         enddo
      enddo
c Now all resonances have the correct 4-momentum
      do k = 3,firstfs-1
         mass = physpar_pdgmasses(a(k))
         width = physpar_pdgwidths(a(k))
         virt = momsq03(pcm(:,k))
         factor = factor*mass**4/((virt-mass**2)**2 + (mass*width)**2)
      enddo
      if (improvedProjector) then
c find bottom
         i_bot = 0
         i_abot = 0
         do k = firstfs,lenb
            if (a(k) == 5) i_bot = k
            if (a(k) == -5) i_abot = k
         enddo
         if (a(3) == -6 .and. a(4) == 6) then
c do nothing
         else if (a(3) == -6 .and. a(4) == -24) then
c            call gettransmass(physpar_pdgmasses(5), pcm(:,i_abot), et)
            call gettransmass(physpar_pdgmasses(5), pcm(:,i_bot), et)
            factor = factor*physpar_pdgmasses(6)**2/et**2/improvedProjectorR
         else if (a(3) == 6 .and. a(4) == -24) then
c            call gettransmass(physpar_pdgmasses(5), pcm(:,i_bot), et)
            call gettransmass(physpar_pdgmasses(5), pcm(:,i_abot), et)
            factor = factor*physpar_pdgmasses(6)**2/et**2/improvedProjectorR
         endif
      endif
      end


      subroutine comprespfactorbornMEI(len0,lenb,a,ares,factor)
      use openloops_powheg, only: openloops_born
      implicit none
      integer len0,lenb,a(lenb),ares(lenb)
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_physpar.h'
      real * 8 factor,factortmp1, factortmp2
      integer firstfs,firstfs0,moth,k
      real * 8 mass,width,virt,pcm(0:3,1:lenb)
      real * 8 momsq03
      external momsq03
      real * 8 pborn(0:3,flst_numfsb+2), sump(0:3)
      integer bflav(nlegborn),is_fs(nlegborn),isfslength
      logical, save :: ini = .true.
      integer, save :: interfInProjs
      real * 8 powheginput
      external powheginput
      if (ini) then
         interfInProjs = powheginput("#RHInterfInProjs")
         ini = .false.
      endif
      factor = 1
      firstfs = lenb - flst_numfsb + 1
      firstfs0 = len0 - flst_numfsb + 1
      pcm(:,1:2) = kn_cmpborn(:,1:2)
      pcm(:,firstfs:lenb) = kn_cmpborn(:,firstfs0:len0)
      pcm(:,3:firstfs-1) = 0
c Sum momenta of each final state particle to all its ancestors.
      do k=firstfs,lenb
         moth = ares(k)
         do while(moth.ne.0)
            pcm(:,moth) = pcm(:,moth) + pcm(:,k)
            moth = ares(moth)
         enddo
      enddo
c Depending on the resonance history, calculate the corresponding born
      call getisfsparticles(lenb,a,ares,isfslength,is_fs)
      if(isfslength /= flst_numfsb+2) then
         write(*,*) ' Error in comprespfactorborn: length mismatch'
         call exit(-1)
      endif
      do k=1,isfslength
         bflav(k)=a(is_fs(k))
         pborn(:,k)=pcm(:,is_fs(k))
      enddo
      if (a(3) == -6 .and. a(4) == 6) then
         if (interfInProjs == 1) then
            call openloops_born(pborn,bflav(1:isfslength),factor)
            call openloops_born(pborn,bflav(1:isfslength),factortmp1,approx='pole_tw')
            call openloops_born(pborn,bflav(1:isfslength),factortmp2,approx='pole_tbw')
            factor = factor - factortmp1 - factortmp2 ! so the tt projector contains the interference terms
         else 
            call openloops_born(pborn,bflav(1:isfslength),factor,approx='pole_tt')
         endif
      else if (a(3) == 6 .and. a(4) == -24) then
         call openloops_born(pborn,bflav(1:isfslength),factor,approx='pole_tw')
      else if (a(3) == -6 .and. a(4) == -24) then
         call openloops_born(pborn,bflav(1:isfslength),factor,approx='pole_tbw')
      endif
      end

      subroutine comprespfactorbornMEITOT(len0,lenb,a,ares,factor)
      use openloops_powheg, only: openloops_born
      implicit none
      integer len0,lenb,a(lenb),ares(lenb)
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_physpar.h'
      real * 8 factor,factortmp1, factortmp2
      integer firstfs,firstfs0,moth,k
      real * 8 mass,width,virt,pcm(0:3,1:lenb)
      real * 8 momsq03
      external momsq03
      real * 8 pborn(0:3,flst_numfsb+2), sump(0:3)
      integer bflav(nlegborn),is_fs(nlegborn),isfslength
      factor = 1
      firstfs = lenb - flst_numfsb + 1
      firstfs0 = len0 - flst_numfsb + 1
      pcm(:,1:2) = kn_cmpborn(:,1:2)
      pcm(:,firstfs:lenb) = kn_cmpborn(:,firstfs0:len0)
cDBG      do k=1,len0
cDBG         print*,k,kn_cmpborn(:,k)
cDBG      enddo
      pcm(:,3:firstfs-1) = 0
c Sum momenta of each final state particle to all its ancestors.
      do k=firstfs,lenb
         moth = ares(k)
         do while(moth.ne.0)
            pcm(:,moth) = pcm(:,moth) + pcm(:,k)
            moth = ares(moth)
         enddo
      enddo
c Depending on the resonance history, calculate the corresponding born
      call getisfsparticles(lenb,a,ares,isfslength,is_fs)
      if(isfslength /= flst_numfsb+2) then
         write(*,*) ' Error in comprespfactorborn: length mismatch'
         call exit(-1)
      endif
      do k=1,isfslength
         bflav(k)=a(is_fs(k))
         pborn(:,k)=pcm(:,is_fs(k))
      enddo
      call openloops_born(pborn,bflav(1:isfslength),factor)
      end

      subroutine inverseMappingFSR(areal,ares,puborn)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_physpar.h'
      include 'pwhg_flst.h'
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer areal(pcmlen), ares(pcmlen)
      real * 8 puborn(0:3,flst_numfsb+2)
      integer ipl, ipnu, ipb, ipg
      real * 8 pb(0:3), pl(0:3), pnu(0:3), pg(0:3), pbNEW(0:3), plNEW(0:3), pnuNEW(0:3)
      real * 8 pt(0:3), pTt, et, pLt, mtSQR
      real * 8 pbg(0:3), pTbg, ebg, ETbg, ETbg1, ETbg2, mbgSQR 
      real * 8 pTb, pTb1, pTb2, pLb, pLb1, pLb2, eb, eb1, eb2, mbSQR
      real * 8 pw(0:3), pwNEW(0:3), mwSQR
      real * 8 cosphi, cosphiorig, cosphi1, cosphi2, theta, thetabackup1, thetabackup2, thetabackup3
      real * 8 DmtwbSQR, A, B, B1, B2, B11, B12, Bold, C, AA, BB, CC, AAA, BBB, CCC
      real * 8 plep(0:3,2), ptemp(0:3)
      integer, save :: ncalls = 0
      integer nres
      integer i
      logical equal, checkMomentumConservation
c     This function maps n+1-particle phase space into an n-particle 
c     preserving all momenta outside the radiating top decay system
c     and in the radiating top decay system it preserves:
c     1.) the four-momentum of the top-quark 
c     2.) W virtuality
c     3.) transverse energy of the bg system (which in the n-particle system
c         becomes the b quark), if possible
c     4.) the direction of the bg system (as above), if possible

c     Count the number of calls for debugging purposes
      ncalls = ncalls + 1
c      if (ncalls == 3516) then
c         print*,'hura'
c      endif
c     Reset puborn
      puborn = 0
      puborn(:,1) = pcm(:,1)
      puborn(:,2) = pcm(:,2)
c     Depending on the resonance history, mark momenta participating in the mapping
c     and copy the other ones
      if (areal(3) == -6 .and. areal(4) == 6) then
         nres = 4
         if (ares(pcmlen) == 3) then
            puborn(:,7-nres) = pcm(:,7)
            puborn(:,8-nres) = pcm(:,8)
            ipl = 9
            ipnu = 10
            puborn(:,11-nres) = pcm(:,11)
            ipb = 12
            ipg = 13
         else if (ares(pcmlen) == 4) then
            ipl = 7
            ipnu = 8
            puborn(:,9-nres) = pcm(:,9)
            puborn(:,10-nres) = pcm(:,10)
            ipb = 11
            puborn(:,12-nres) = pcm(:,12)
            ipg = 13
         endif
      else if (areal(3) == 6 .and. areal(4) == -24) then
         nres = 3
         if (ares(pcmlen) == 3) then
            ipl = 6
            ipnu = 7
            puborn(:,8-nres) = pcm(:,8)
            puborn(:,9-nres) = pcm(:,9)
            ipb = 10
            puborn(:,11-nres) = pcm(:,11)
            ipg = 12
         endif
      else if (areal(3) == -6 .and. areal(4) == -24) then
         nres = 3
         if (ares(pcmlen) == 3) then
            puborn(:,6-nres) = pcm(:,6)
            puborn(:,7-nres) = pcm(:,7)
            ipl = 8
            ipnu = 9
            puborn(:,10-nres) = pcm(:,10)
            ipb = 11
            ipg = 12
         endif
      endif
c     Collect the momenta
      pl = pcm(:,ipl)
      pnu = pcm(:,ipnu)
      pb = pcm(:,ipb)
      pg = pcm(:,ipg)
c     Construct W momentum and calculate quantities required later
      pw = pl+pnu
      mwSQR = pw(0)**2-pw(1)**2-pw(2)**2-pw(3)**2
c     Construct top momentum and calculate quantities required later
      pt = pb+pw+pg
      pTt = sqrt(pt(1)**2+pt(2)**2)
      et = pt(0)
      pLt = pt(3)
      mtSQR = et**2-pTt**2-pLt**2
c     Construct bottom-gluon system and calculate quantities required later
      pbg = pb+pg
      ebg = pbg(0)
      pTbg = sqrt(pbg(1)**2+pbg(2)**2)
      mbgSQR = ebg**2-pTbg**2-pbg(3)**2
      ETbg = sqrt(mbgSQR+pTbg**2)
c     Find the relative transverse angle of the top and the bg system
      cosphi = (pt(1)*pbg(1)+pt(2)*pbg(2))/pTt/pTbg
      cosphiorig = cosphi
c     Construct bottom after the mapping
      mbSQR = physpar_pdgmasses(5)**2 ! bottom needs to be on shell
      pTb = sqrt(ETbg**2-mbSQR) ! so the amplitude of the transverse momentum changes
c     Let us solve the quadratic equation for pLb
      DmtwbSQR =mtSQR-mwSQR+mbSQR
      C = 2*(et - pLt)*(et + pLt)
c     first check that the solution exists
      B1 = (DmtwbSQR + 2*cosphi*pTb*pTt)**2
      B2 = 2*ETbg**2*C
      if ( B1 > B2 ) then
         B = et**2*(B1 - B2)
c     otherwise 
c        1.) rotate the cosphi angle 
c        2.) and if impossible, reduce the ETBg 
c        to turn B from negative to 0
      else 
         AA = -DmtwbSQR*pTb*PTt
         CC = 2*pTb**2*pTt**2
         BB = ETbg**2*pTb**2*pTt**2*(et**2 - pLt**2)
         cosphi1 = (AA + 2*sqrt(BB))/CC
         cosphi2 = (AA - 2*sqrt(BB))/CC
         B11 = (DmtwbSQR + 2*cosphi1*pTb*pTt)**2
         B12 = (DmtwbSQR + 2*cosphi2*pTb*pTt)**2
         if ( .not.equal((DmtwbSQR + 2*cosphi1*pTb*pTt)**2, B2, 1d-6) ) then
            print*,"inverseMappingFSR: FATAL ERROR Solution 1 for cosphi doesn't satisfy the quadratic equation. Exiting!"
cEXIT            call exit(-1)
         endif
         if ( .not.equal((DmtwbSQR + 2*cosphi2*pTb*pTt)**2, B2, 1d-6) ) then
            print*,"inverseMappingFSR: FATAL ERROR Solution 2 for cosphi doesn't satisfy the quadratic equation. Exiting!"
cEXIT            call exit(-1)
         endif
c        pick solution 1 if admissible
         if ( abs(cosphi1) <= 1 ) then
            cosphi = cosphi1
c        otherwise pick solution 2 if admissible
         else if ( abs(cosphi2) <= 1 ) then
            cosphi = cosphi2
c        it isn't possible to adjust the angle alone because neither of the solutions is in the range [-1,1]
         else 
c           adjust the ptB assuming the cosphi is 1 (which should always be possible)
c           looking at the equation, it should be obvious that the minimum ETbg reduction happens at cosphi 1
cDBG            print*,"cosphi1=",cosphi1,"cosphi2=",cosphi2
c           choose the sign according to the solution for cosphi closer to the physical one (i.e. closer to 1)
cDBG            if ( abs(cosphi1) < abs(cosphi2) ) then
cDBG               cosphi = sign(1d0, cosphi1)
cDBG            else 
cDBG               cosphi = sign(1d0, cosphi2)
cDBG            endif
            cosphi = sign(1d0, cosphiorig)
c            cosphi = 1
cThe following were derived assuming cosphi=1
cOLD            AAA = 2*DmtwbSQR*pTt
cOLD            BBB = 2*(C*(DmtwbSQR**2 - 2*mbSQR*(C - 2*pTt**2)))
cOLD            CCC = 2*(C - 2*pTt**2)
cHere we rederive them restoring explicit cosphi dependence, such that 
c we can also choose cosphi to be -1 (or anything else should we need it in the future)
            AAA = 2*DmtwbSQR*pTt*cosphi
            BBB = 2*(C*(DmtwbSQR**2 - 2*mbSQR*(C - 2*pTt**2*cosphi**2)))
            CCC = 2*(C - 2*pTt**2*cosphi**2)
            pTb1 = (AAA - sqrt(BBB))/CCC
            pTb2 = (AAA + sqrt(BBB))/CCC
            ETbg1 = sqrt(pTb1**2 + mbSQR)
            ETbg2 = sqrt(pTb2**2 + mbSQR)
c           Pick a solution closer to the original
            if ( abs(ETbg1 - ETbg) < abs(ETbg2 - ETbg) ) then
               ETbg = ETbg1
               pTb = pTb1
            else 
               ETbg = ETbg2
               pTb = pTb2
            endif
            if ( .not. equal((DmtwbSQR + 2*cosphi*pTb*pTt)**2, 2*ETbg**2*C,1d-6) ) then
               print*,"inverseMappingFSR: FATAL ERROR Prescription for ETbg reduction does not solve the quadratic equation! Exiting!"
cEXIT               call exit(-1)
            endif
         endif
c        in all the cases B = 0 by construction
         B = 0
      endif
      A = pLt*(DmtwbSQR+2*pTt*pTb*cosphi)
c     The quadratic equation yields two solutions
      pLb1 = (A-sqrt(B))/C
      pLb2 = (A+sqrt(B))/C
c     Let's calculate the remaining bottom related quantities
      eb1 = sqrt(ETbg**2 + pLb1**2)
      eb2 = sqrt(ETbg**2 + pLb2**2)
c     Check the solutions
      if ( .not. equal(2*(et*eb1 - pTt*pTb*cosphi - pLt*pLb1), DmtwbSQR, 1d-6) ) then
         print*,"inverseMappingFSR: FATAL ERROR Solution 1 for pLb doesn't satisfy the quadratic equation. Exiting!"
cEXIT         call exit(-1)
      endif  
      if ( .not. equal(2*(et*eb2 - pTt*pTb*cosphi - pLt*pLb2), DmtwbSQR, 1d-6) ) then
         print*,"inverseMappingFSR: FATAL ERROR Solution 2 for pLb doesn't satisfy the quadratic equation. Exiting!"
cEXIT         call exit(-1)
      endif  
c     Finally let us construct the final bottom-momentum picking the solution
c     closer to the longitudinal momentum of the original bottom-gluon system
      if ( abs(plb1 - pbg(3)) < abs(plb2 - pbg(3)) ) then
         eb = eb1
         pLb = pLb1
      else
         eb = eb2
         pLb = pLb2
      endif
      if ( .not. equal(eb**2-pTb**2-pLb**2, mbSQR, 1d-3) ) then
         print*,"inverseMappingFSR: WARNING The solution of the quadratic equation for longitudinal momentum of the bottom doesn't seem to"//
     1     "have the correct virtuality = ",sqrt(eb**2-pTb**2-pLb**2)," compared to ", sqrt(mbSQR)
         print*,"inverseMappingFSR: ncalls = ",ncalls
      endif  
c     We know the amplitude of the bottoms pT but not its direction
c     we know the angle with respect to the top direction though
c     we have to choose the right sign, we can help ourselves with the atan2 function
c     which in this setup measures acos(cosphi) but with the correct quadrant mapping
c     we still have to use cosphi though, because it may have been rescaled above
      if (atan2(pt(1)*pbg(2)-pbg(1)*pt(2),pt(1)*pbg(1)+pt(2)*pbg(2)) > 0) then
         theta = atan2(pt(2),pt(1)) + acos(cosphi)
      else 
         theta = atan2(pt(2),pt(1)) - acos(cosphi)
      endif
c      thetabackup1 = acos(cosphi)
c      thetabackup2 = atan2(pt(1)*pbg(2)-pbg(1)*pt(2),pt(1)*pbg(1)+pt(2)*pbg(2))
c     the cos(angle) to angle mapping is not one-to-one though
      if ( equal((pt(1)*cos(theta)+pt(2)*sin(theta))/pTt, cosphi, 1d-6) ) then
         pbNEW(1) = pTb*cos(theta)
         pbNEW(2) = pTb*sin(theta)
      else if ( equal((-pt(1)*cos(theta)-pt(2)*sin(theta))/pTt, cosphi, 1d-6) ) then
         pbNEW(1) = -pTb*cos(theta)
         pbNEW(2) = -pTb*sin(theta)
      else 
         print*,"inverseMappingFSR: something is wrong with the final angular mappings. Exiting!"
         print*,"inverseMappingFSR: ncalls = ",ncalls
cEXIT         call exit(-1) 
      endif
      pbNEW(0) = eb
      pbNEW(3) = pLb
      pwNEW = pt - pbNEW
c     let us check the angle
      if ( .not. equal((pt(1)*pbNEW(1)+pt(2)*pbNEW(2))/pTt/pTb, cosphi, 1d-6) ) then
         print*,"inverseMappingFSR: WARNING The cosine of angle between the top and the bottom in the transvers plane seems off:",(pt(1)*pbNEW(1)+pt(2)*pbNEW(2))/pTt/pTb,' as compared to:', cosphi
         print*,"inverseMappingFSR: ncalls = ",ncalls
      endif  
c     let us check the final bottom and W virtualities
      if ( .not. equal(pbNEW(0)**2-pbNEW(1)**2-pbNEW(2)**2-pbNEW(3)**2, mbSQR, 1d-3) ) then
         print*,"inverseMappingFSR: WARNING The bottom virtuality is off: ", pbNEW(0)**2-pbNEW(1)**2-pbNEW(2)**2-pbNEW(3)**2, mbSQR, 
     1     'deviate by:', abs(pbNEW(0)**2-pbNEW(1)**2-pbNEW(2)**2-pbNEW(3)**2 - mbSQR)/mbSQR
         print*,"inverseMappingFSR: ncalls = ",ncalls
      endif  
      if ( .not. equal(pwNEW(0)**2-pwNEW(1)**2-pwNEW(2)**2-pwNEW(3)**2, mwSQR, 1d-6) ) then
         print*,"inverseMappingFSR: WARNING The W virtuality is off: ",pwNEW(0)**2-pwNEW(1)**2-pwNEW(2)**2-pwNEW(3)**2,mwSQR, 'deviate by:', abs(pwNEW(0)**2-pwNEW(1)**2-pwNEW(2)**2-pwNEW(3)**2-mwSQR)/mwSQR
         print*,"inverseMappingFSR: ncalls = ",ncalls
      endif
c     rotate the lepton and the neutrino in the transverse plane by the same amount as the W rotated
      plep(:,1) = pl
      plep(:,2) = pnu
      call boost2reson(pw,2,plep,plep)
      call boost2resoninv(pwNEW,2,plep,plep)
      plNEW = plep(:,1)
      pnuNEW = plep(:,2)
c     copy over the rest of the puborn
      puborn(:,ipl-nres) = plNEW
      puborn(:,ipnu-nres) = pnuNEW
      puborn(:,ipb-nres) = pbNEW      
c     check momentum conservation at the reconstructed level
      if (.not.checkMomentumConservation(pt-pwNEW-pbNEW, 1d-3)) then
         print *, "inverseMappingFSR: WARNING The momentum in the born kinematics at the reconstructed top level is not preserved: ptot=", pt-pwNEW-pbNEW
         print *, "inverseMappingFSR: ncalls = ",ncalls
      endif
c     and at the decay level
      ptemp = puborn(:,1) + puborn(:,2)
      do i=3,flst_numfsb+2
         ptemp(:) = ptemp(:) - puborn(:,i)
      enddo
      if (.not.checkMomentumConservation(ptemp, 1d-3)) then
         print *, "inverseMappingFSR: WARNING The momentum in the born kinematics at the decayed top level is not preserved: ptot=", ptemp
         print *, "inverseMappingFSR: ncalls = ",ncalls
cEXIT         call exit(-1)
      endif
      end

      subroutine identifyMostLikelyTopology(ppl, pmi, prad, pb, pab, ptop, patop, imax, jmax, kmax)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_physpar.h'
      real * 8 ptop(0:3), patop(0:3), prad(0:3), pab(0:3), pb(0:3), ppl(0:3), pmi(0:3)
      integer imax,jmax,kmax
      real * 8 p1(0:3), p1p(0:3), p1pp(0:3), p2(0:3)
      integer i,j,k
      real * 8 Pt, Pwb, Pfks, Pmax
      real * 8 tmsqr, twsqr, bmsqr
      real * 8 dotp
      external dotp
      integer radiatingLeg
      real * 8 cosphi, cosphipl, cosphimi
      integer nexp
      parameter (nexp=8)
      integer lasti(0:maxalr,0:nexp), lastj(0:maxalr,0:nexp), lastk(0:maxalr,0:nexp), lastalr, lastjexp
      real * 8 lastprad(0:maxalr,0:nexp,0:3), lastq23sqr(0:maxalr,0:nexp), lasts123(0:maxalr,0:nexp), lastx(0:maxalr,0:nexp), 
     1  lasty(0:maxalr,0:nexp), lastpfks(2,0:maxalr,0:nexp), lastpmax(0:maxalr,0:nexp)
      common /isrmappings/lastprad, lastq23sqr, lasts123, lastx, lasty, lastpfks, lastpmax, lasti, lastj, lastk, lastalr, lastjexp
c     The inverse ISR mapping depends on the resonance history
c     in this routine we identify the most likely resonance history
c     based on the real kinematics of the event.

c     There are 8 possible single top configurations 
c     1.) p+ splits into bb~
c        a.) p+ radiates gluon
c        b.) p- radiates gluon
c     2.) p- splits into bb~
c        a.) p+ radiates gluon
c        b.) p- radiates gluon
c     Times 2 possible relabellings of the final state momenta

c     For each i,j,k let's calculate probabilities corresponding 
c     to a given hypothesis
c     - i the initial state leg participating in the g>bb splitting
c     - j is 1 if the same leg radiates, 2 otherwise
c     - k is the index of the final state top
c     in the following p1 is the momentum of the internal b line in g>bb splitting
c     while p2 is the momentum of the top quark associated with the other bottom
c        i.e. for i,j,k=2,2,4 ... p1 = pmi - pab, p2 = pW + pb
c                 i,j,k=2,1,4 ... p1 = pmi - prad - pab, p2 = pW + pb
c                 i,j,k=1,2,4 ... p1 = ppl - pab, p2 = pW + pb
c                 i,j,k=1,1,4 ... p1 = ppl - prad - pab, p2 = pW + pb
c                 i,j,k=2,2,5 ... p1 = pmi - pb, p2 = pW- + pab
c                 i,j,k=2,1,5 ... p1 = pmi - prad - pb, p2 = pW- + pab
c                 i,j,k=1,2,5 ... p1 = ppl - pb, p2 = pW- + pb
c                 i,j,k=1,1,5 ... p1 = ppl - prad - pb, p2 = pW- + pab
      tmsqr = physpar_pdgmasses(6)**2
      twsqr = physpar_pdgwidths(6)**2
      bmsqr = physpar_pdgmasses(5)**2
      Pmax = 0
cDBG      print*,'identifyMostLikelyTopology BEGIN'
      do i=1,2
         if ( i == 1 ) then
            p1 = ppl
         else
            p1 = pmi
         endif
         cosphi = (prad(3)*p1(3))/sqrt(p1(3)**2)/sqrt(prad(1)**2+prad(2)**2+prad(3)**2)
         Pfks = (1d0/(1d0-cosphi))**2
         lastpfks(i,lastalr,lastjexp) = pfks
cDBG         print*,'Pfks=',Pfks
         do j=1,2
            if ( j == 1 ) then 
               p1p = p1 - prad
            else 
               p1p = p1
            endif
            do k=4,5
               if ( k == 4 ) then
                  p2 = ptop ! if top is on shell
                  p1pp = p1p - pab ! it's the bottom that comes from the g>bb
               else
                  p2 = patop
                  p1pp = p1p - pb
               endif
               Pwb = 1d0/( dotp(p1pp,p1pp) - bmsqr )
cDBG               print*,'Pwb=',Pwb
c               if ( Pwb > 1d-4 ) then
c                  print*,"identifyMostLikelyTopology: internal bottom line is not spacelike. Exiting!"
c                  call exit(-1)
c               endif
               Pt = tmsqr/( (dotp(p2,p2)-tmsqr)**2 + twsqr*tmsqr )
cDBG               print*,'Pt=',Pt
c               print*,i,j,k,Pwb,Pt
               if (abs(Pfks*Pwb/Pt) > Pmax) then
                  Pmax = abs(Pfks*Pwb/Pt)
cDBG               if (abs(Pwb/Pt) > Pmax) then
cDBG                  Pmax = abs(Pwb/Pt)
                  imax = i
                  jmax = j
                  kmax = k
               endif
cDBG               print*,'Product = ', Pfks*Pwb/Pt
            enddo
         enddo
      enddo
      lastpmax = pmax/pfks
cDBG      print*,'identifyMostLikelyTopology END'
      if (imax == 1) then
         if (jmax == 1) then
            radiatingLeg = 1
         else
            radiatingLeg = 2
         endif
      else 
         if (jmax == 1) then
            radiatingLeg = 2
         else
            radiatingLeg = 1
         endif
      endif
cDBG      cosphipl = (prad(3)*ppl(3))/sqrt(ppl(3)**2)/sqrt(prad(1)**2+prad(2)**2+prad(3)**2)
cDBG      cosphimi = (prad(3)*pmi(3))/sqrt(pmi(3)**2)/sqrt(prad(1)**2+prad(2)**2+prad(3)**2)
cDBG      if ( (cosphipl > cosphimi .and. radiatingLeg == 2 )
cDBG     1 .or.(cosphipl < cosphimi .and. radiatingLeg == 1 ) ) then
cDBG         print*,"identifyMostLikelyTopology: WARNING potentially wrong guess for the radiating leg!"
cDBG         print*,"cos[phi(ppl,prad)] = ", cosphipl
cDBG         print*,"cos[phi(pmi,prad)] = ", cosphimi
cDBG         print*,"radiatingLeg = ", radiatingLeg
cDBG      endif
cDBG      print*,"identifyMostLikelyTopology:"
cDBG      print*,"cos[phi(ppl,prad)] = ", cosphipl
cDBG      print*,"cos[phi(pmi,prad)] = ", cosphimi
cDBG      print*,"radiatingLeg = ", radiatingLeg
      end

      subroutine inverseMappingISR(areal,puborn)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_physpar.h'
      real * 8 pcm(0:3,1:nlegreal)
      integer pcmlen
      common/local_pcm/pcm,pcmlen
      integer areal(pcmlen)
      real * 8 puborn(0:3,flst_numfsb+2)
      integer imax,jmax,kmax
      integer notImax
      real * 8 ptop(0:3), patop(0:3), ptopM(0:3), ptopMt(0:3), prad(0:3), pab(0:3), pb(0:3), pbM(0:3)
      real * 8 p123(0:3), p23(0:3), p23t(0:3), s123, q23sqr
      real * 8 dotp
      external dotp
      real * 16 dotp_qp
      external dotp_qp
      real * 8 x, r, y, z, A, B, ptemp(0:3), s123tmp
      real * 16 x_qp, p23t_qp(0:3), ptopM_qp(0:3), p23_qp(0:3), q23sqr_qp, s123_qp, p123_qp(0:3)
      integer i
      logical equal
      integer :: ncalls = 0, negxvals = 0
      real * 8 pdec(0:3,3)
      integer itopdec(3)
      integer iatopdec(3)
      integer nextpuborn
      logical checkMomentumConservation
      integer nexp
      parameter (nexp=8)
      integer lasti(0:maxalr,0:nexp), lastj(0:maxalr,0:nexp), lastk(0:maxalr,0:nexp), lastalr, lastjexp
      real * 8 lastprad(0:maxalr,0:nexp,0:3), lastq23sqr(0:maxalr,0:nexp), lasts123(0:maxalr,0:nexp), lastx(0:maxalr,0:nexp), 
     1  lasty(0:maxalr,0:nexp), lastpfks(2,0:maxalr,0:nexp), lastpmax(0:maxalr,0:nexp)
      common /isrmappings/lastprad, lastq23sqr, lasts123, lastx, lasty, lastpfks, lastpmax, lasti, lastj, lastk, lastalr, lastjexp
      logical :: bothYoverThreshold = .true.
      real * 8 pt2
c     TODO delete once done
      puborn = 0
c     increase the call counter
      ncalls = ncalls + 1
c      if (ncalls == 62935) then
c         print*,"here"
c      endif
c     Find the momenta that we will need in the following
      prad = pcm(:,pcmlen)
      pt2 = prad(1)**2+prad(2)**2

      lastprad(lastalr,lastjexp,:)= prad
      ptop = 0
      patop = 0
      do i=1,pcmlen
         select case (areal(i))
            case (5)
               pb = pcm(:,i)
               ptop = ptop + pb
               itopdec(1) = i
            case (-5)
               pab = pcm(:,i)
               patop = patop + pab
               iatopdec(1) = i
            case (24)
               ptop = ptop + pcm(:,i)
            case (-24)
               patop = patop + pcm(:,i)
            case (-11)
               itopdec(2) = i
            case (12)
               itopdec(3) = i
            case (13)
               iatopdec(2) = i
            case (-14)
               iatopdec(3) = i
         end select
      enddo
c     sanity check
      if(.not.checkMomentumConservation(pcm(:,1) + pcm(:,2) - ptop - patop - prad, 1d-3)) then
         print*,"inverseMappingISR: FATAL ERROR Something went wrong in reconstructing the participating particles, momentum is not conserved: ptot=",pcm(:,1) + pcm(:,2) - ptop - patop - prad,". Exiting!"
         print*,"inverseMappingISR: ncalls = ",ncalls
cEXIT         call exit(-1)
      endif
c     first we identify the most likely single top topology
      call identifyMostLikelyTopology(pcm(:,1), pcm(:,2), prad, pb, pab, ptop, patop, imax, jmax, kmax)
c      imax = 1 
c      jmax = 1
c      kmax = 5
c     and map from n+1 to n particle phase space such that
c        1.) we preserve the momenta of the b and the initial state responsible for gbb and the corresponding W
c        2.) preserves the virtuality of the system of the other b and W
c     the mapping then depends on whether the incoming lines
c     that participate in g>bb splitting and that radiate are one
c     and the same line
c     if they are, jmax == 1

      call calculateY(y)
      if (y>100) then 
         if (imax == 1) then
            imax = 2
         else
            imax = 1
         endif
         call calculateY(y)
         if (y>100) then
            bothYoverThreshold = .true.
            if (imax == 1) then
               imax = 2
            else
               imax = 1
            endif
         endif
      endif
c         if (y>10) then
c            if (.not.identifyMostLikelyTopologyOverriden) then
c               identifyMostLikelyTopologyOverriden = .true.
c               if (imax == 1) then
c                  imax = 2
c               else
c                  imax = 1
c               endif
c               goto 999
c            else
c               print *, "inverseMappingISR: mapping case was already overriden and it still fails."
c               print *, "inverseMappingISR: ncalls = ",ncalls
c            endif
c         endif
c         if (y>10) then
c            if (.not.identifyMostLikelyTopologyOverriden) then
c               identifyMostLikelyTopologyOverriden = .true.
c               if (imax == 1) then
c                  imax = 2
c               else
c                  imax = 1
c               endif
c               goto 999
c            else
c               print *, "inverseMappingISR: mapping case was already overriden and it still fails."
c               print *, "inverseMappingISR: ncalls = ",ncalls
c            endif
c         endif




      p23 = pcm(:,imax) - prad
      if (imax == 1) then
         notImax = 2
      else
         notImax = 1
      endif
      p123 = p23 + pcm(:,notImax)
      q23sqr = - dotp(p23,p23)
      lastq23sqr(lastalr,lastjexp) = q23sqr
      s123 = dotp(p123,p123)
      lasts123(lastalr,lastjexp) = s123
      x = q23sqr/(s123+q23sqr)
      lastx(lastalr,lastjexp) = x
      if (x < 0d0) negxvals = negxvals + 1
      p23t = x*pcm(:,notImax) + p23
      lasti(lastalr,lastjexp) = imax
      lastj(lastalr,lastjexp) = jmax
      lastk(lastalr,lastjexp) = kmax
      if (jmax == 2) then
c        case A: the line participating in the g>bb splitting and the line that radiates are different
c           In this case we work with the initial state that radiates and rescale it and modify the top 
c           (while preserving its virtuality) as to preserve to momentum.
c           The system that undergoes the g>bb splitting is unaffected (the other initial state, and the
c           corresponding b and W)
         if (kmax == 4) then
            ptopM = ptop
         else 
            ptopM = patop
         endif
         A = 2 * (dotp(ptopM,p23t) - dotp(p23,p23t))
         B = dotp(p23,p23) - 2*dotp(ptopM,p23)
         if ( abs(A) < 1d-4 ) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR A is very near zero (case A): A=",A,". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
         y = -B/A
c        check the value of y is reasonable, if it was not then perhaps we guessed wrong in the identifyMostLikelyTopology
         lasty(lastalr,lastjexp) = y
         puborn(:,imax) = y * p23t
         puborn(:,notImax) = pcm(:,notImax)
c        check that the pcm(:,imax) virtuality is preserved to be zero
         if (.not.equal(dotp(puborn(:,imax), puborn(:,imax)),0d0,1d-6)) then
            if (pt2 < physpar_pdgmasses(6)**2) then
               print *, "inverseMappingsISR: pt2=",pt2
               if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
               print *, "inverseMappingISR: WARNING Initial state failed to stay massless (case A): virt=", dotp(puborn(:,imax), puborn(:,imax))
               print *, "inverseMappingISR: ncalls = ",ncalls
            endif
         endif
         ptopMt = ptopM + y * p23t - p23
c        check that the top virtuality is preserved
         if (.not.equal(dotp(ptopM, ptopM), dotp(ptopMt, ptopMt),1d-6)) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR Virtuality of the top not preserved (case A): ", dotp(ptopM, ptopM), dotp(ptopMt, ptopMt), 
     1           "deviate by:", abs(dotp(ptopM, ptopM)-dotp(ptopMt, ptopMt))/dotp(ptopMt, ptopMt),". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXITC            call exit(-1)
         endif
      else 
c        case B: the line participating in the g>bb splitting and the line that radiates are the same
c           In this case we work with the initial state that radiates and rescale it while preserving the virtuality
c           of the internal b-quark. 
c           The recoil is absorbed by the internal top and propagated to the final state top.
         if (kmax == 4) then
            pbM = pab ! the b that does not originate from the decay of the top
            ptopM = ptop
         else 
            pbM = pb ! the b that does not originate from the decay of the anti-top
            ptopM = patop
         endif
c        find the rescaling factor y  such that the internal b-quark line's virtuality is preserved
         A = 2 * dotp(pbM,p23t)
         if ( abs(A) < 1d-4 ) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR A|y is very near zero (case B): A=",A," Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
         B = dotp(p23,p23) - 2*dotp(pbM,p23)
         y = -B/A
         lasty(lastalr,lastjexp) = y
c        check that the virtuality of the internal b-quark line is preserved
         if (.not.equal(dotp(p23-pbM,p23-pbM),dotp(y*p23t-pbM,y*p23t-pbM),1d-5)) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR the virtuality of the intermediate b-quark is not preserved (case B): ",
     1          dotp(p23-pbM,p23-pbM), dotp(y*p23t-pbM,y*p23t-pbM), "deviate by: ", abs(dotp(p23-pbM,p23-pbM) - dotp(y*p23t-pbM,y*p23t-pbM))/dotp(y*p23t-pbM,y*p23t-pbM),". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
         puborn(:,imax) = y * p23t
c        check that the pcm(:,imax) virtuality is preserved to be zero
         if (.not.equal(dotp(puborn(:,imax), puborn(:,imax)),0d0, 1d-3)) then
            if (pt2 < physpar_pdgmasses(6)**2) then
               print *, "inverseMappingsISR: pt2=",pt2
               if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
               print *, "inverseMappingISR: WARNING Radiating initial state failed to stay massless (case B): virt=", dotp(puborn(:,imax), puborn(:,imax))
               print *, "inverseMappingISR: ncalls = ",ncalls
            endif
         endif
c        find the rescaling factor z such that the virtuality of the top is preserved
         A = 2 * ( dotp(pcm(:,notImax),ptopM) + (y - 1d0)*dotp(pcm(:,notImax),p23) )
         if ( abs(A) < 1d-4 ) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR A|z is very near zero (case B): A=", A,". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
         B = (y - 1d0) * (2*dotp(ptopM,p23) + (y - 1d0)*dotp(p23,p23))
         z = B/A
         ptopMt = ptopM + (y - 1d0)*p23 - z*pcm(:,notImax)
c        check that the top virtuality is preserved
         if (.not.equal(dotp(ptopM, ptopM), dotp(ptopMt, ptopMt), 1d-3)) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR Virtuality of the top not preserved (case B): ", 
     1         dotp(ptopM, ptopM), dotp(ptopMt, ptopMt), "deviate by: ",  abs(dotp(ptopM, ptopM)-dotp(ptopMt, ptopMt))/dotp(ptopMt, ptopMt),
     2         ". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
c        check that (1 - x*y + z) does not change the direction of the momentum
         if ((1d0 - x*y - z) < 0) then
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: FATAL ERROR The mapping (case B) inverts the direction of the initial state momentum: (1d0-x*y-z)=",(1d0 - x*y - z),". Exiting!"
            print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT            call exit(-1)
         endif
         puborn(:,notImax) = (1d0 - x*y - z)*pcm(:,notImax)
c        check that the pcm(:,imax) virtuality is preserved to be zero
         if (.not.equal(dotp(puborn(:,notImax), puborn(:,notImax)),0d0,1d-6)) then
            if (pt2 < physpar_pdgmasses(6)**2) then
               print *, "inverseMappingsISR: pt2=",pt2
               if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
               print *, "inverseMappingISR: WARNING The other initial state failed to stay massless (case B): virt=", dotp(puborn(:,notImax), puborn(:,notImax))
               print *, "inverseMappingISR: ncalls = ",ncalls
            endif
         endif
      endif
c     finally, deal with the top and its decay products
      if (kmax == 4) then
c         print*,'before:'
c         print*,ptop
         do i=1,3
            pdec(:,i) = pcm(:,itopdec(i))
c            print*,pdec(:,i)
         enddo
         call boost2reson(ptop,3,pdec,pdec)
         call boost2resoninv(ptopMt,3,pdec,pdec)
c         print*,'after:'
c         print*,ptopMt
         do i=1,3
            puborn(:,itopdec(i)-pcmlen+flst_numfsb+3) = pdec(:,i)
            puborn(:,iatopdec(i)-pcmlen+flst_numfsb+3) = pcm(:,iatopdec(i))
c            print*,pdec(:,i)
         enddo
         ptop = ptopMt
      else 
c         print*,'before:'
c         print*,ptop
         do i=1,3
            pdec(:,i) = pcm(:,iatopdec(i))
c            print*,pdec(:,i)
         enddo
         call boost2reson(patop,3,pdec,pdec)
         call boost2resoninv(ptopMt,3,pdec,pdec)
c         print*,'after:'
c         print*,ptopMt
         do i=1,3
            puborn(:,iatopdec(i)-pcmlen+flst_numfsb+3) = pdec(:,i)
            puborn(:,itopdec(i)-pcmlen+flst_numfsb+3) = pcm(:,itopdec(i))
c            print*,pdec(:,i)
         enddo
         patop = ptopMt
      endif
c     check the momentum conservation at the reconstructed top level
      if(.not.checkMomentumConservation(puborn(:,1) + puborn(:,2) - ptop - patop, 1d-3)) then
         if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
         print *, "inverseMappingISR: FATAL ERROR The momentum in the born kinematics at the reconstructed top level is not preserves: ptot=",puborn(:,1) + puborn(:,2) - ptop - patop,". Exiting!"
         print *, "inverseMappingISR: ncalls = ",ncalls
cEXIT         call exit(-1)
      endif
c     and at the top decay level; this sometimes does not work out quite as well, because the boost is precise only to 8 digits and the top momenta may be huge
      ptemp = puborn(:,1) + puborn(:,2)
      do i=3,flst_numfsb+2
         ptemp(:) = ptemp(:) - puborn(:,i)
      enddo
      if (.not.checkMomentumConservation(ptemp, 1d-3)) then
         if (pt2 < physpar_pdgmasses(6)**2) then
            print *, "inverseMappingsISR: pt2=",pt2
            if (bothYoverThreshold) print *, "inverseMappingsISR: both y over threshold"
            print *, "inverseMappingISR: WARNING The momentum in the born kinematics at the decayed top level is not preserved: ptot=", ptemp
            print *, "inverseMappingISR: ncalls = ",ncalls
         endif
      endif

      contains
         subroutine calculateY(yret)
          double precision yret
          p23 = pcm(:,imax) - prad
          if (imax == 1) then
             notImax = 2
          else
             notImax = 1
          endif
          p123 = p23 + pcm(:,notImax)
          q23sqr = - dotp(p23,p23)
          s123 = dotp(p123,p123)
          x = q23sqr/(s123+q23sqr)
          if (x < 0d0) negxvals = negxvals + 1
          p23t = x*pcm(:,notImax) + p23
          if (jmax == 2) then ! case A
             if (kmax == 4) then
                ptopM = ptop
             else 
                ptopM = patop
             endif
             A = 2 * (dotp(ptopM,p23t) - dotp(p23,p23t))
             B = dotp(p23,p23) - 2*dotp(ptopM,p23)
             yret = -B/A
          else ! case B
             if (kmax == 4) then
                pbM = pab ! the b that does not originate from the decay of the top
                ptopM = ptop
             else 
                pbM = pb ! the b that does not originate from the decay of the anti-top
                ptopM = patop
             endif
             A = 2 * dotp(pbM,p23t)
             B = dotp(p23,p23) - 2*dotp(pbM,p23)
             yret = -B/A
          endif
          end
      end

      logical function checkMomentumConservation(p,eps)
      implicit none
      real * 8 p(0:3), eps
      logical equal
      if ( equal(p(1),0d0,eps) .and. equal(p(2),0d0,eps) .and. equal(p(3),0d0,eps) ) then
         checkMomentumConservation = .true.
      else
         checkMomentumConservation = .false.
      endif
      return
      end

      logical function equal(a,b,eps)
      implicit none
      real * 8 a, b, eps
c      real, parameter :: eps=4e-3
      if ( abs((a - b)/a) < eps ) then
         equal=.true.
      else if ( b == 0d0 .and. ( abs(a) < eps )) then
         equal=.true.
      else 
         equal=.false.
      endif
      return
      end function

      function dotp_qp(p1,p2)
      implicit none
      real * 8 dotp_qp,p1(0:3),p2(0:3)
      dotp_qp = (p1(0)*p2(0) - p1(3)*p2(3)) - p1(1)*p2(1) - p1(2)*p2(2)
      end

      subroutine gettransmass(m,p,mt)
      implicit none
      real * 8 m,p(0:3),mt
      mt=dsqrt(abs(m**2+p(1)**2+p(2)**2))
      end

