#!/bin/bash


function makecheck_compare {
    case $1 in
	*.lhe) cmp <(sed '/SVN status Begin/,/SVN status End/d' $1) <(sed '/SVN status Begin/,/SVN status End/d' $2) > /dev/null ;;
	*) cmp $1 $2 > /dev/null ;;
    esac
}

function printcheck {
    files=`ls -1 *0010* | grep -v counter`
    if [ $build = yes ]
    then
	\mv -f $files savechecks/
	\rm -f *0010*
    else
        echo -n $1 >> checks.txt
	stat=OK
	for file in $files
	do
#	    echo "comparing file '$file'" >> checks.txt
	    if ! makecheck_compare $file savechecks/$file
	    then	        	       
		if [ "$stat" = "OK" ] ; then
		  echo >> checks.txt
		fi
   	        stat=
		echo basic-check/scratch_checks/$file basic-check/savechecks/$file 'differ'
		echo basic-check/scratch_checks/$file basic-check/savechecks/$file 'differ' >> checks.txt
	    fi
	done
	mv $files scratch_checks
	rm *0010*
	if [ "$stat" = "OK" ] ; then
	    echo " OK" >> checks.txt
	fi
    fi

}


\rm -f scratch_checks/*

./clean.sh

if [ n$1 = n--build ]
then
    build=yes
else
    build=no
    > checks.txt
fi

seq 100 > pwgseeds.dat

# check stage 1

cat powheg.input-save | sed 's/^ *ncall1 .*/ncall1 100/ ; s/^ *parallelstage .*/parallelstage 1/' > powheg.input

../pwhg_main <<EOF
10
EOF

printcheck "Stage 1:"

# check stage 2

cat powheg.input-save | sed 's/^ *parallelstage .*/parallelstage 2/ ; s/^ *ncall2 .*/ncall2 100/' > powheg.input

(cd premade-grids ; cp pwg-xg2-xgrid-btl-000[12].dat  pwg-xg2-xgrid-rm-000[12].dat ../)

../pwhg_main <<EOF
10
EOF

printcheck "Stage 2:"

# check stage 3

cat powheg.input-save | sed 's/^ *parallelstage .*/parallelstage 3/ ;  s/^ *ncall2 .*/ncall2 100/ ; s/^ *nubound .*/nubound 100/' > powheg.input

(cd premade-grids ; cp pwggrid-btl-000[12].dat pwggrid-rm-000[12].dat pwgbtlupb-000[12].dat pwgrmupb-000[12].dat ../)

../pwhg_main <<EOF
10
EOF

printcheck "Stage 3:"

# check stage 4

cat powheg.input-save | sed 's/^ *parallelstage .*/parallelstage 4/ ; s/^ *numevts .*/numevts 100/ ' > powheg.input

(cd premade-grids ; cp pwgfullgrid-*-000[12].dat pwgubound-000[12].dat ../)

../pwhg_main <<EOF
10
EOF

printcheck "Stage 4:"
