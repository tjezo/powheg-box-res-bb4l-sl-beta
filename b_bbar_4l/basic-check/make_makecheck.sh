#!/bin/bash

./clean.sh

\rm premade-grids/*

runfiles="pwgbtlupb-*.dat  pwgevents-*.lhe  pwgfullgrid-btl-*.dat  pwggrid-btl-*.dat  pwggrid-rm-*.dat pwgrmupb-*.dat  pwg-xg2-xgrid-btl-*.dat  pwg-xg2-xgrid-rm-*.dat pwgfullgrid-rm-*.dat pwgubound-*.dat"

seq 100 > pwgseeds.dat

chmod u+x run-parallel.sh

./run-parallel.sh

if [ -e pwgfullgrid-btl-0002.dat ]
then
    mv pwgfullgrid-btl-0002.dat pwgfullgrid-btl-0001.dat
fi

if [ -e pwgfullgrid-rm-0002.dat ]
then
    mv pwgfullgrid-rm-0002.dat pwgfullgrid-rm-0001.dat
fi

\mv -f $runfiles premade-grids/

./makecheck.sh --build
