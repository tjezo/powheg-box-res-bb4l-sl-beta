      subroutine gen_leshouches(id)
      implicit none
c id is either btilde or remn
      character * (*) id
      include 'pwhg_math.h'
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'pwhg_kn.h'
      include 'pwhg_rad.h'
      include 'pwhg_em.h'
      include 'LesHouches.h'
      include 'LesHouches_uborn.h'
      integer alr,em,rad,flem,flrad,coluborn(2),nrad
      real * 8 beta, vec(3)
      integer j
      integer, save :: nnnb = 0
      real * 8  t,csi,y,azi,beams(0:3,2),masses(nlegborn),xb1,xb2,x1,x2,jacreal
      integer, allocatable, save :: reslistborn(:),reslistreal(:)
      real * 8, allocatable, save :: pborn(:,:),cmpborn(:,:),preal(:,:),cmpreal(:,:)
      logical, save :: ini=.true.
      integer, save :: nupmax = 0
c When running with allrad set to 1, radiation generated in production and
c decays are all stored by the handle radiation routine
      if(id == 'btilde') then
c        figure out the number of radiations, in our case it should not be larger than 1
         call handle_radiations('inquire',nrad,0d0,0d0,0d0,0d0)
      elseif(id == 'remn') then
c        in the case of remnant we did nothing, because all the remnant event already have one emission
         return
      else
c        otherwise throw an error message
         write(*,*) ' gen_leshouches: got argument id=',id
         write(*,*) ' cannot handle, exiting ...'
         call exit(-1)
      endif
c For safety, call again the setup_resgroupstuff routine.
c rad_ubornsubp is set both for a btilde and for a remnant event.
      flst_ibornresgroup = flst_bornresgroup(rad_ubornsubp)
      call setup_resgroupstuff

c set scalup to an invalid value. At the end it should have been set.
c If not, throw an error and exit.
c
      if(nrad.eq.0) then
c     no radiation has been generated, do nothing
      else
c     radiation has been generated, append it to the Les Houches record
         if(id == 'btilde') then         
            if (nup > nupmax) then
               if (nupmax > 0) then
                  deallocate(reslistreal)
                  deallocate(pborn) 
                  deallocate(cmpborn)
                  deallocate(reslistborn)
                  deallocate(preal)
                  deallocate(cmpreal)
               endif
               allocate(reslistreal(nup+1))
               allocate(pborn(0:3,nup)) 
               allocate(cmpborn(0:3,nup))
               allocate(reslistborn(nup))
               allocate(preal(0:3,nup+1))
               allocate(cmpreal(0:3,nup+1))
               nupmax = nup
            endif
c           Reconstruct the index of the underlying born flavour
c           first reconstruct the resonance structure
            reslistreal = 0
            do j=1,nup
               reslistreal(j) = mothup(1,j)
               if (reslistreal(j) < 3) reslistreal(j) = 0
            enddo
c     initially we assign the Born kinematics to the real momenta.
c     When entering the following loop, the current real will be assigned to the Born
c     temporary array, and the real will be build from the temporary Born and
c     the stored radiation parameters. In the flg_allrad == .true. case, this operation
c     may be performed several times, building radiation from each resonance (that is why
c     the previous real becomes the next Born.)
c     Better to use slices always ...
            preal(1:3,1:nup) = pup(1:3,1:nup)
            preal(0,1:nup) = pup(4,1:nup)
c           boost lab momenta read in from LHE into the CM momenta
            call compcmkin_g(nup,preal,cmpreal)
            masses(1:flst_ibornlength) = kn_masses(1:flst_ibornlength)
            x1 = pup(4,1)/ebmup(1)
            x2 = pup(4,2)/ebmup(2)
c     
            beams = kn_beams
c TODO should we make sure kn_beams is consistent between the event and the powheg.input file?
            call handle_radiations('next',alr,t,csi,y,azi)
c           if acceptable emission has been generated
            if(t.gt.0) then
               em = flst_emitter(alr)
cTODOc     rad_kinreg may be printed at the end of the event for debugging purposes.
cTODOc     the following scheme works as usual for a single radiation, i.e. rad_kinreg=1
cTODOc     for isr, and from 2 on for final state radiation, depending upon which line is
cTODOc     radiating.
cTODOc     In case there are multiple radiation (with allrad present), the radiation
cTODOc     regions are shifted two digits left. This assumes that there are less than 99
cTODOc     radiation regions. If there are two many radiating resonances, we must stop so that
cTODOc     rad_kinreg does not overflow.
cTODO               if(rad_kinreg < 2147483647/100) then
cTODO                  if(em < 3) then
cTODO                     rad_kinreg = rad_kinreg*100 + 1
cTODO                  else
cTODO                     if(em+2-flst_lightpart < 99) then
cTODO                        rad_kinreg = rad_kinreg*100 + (em+2-flst_lightpart)
cTODO                     endif
cTODO                  endif
cTODO               endif
c     the current real becomes the new Born.
               reslistborn(1:nup) = reslistreal(1:nup)
               pborn(:,1:nup) = preal(:,1:nup)
               cmpborn(:,1:nup) = cmpreal(:,1:nup)
               xb1 = x1
               xb2 = x2
               if(em.le.2) then
                  call gen_real_phsp_isr_rad_g0(
     1                 nup,csi,y,azi,beams,
     2                 xb1,xb2,pborn(:,1:nup),
     3                 cmpborn(:,1:nup),x1,x2,preal(:,1:nup+1),cmpreal(:,1:nup+1),jacreal)
               else
                  call gen_real_phsp_fsr_rad_g0(
     1                 nup,reslistborn(1:nup),em,csi,y,azi,
     2                 xb1,xb2,masses,pborn(:,1:nup),
     3                 cmpborn(:,1:nup),x1,x2,preal(:,1:nup+1),cmpreal(:,1:nup+1),jacreal)
               endif
               reslistreal(1:nup) = reslistborn(1:nup)
               if(em <= 2) then
                  reslistreal(nup+1) =  0
               else
                  reslistreal(nup+1) =  flst_alrres(em,alr)
               endif
               nup = nup+1
               call setup_real_lh
            endif
            call momenta_lh(preal,nup)
         endif
      endif
c add resonances, perform decays, put particles on shell, etc.(or nothing!)
      call finalize_lh
      contains
         subroutine setup_real_lh
         implicit none
         istup(nup)=1
         spinup(nup)=9
         vtimup(nup)=0
         if(reslistreal(nup).eq.0) then
            mothup(1,nup)=1
            mothup(2,nup)=2
         else
            mothup(1,nup)=reslistreal(nup)
            mothup(2,nup)=reslistreal(nup)
         endif
         rad=nup
         if(em.eq.0) then
            if(kn_y.gt.0) then
               em=1
            else
               em=2
            endif
         endif
         flem=flst_alr(em,alr)
         flrad=flst_alr(flst_alrlength(alr),alr)
         coluborn(1)=icolup(1,em)
         coluborn(2)=icolup(2,em)
         if(em.le.2) then
c     setcolour_rad works with all incoming (or all outgoing) flavours and colours;
c     for ISR make everything outgoing:
c     thus in input change the sign of the emitter flavour, to make it outgoing
c     and conjugate its colour in output, to make it incoming.
            call setcolour_rad(coluborn,-flem,flrad,
     1           icolup(1,em),icolup(1,rad))
            call colour_conj(icolup(1,em))
         else
c     For ISR make everything incoming;
c     Change the sign of the emitter and radiated flavours in input,
c     to make it incoming;
c     conjugate their colours in the output, to make them outgoing.
            if (flrad.ne.22) then
               call setcolour_rad(coluborn,-flem,-flrad,
     1              icolup(1,em),icolup(1,rad))
            else
               call setcolour_rad(coluborn,-flem,flrad,
     1              icolup(1,em),icolup(1,rad))
            endif
            call colour_conj(icolup(1,rad))
            call colour_conj(icolup(1,em))
         endif
         if(flrad.eq.0) flrad=21
         idup(rad)=flrad
         if(flem.eq.0) flem=21
         idup(em)=flem
         end subroutine setup_real_lh
         
      end

      subroutine colour_conj(icol)
      implicit none
      integer icol(2)
      integer itmp
      itmp=icol(2)
      icol(2)=icol(1)
      icol(1)=itmp
      end

      subroutine momenta_lh(p,n)
      implicit none
      integer n
      real * 8 p(0:3,n)
      integer k,mu
      include 'LesHouches.h'
      do k=1,n
         do mu=1,3
            pup(mu,k)=p(mu,k)
         enddo
         pup(4,k)=p(0,k)
         pup(5,k)=sqrt(abs(p(0,k)**2-p(1,k)**2
     #       -p(2,k)**2-p(3,k)**2))
      enddo
      end

      subroutine getnewcolor(newcolor)
      implicit none
      integer newcolor
      include 'LesHouches.h'
      integer j,k
      newcolor=511
 1    continue
      do j=1,nup
         do k=1,2
            if(icolup(k,j).eq.newcolor) then
               newcolor=newcolor+1
               goto 1
            endif
         enddo
      enddo
      end

c Given a vertex with 3 partons, the first parton with incoming colour
c colin, the second and third partons with incoming flavours fl2, fl3,
c it assigns the incoming colours of the second and third partons
c colout2, colout3. The colour assignment is unambiguous in all cases, but in
c the one where both fl2 and fl3 are gluons, where a 50% random choice is made
c over the two possible colour connections.
      subroutine setcolour_rad(colin,fl2,fl3,colout2,colout3)
      implicit none
      integer colin(2),fl2,fl3,colout2(2),colout3(2)
      integer newcolor
      real * 8 random
      logical is_coloured
      external random,is_coloured
      call getnewcolor(newcolor)
c First handle photon case
      if(fl2.eq.22) then
         colout2=0
         colout3(1)=colin(2)
         colout3(2)=colin(1)
      elseif(fl3.eq.22) then
         colout3=0
         colout2(1)=colin(2)
         colout2(2)=colin(1)
      elseif(colin(1).eq.0.and.colin(2).eq.0) then
         if(is_coloured(fl2)) then
            if(fl2.gt.0) then
               colout2(1)=newcolor
               colout2(2)=0
               colout3(2)=newcolor
               colout3(1)=0
            elseif(fl3.gt.0) then
               colout2(1)=0
               colout2(2)=newcolor
               colout3(2)=0
               colout3(1)=newcolor
            else
               goto 998
            endif
         else
            colout2=0
            colout3=0
         endif
      else
c now handle all coloured particles
         if(colin(1).ne.0.and.colin(2).ne.0) then
            if(fl2.eq.0.and.fl3.eq.0) then
               if(random().gt.0.5d0) then
                  colout2(1)=colin(2)
                  colout2(2)=newcolor
                  colout3(1)=newcolor
                  colout3(2)=colin(1)
               else
                  colout3(1)=colin(2)
                  colout3(2)=newcolor
                  colout2(1)=newcolor
                  colout2(2)=colin(1)
               endif
            elseif(fl2.gt.0.and.fl3.lt.0) then
               colout2(2)=0
               colout2(1)=colin(2)
               colout3(1)=0
               colout3(2)=colin(1)
            elseif(fl2.lt.0.and.fl3.gt.0) then
               colout2(1)=0
               colout2(2)=colin(1)
               colout3(2)=0
               colout3(1)=colin(2)
            else
               goto 998
            endif
         elseif(colin(2).eq.0) then
            if(fl2.eq.0) then
               colout3(1)=0
               colout3(2)=newcolor
               colout2(1)=newcolor
               colout2(2)=colin(1)
            elseif(fl3.eq.0) then
               colout2(1)=0
               colout2(2)=newcolor
               colout3(1)=newcolor
               colout3(2)=colin(1)
            else
               goto 998
            endif
         elseif(colin(1).eq.0) then
            if(fl2.eq.0) then
               colout3(1)=newcolor
               colout3(2)=0
               colout2(2)=newcolor
               colout2(1)=colin(2)
            elseif(fl3.eq.0) then
               colout2(1)=newcolor
               colout2(2)=0
               colout3(1)=colin(2)
               colout3(2)=newcolor
            else
               goto 998
            endif
         else
            goto 998
         endif
      endif
      return
 998  continue
      write(*,*)
     #     ' Error in setcolour_rad: inconsistent colour flavour input'
      write(*,*) 'colin[1:2] ',colin(1),' ',colin(2)
      write(*,*) 'fl2, fl3 ',fl2,' ',fl3
      write(*,*) 'newcolor ',newcolor
      call pwhg_exit(1)
      end

      subroutine pdfreweightinfo(id1,id2,x1,x2,xmufact,xf1,xf2)
      implicit none
      integer id1,id2
      real * 8 x1,x2,xf1,xf2,xmufact
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_rad.h'
      include 'pwhg_pdf.h'
      include 'pwhg_st.h'
      include 'LesHouches.h'
      real * 8 pdf(-pdf_nparton:pdf_nparton)
      if(rad_type.eq.1) then
c Btilde event: pass x1 and x2, id1, id2 etc. of the current underlying Born
         id1=flst_born(1,rad_ubornsubp)
         id2=flst_born(2,rad_ubornsubp)
         x1=kn_xb1
         x2=kn_xb2
         call setscalesbtilde
      elseif(rad_type.eq.2.or.rad_type.eq.3) then
         id1=idup(1)
         id2=idup(2)
         if(id1.eq.21) id1=0
         if(id2.eq.21) id2=0         
         x1=kn_x1
         x2=kn_x2
         call setscalesbtilde
      endif
      xmufact=sqrt(st_mufact2)
      call pdfcall(1,x1,pdf)
      xf1=x1*pdf(id1)
      call pdfcall(2,x2,pdf)
      xf2=x2*pdf(id2)
      if(id1.eq.0) id1=21
      if(id2.eq.0) id2=21
      end

