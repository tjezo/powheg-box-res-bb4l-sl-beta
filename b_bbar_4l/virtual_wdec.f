      subroutine setvirtual(p,vflav,virtual)
c Virtual needs to be provided by the user and put here
      implicit none
      include 'nlegborn.h'
      include 'pwhg_st.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 p(0:3,nlegs)
      integer vflav(nlegs)
      real * 8 virtual,virtualol
      real * 8 powheginput
      external powheginput
      logical openloopsreal,openloopsvirtual
      common/copenloopsreal/openloopsreal,openloopsvirtual
      real * 8 bmunu(0:3,0:3,nlegs),born
      real * 8 bornjk(nlegs,nlegs)      
      logical ini,MSbarscheme
      save ini,MSbarscheme
      data ini/.true./
      
cTODO      if (ini) then
cTODO         MSbarscheme=.true.
cTODO         if (powheginput("#MSbarscheme").eq.0) MSbarscheme=.false.
cTODO         ini=.false.
cTODO      endif      
cTODO
cTODO      call openloops_virtual(p,vflav,virtualol)
cTODO      virtual = virtualol
cTODO
cTODO      if (MSbarscheme) then
cTODOc     see  Cacciari, Greco, Nason  hep-ph/9803400
cTODO         call setborn(p,vflav,born,bornjk,bmunu)
cTODO         if (vflav(1).ne.0.and.vflav(2).ne.0) then            
cTODOc     a factor of as/(2 pi) will be attached by the BOX 
cTODO            virtual=virtual - 4d0/3*TF*log(st_muren2/ph_bmass**2)*born
cTODO         elseif  (vflav(1).eq.0.and.vflav(2).eq.0) then    
cTODOc     a factor of as/(2 pi) will be attached by the BOX 
cTODO            virtual = virtual + 4d0/3*TF*log(st_mufact2/st_muren2)*born
cTODO         else
cTODO            write(*,*) "We should not be here!!"
cTODO            call pwhg_exit(-1)
cTODO         endif
cTODO      endif

      virtual = 0d0
      end
