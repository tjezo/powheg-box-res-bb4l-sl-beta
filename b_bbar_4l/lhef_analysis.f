      program leshouchesanal
      implicit none
      include "nlegborn.h"
      include 'LesHouches.h'
      include 'LesHouches_uborn.h'
      include 'hepevt.h'
      include "pwhg_rad.h"
      include "pwhg_rwl.h"
      include "pwhg_rnd.h"
      integer j,nev,nevpassed,k,l
      character * 6 WHCPRG
      common/cWHCPRG/WHCPRG
      integer iun
      common/c_unit/iun
      real * 8 ub_btilde_corr,ub_remn_corr,ub_corr
      real * 8 powheginput
      external powheginput
      integer maxevin
      
      integer iarg, ios
      real * 8 tmp
      character * 100 arg
      integer radtype
      common/radtype/radtype
      logical vetoEvent
      real * 8 features(28), xgcut, XGBpredict
      integer nfeatures
      integer maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer mjets,jetvechep(maxtracks)
      real * 8 pjet(4,maxjets)
      logical isForClustering(maxtracks)
      integer jetvec(maxtracks)
      real * 8 pt, ptmaxb, ptmaxj
      integer iptmaxb, iptmaxj
      logical bjettag(maxjets)
      integer nbjetswritten

      logical, save :: ini = .true., vetoFrag, recoFrag, vetoOR, keepTTonly

      logical foundTop, foundATop

      real * 8 ptop_uborn(0:4), patop_uborn(0:4)
      common/top_uborn/ptop_uborn, patop_uborn

c     any number of symbol=value on the command line
c     is interpreted as overriding the powheg.input value.
c     Furthermore, the special keyword iwhichseed=<integer>
c     assigns the seed number of parallel runs, preventing
c     the program to inquire for it later.
      iarg=1
      call get_command_argument(iarg,arg)
      do while(arg /= '')
         j=index(arg,'=')
         if(j>0) then
            read(arg(j+1:),fmt=*,iostat=ios) tmp
         else
            ios = -1
         endif
         if(ios == 0) then
            call powheginputoverride(arg(1:j-1),tmp)
         else
            write(*,*) 'pwhg_main: error in command line argument ',iarg
            call exit(-1)
         endif
         iarg=iarg+1
         call get_command_argument(iarg,arg)
      enddo

      if (ini) then
         vetoFrag = powheginput('#LHEVetoFrag').eq.1
         recoFrag = powheginput('#LHERecoFrag').eq.1
         vetoOR = powheginput('#LHEVetoOR').eq.1
         keepTTonly = powheginput('#keepTTonly').eq.1
         ini = .false.
      endif

c     let the analysis subroutine know that it is run by this program
      WHCPRG='LHE   '

      ub_btilde_corr = powheginput('#ub_btilde_corr')
      if (ub_btilde_corr < 0d0) then
         ub_btilde_corr = 1d0
      endif
      ub_remn_corr = powheginput('#ub_remn_corr')
      if (ub_remn_corr < 0d0) then
         ub_remn_corr = 1d0
      endif
     
      call XGBinitialize()
 
      call opencountunit(nev,iun)
      call pwhgsetnumberofevents(nev)
      call upinit
      call init_hist 
      nevpassed = 0
      maxevin = powheginput('#maxev')
      if (maxevin>0.and.maxevin<=nev) then
        nev = maxevin
      endif
      do j=1,nev
         call upevnt
         radtype=rad_type
         if(nup.eq.0) then
            write(*,*) ' nup = 0 skipping event'
            goto 111
         endif
c rescale the weight of the event depending on the rad_type (1..btilde, 2..remn)
c   using the ub_..._corr factors
         if (rad_type == 1) then
            ub_corr = ub_btilde_corr
         else if (rad_type == 2) then
            ub_corr = ub_remn_corr
         else 
            ub_corr = 1d0
         endif
         if (rwl_num_weights.gt.0) then
            rwl_weights(1:rwl_num_weights)=
     $           ub_corr * rwl_weights(1:rwl_num_weights)
         endif
         xwgtup = ub_corr * xwgtup
         foundTop = .false.
         foundATop = .false.
         do k =1, nup
            if (idup(k) == 6) then
               foundTop = .true.
            else if (idup(k) == -6) then
               foundATop = .true.
            endif
         enddo
         ptop_uborn(:) = 0
         patop_uborn(:) = 0
         do k = 1, nup_uborn
            if ( idup_uborn(k) == 5 .or. idup_uborn(k) == -11 .or. idup_uborn(k) == 12 ) then
               ptop_uborn(:) = ptop_uborn(:) + pup_uborn(:,k)
            else if ( idup_uborn(k) == -5 .or. idup_uborn(k) == 13 .or. idup_uborn(k) == -14 ) then
               patop_uborn(:) = patop_uborn(:) + pup_uborn(:,k)
            endif
         enddo
c         if (foundTop .and. foundATop) then
c            print*,"doublyRes"
c         endif
         if ( keepTTonly .and. ( (.not. foundTop) .or. (.not. foundATop) ) ) cycle
         call lhuptohepevt(j)
         if(abs(idwtup).eq.3) xwgtup=xwgtup*xsecup(1)
         if (mod(j,20000).eq.0) then
            write(*,*) "# of events processed =",j
            call lheanend
         endif
c veto or manipulate the event
         if (vetoEvent(vetoOR, vetoFrag)) cycle
         if (recoFrag) call reabsorbWmEmission()
         if (powheginput("#xgbcut").eq.1) then
c        build jets for xgbcut
            isForClustering = .false.
            do k=1,nhep
               if(isthep(k) == 1) then
                  if(abs(idhep(k)).lt.11.or.abs(idhep(k)).gt.16) isForClustering(k) = .true.
               endif
            enddo
            mjets = maxjets
            call buildjets2(mjets,-1d0,pjet,jetvec,isForClustering)
c           tag and find hardest jets
            ptmaxb = 0d0
            ptmaxj = 0d0
            iptmaxb = 0
            iptmaxj = 0
            bjettag = .false.
            do k=1,nhep
c              if the particle in question is a part of the jet
               if (jetvec(k) > 0) then
                  if (abs(idhep(k)) == 5) then
                     bjettag(jetvec(k)) = .true.
                  else 
                     bjettag(jetvec(k)) = .false. .or. bjettag(jetvec(k))
                  endif
               endif
            enddo
            do k = 1,mjets
               pt = sqrt(pjet(1,k)**2+pjet(2,k)**2)
               if (bjettag(k)) then
                  if (pt > ptmaxb) then
                     ptmaxb = pt
                     iptmaxb = k
                  endif
               else 
                  if (pt > ptmaxj) then
                     ptmaxj = pt
                     iptmaxj = k
                  endif
               endif
            enddo
c do the xgboost cut
            nfeatures = 1
            do k = 1, nup
               if (istup(k) == 1 .and. abs(idup(k))>10 .and. idup(k) /= 21) then
                  do l = 1, 4
                     features(nfeatures) = pup(l,k)
                     nfeatures = nfeatures + 1
                  enddo
               endif
            enddo
            nbjetswritten = 0
            do k = 1, mjets
               if (bjettag(k)) then
                  do l = 1, 4
                     features(nfeatures) = pjet(l,k)
                     nfeatures = nfeatures + 1
                  enddo
                  nbjetswritten = nbjetswritten + 1
               endif
            enddo
            if (nbjetswritten == 2) then
            else if (nbjetswritten == 1) then
               do k = 1, 4
                  features(nfeatures) = 0d0
                  nfeatures = nfeatures + 1
               enddo
            else
               print*,"Shouldn't get here! Exiting!"
               call exit()
            endif
            if (ptmaxj > 0) then
               do k = 1, 4
                  features(nfeatures) = pjet(k,iptmaxj)
                  nfeatures = nfeatures + 1
               enddo
            else
               do k = 1, 4
                  features(nfeatures) = 0d0
                  nfeatures = nfeatures + 1
               enddo
            endif
            xgcut = XGBpredict(nfeatures-1,features)
            if (xgcut < 0.5) cycle
         endif
         call analysis(xwgtup)
         call pwhgaccumup
         nevpassed = nevpassed + 1
111      continue
      enddo
      call lheanend
      call finalize(rnd_cwhichseed)
      write(*,*) 'EVENTS FOUND: ',nev
      write(*,*) 'EVENTS ANALYZED: ',nevpassed
      call xgbfinalize()
      end

      subroutine lheanend
      character * 20 pwgprefix
      character * 100 filename
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      include 'pwhg_rnd.h'
      if(rnd_cwhichseed.ne.'none') then
         filename=pwgprefix(1:lprefix)//'LHEF_analysis-'
     1        //rnd_cwhichseed
      else
         filename=pwgprefix(1:lprefix)//'LHEF_analysis'
      endif
      call pwhgsetout
      call pwhgtopout(filename)
      end
      
      subroutine UPINIT
      implicit none
      integer iun
      common/c_unit/iun
      call lhefreadhdr(iun)
      end

      subroutine UPEVNT
      integer iun
      common/c_unit/iun
      call lhefreadev(iun)
      end

      subroutine lhuptohepevt(n)
      implicit none
      include 'hepevt.h'
      include 'LesHouches.h'
      integer ihep,mu,n
      
      nhep=nup
      nevhep=n
      do ihep=1,nhep
         isthep(ihep)=istup(ihep)
         idhep(ihep)=idup(ihep)
         do mu=1,2
            jmohep(mu,ihep)=mothup(mu,ihep)
         enddo
         do mu=1,5
            phep(mu,ihep)=pup(mu,ihep)
         enddo
      enddo
      end

      subroutine getparallelparms(parallelstage,xgriditeration)
      implicit none
      integer parallelstage,xgriditeration
      parallelstage = 5
      xgriditeration = 0
      end

      function vetoEvent(vetoOR, vetoFrag)
c veto LHE events:
c  in this veto we throw away following events 
c     a) if vetoOR: in which gluon originating from anywhere else enters the W+ decay product jet-cones
c     b) if vetoFrag: in which the gluon from W+ escapes the W+ decay product jet-cones
      implicit none
      logical vetoOR,vetoFrag,vetoEvent
      include 'LesHouches.h'
      include 'hepevt.h'
      integer maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer mjets,jetvechep(maxtracks)
      real * 8 kt(maxjets),eta(maxjets),rap(maxjets),
     1     phi(maxjets),p(4,maxjets)
      logical isForClustering(maxtracks)
      integer jetvec(maxtracks)
      logical containsOtherRadiation, containsGluonFromWm
      integer iDjet, iUjet
      integer jhep
      integer ng
      logical vetoORRes, vetoFragRes
c initialize vetoEvent
      vetoEvent = .false.
c if no veto is activated then exit returning false
      if (.not.(vetoOR .or. vetoFrag)) return
c otherwise build jets
      isForClustering = .false.
      do jhep=1,nhep
         if(isthep(jhep) == 1) then
            if(abs(idhep(jhep)).lt.11.or.abs(idhep(jhep)).gt.16) isForClustering(jhep) = .true.
         endif
      enddo
      mjets = maxjets
      call buildjets(mjets,-1d0,kt,eta,rap,phi,p,jetvec,isForClustering)
c find u and d jets
      iDjet = -1
      iUjet = -1
      do jhep=1,nhep
         if ( idhep(jhep).eq.3 ) iDjet = jetvec(jhep)
         if ( idhep(jhep).eq.-4 ) iUjet = jetvec(jhep)
      enddo
c veto other radiation
      vetoORRes = .false.
      if (vetoOR) then
         vetoORRes = ( containsOtherRadiation(iDjet, jetvec, nhep) .or. containsOtherRadiation(iUjet, jetvec, nhep) ) 
c note that containsOtherRadiation returns false if idjet is -1
c     this covers the case of leptonic decay, in the case of hadronic decay both iDjet and iUjet will be
c     set, to different values if these form two different jets or to the same if they end up in the same jet
c     (there is no pt threshold)
      endif
c veto fragmentation
      vetoFragRes = .false.
      if (vetoFrag) then
         ng = 0
         if (iDjet>0 .and. iUjet>0) then
            if ( containsGluonFromWm(iDjet, jetvec, nhep) ) ng = ng + 1
            if ( containsGluonFromWm(iUjet, jetvec, nhep) ) ng = ng + 1
            if (ng==0) then
               vetoFragRes = .true.
            elseif (ng==1) then
               vetoFragRes = .false.
            elseif (iDjet == iUjet) then ! ng can be 2 if the two jets are in fact the same jet
               vetoFragRes = .false.
            else 
               print*, "More than one gluons in W- decay? WFT!"
               print*, "iDjet = ", iDjet
               print*, "iUjet = ", iUjet
               print*, "jetvec = ", jetvec
               call exit(-1)
            endif
         endif
      endif
c combine the result
      vetoEvent = vetoORRes .or. vetoFragRes
      return
      end

      subroutine reabsorbWmEmission
      implicit none
      include 'LesHouches.h'
      include 'hepevt.h'
      integer iDquark, iUquark, iGluon
      integer jhep
      double precision dy, deta, dphi, drU, drD
c go through the even and find all the partons
      iDquark = -1
      iUquark = -1
      iGluon = -1
      do jhep=1,nhep
         if( isthep(jhep) == 1 ) then
            if ( mothup(1,jhep) > 0 .and. idhep(mothup(1,jhep)) == -24 ) then ! if the mother is a W-
               if ( idhep(jhep).eq.3 ) iDquark = jhep
               if ( idhep(jhep).eq.-4 ) iUquark = jhep
               if ( idhep(jhep).eq.21 ) iGluon = jhep
            endif
         endif
      enddo
      if (iDquark < 1 .or. iUquark < 1 .or. iGluon < 1) return
c measure deltaR
      call getdydetadphidr(phep(1:4,iDquark), phep(1:4,iGluon), dy, deta, dphi, drD)
      call getdydetadphidr(phep(1:4,iUquark), phep(1:4,iGluon), dy, deta, dphi, drU)
c depending on which quark the gluon is closer to merge
      if (drD < drU) then
         call mergeParticles(iDquark, iGluon)
      else 
         call mergeParticles(iUquark, iGluon)
      endif
      end

      subroutine mergeParticles(iem, irad)
      implicit none
      integer iem, irad
      include 'hepevt.h'
      integer NEVHEPcp, NHEPcp, ISTHEPcp(NMXHEP), IDHEPcp(NMXHEP), JMOHEPcp(2,NMXHEP), JDAHEPcp(2,NMXHEP)
      double precision PHEPcp(5,NMXHEP),VHEPcp(4,NMXHEP)
      integer jhep
c     backup the current event record
      NHEPcp = NHEP
      ISTHEPcp = ISTHEP
      IDHEPcp = IDHEP
      JMOHEPcp = JMOHEP
      JDAHEPcp = JDAHEP
      PHEPcp = PHEP
      VHEPcp = VHEP
c     copy everything over into the new event record except for the particle irad
      do jhep=1,NHEP
         if (jhep .ne. irad) then
            ISTHEP(jhep) = ISTHEPcp(jhep)
            IDHEP(jhep) = IDHEPcp(jhep)
            JMOHEP(:,jhep) = JMOHEPcp(:,jhep)
            JDAHEP(:,jhep) = JDAHEPcp(:,jhep)
            PHEP(:,jhep) = PHEPcp(:,jhep)
            VHEP(:,jhep) = VHEPcp(:,jhep)
         endif
      enddo
c     reduce the nevhep and nhep numbers
      NHEP = NHEPcp - 1
c     and merge the momenta
      PHEP(:,iem) = PHEPcp(:,iem) + PHEPcp(:,irad)
      end

      function containsOtherRadiation(idjet, jetvec, maxtracks)
      implicit none
      integer idjet
      integer jetvec(maxtracks)
      integer maxtracks
      logical containsOtherRadiation
      include 'hepevt.h'
      include 'LesHouches.h'
      integer jtrack
      if (idjet == -1) then
         containsOtherRadiation = .false.
      else 
         containsOtherRadiation = .false.
         do jtrack=1,maxtracks
            if ( jetvec(jtrack) == idjet ) then
c               print*,'  ', idhep(jtrack), mothup(1,jtrack), mothup(2,jtrack)
               if ( idhep(jtrack) == 21 .and. mothup(1,jtrack) < 5 .and. mothup(2,jtrack) < 5 ) then ! top is at position 3 and 4
                  containsOtherRadiation = .true.
                  exit
               endif
            endif
         enddo
      endif
      end

      function containsGluonFromWm(idjet, jetvec, maxtracks)
      implicit none
      integer idjet
      integer jetvec(maxtracks)
      integer maxtracks
      logical containsGluonFromWm
      include 'hepevt.h'
      include 'LesHouches.h'
      integer jtrack
      if (idjet == -1) then
         containsGluonFromWm = .false.
      else 
         containsGluonFromWm = .false.
         do jtrack=1,maxtracks
            if ( jetvec(jtrack) == idjet ) then
               if ( idhep(jtrack) == 21 .and. mothup(1,jtrack) > 0 .and. mothup(2,jtrack) > 0 ) then
                  if (idhep(mothup(1,jtrack)) == -24 .and. idhep(mothup(2,jtrack)) == -24 ) then
                     containsGluonFromWm = .true.
                     exit
                  endif
               endif
            endif
         enddo
      endif
      end

      subroutine buildjets(mjets,palg,kt,eta,rap,phi,pjet,jetvechep,
     1                                               isForClustering)
c     arrays to reconstruct jets
      implicit  none
      include  'hepevt.h'
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer   mjets,jetvechep(maxtracks)
      real * 8  kt(maxjets),eta(maxjets),rap(maxjets),
     1     phi(maxjets),pjet(4,maxjets)
      logical   isForClustering(maxtracks)
      real * 8  ptrack(4,maxtracks),pj(4,maxjets),ptot(4)
      integer   jetvec(maxtracks),itrackhep(maxtracks)
      integer   ntracks,njets
      integer   j,k,mu
      real * 8  r,palg,ptmin,pp,tmp
      integer sonofid
      external sonofid
C - Initialize arrays and counters for output jets
      kt = 0
      eta = 0
      rap = 0
      phi = 0
      pjet = 0
      jetvechep = 0

      ptrack = 0
      ntracks=0
      pj = 0
      jetvec = 0

C - Extract final state particles to feed to jet finder
      do j=1,nhep
         if(.not.isForClustering(j)) cycle
         if(ntracks.eq.maxtracks) then
            write(*,*) 'analyze: need to increase maxtracks!'
            write(*,*) 'ntracks: ',ntracks
            call exit(-1)
         endif
         ntracks=ntracks+1
         ptrack(:,ntracks) = phep(1:4,j)
         itrackhep(ntracks)=j
      enddo
      if (ntracks.eq.0) then
         mjets=0
         return
      endif
cDEL      if(palg.eq.11) then
cDELc better to boost everything to the total CM of the tracks
cDEL         ptot = 0
cDELc         print*, 'ntracks in buildjets for epem',ntracks
cDEL         do j=1,ntracks
cDEL            ptot = ptot + ptrack(:,j)
cDEL         enddo
cDEL         call boost2reson4(ptot,ntracks,ptrack,ptrack)
cDEL      endif
C --------------- C
C - Run FastJet - C
C --------------- C
C - R = 0.7   radius parameter
C - f = 0.75  overlapping fraction
      r     = 0.5d0
      ptmin = 0d0
      njets=mjets
      call fastjetppgenkt(ptrack,ntracks,r,palg,ptmin,pjet,njets,jetvec)
      mjets=min(mjets,njets)
      if(njets.eq.0) return
c check consistency
      do k=1,ntracks
         if(jetvec(k).gt.0) then
            do mu=1,4
               pj(mu,jetvec(k))=pj(mu,jetvec(k))+ptrack(mu,k)
            enddo
         endif
      enddo
      tmp=0
      do j=1,mjets
         do mu=1,4
            tmp=tmp+abs(pj(mu,j)-pjet(mu,j))
         enddo
      enddo
      if(tmp.gt.1d-4) then
         write(*,*) ' bug!'
      endif
C --------------------------------------------------------------------- C
C - Computing arrays of useful kinematics quantities for hardest jets - C
C --------------------------------------------------------------------- C
      do j=1,mjets
         kt(j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
         pp = sqrt(kt(j)**2+pjet(3,j)**2)
         eta(j)=0.5d0*log((pp+pjet(3,j))/(pp-pjet(3,j)))
         rap(j)=0.5d0*log((pjet(4,j)+pjet(3,j))/(pjet(4,j)-pjet(3,j)))
         phi(j)=atan2(pjet(2,j),pjet(1,j))
      enddo
      jetvechep = 0
      do j=1,ntracks
         jetvechep(itrackhep(j))=jetvec(j)
      enddo
      end

      subroutine getyetaptmass(p,y,eta,pt,mass)
      implicit none
      real * 8 p(4),y,eta,pt,mass,pv
      real *8 tiny
      parameter (tiny=1.d-5)
      y=0.5d0*log((p(4)+p(3))/(p(4)-p(3)))
      pt=sqrt(p(1)**2+p(2)**2)
      pv=sqrt(pt**2+p(3)**2)
      if(pt.lt.tiny)then
         eta=sign(1.d0,p(3))*1.d8
      else
         eta=0.5d0*log((pv+p(3))/(pv-p(3)))
      endif
      mass=sqrt(abs(p(4)**2-pv**2))
      end

      subroutine getdydetadphidr(p1,p2,dy,deta,dphi,dr)
      implicit none
      include 'pwhg_math.h' 
      real * 8 p1(*),p2(*),dy,deta,dphi,dr
      real * 8 y1,eta1,pt1,mass1,phi1
      real * 8 y2,eta2,pt2,mass2,phi2
      call getyetaptmass(p1,y1,eta1,pt1,mass1)
      call getyetaptmass(p2,y2,eta2,pt2,mass2)
      dy=y1-y2
      deta=eta1-eta2
      phi1=atan2(p1(1),p1(2))
      phi2=atan2(p2(1),p2(2))
      dphi=abs(phi1-phi2)
      dphi=min(dphi,2d0*pi-dphi)
      dr=sqrt(deta**2+dphi**2)
      end

      subroutine buildjets2(mjets,palg,pjet,jetvechep,isForClustering)
c     arrays to reconstruct jets
      implicit  none
      include  'hepevt.h'
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer   mjets,jetvechep(maxtracks)
      real * 8  kt(maxjets),eta(maxjets),rap(maxjets),
     1     phi(maxjets),pjet(4,maxjets)
      logical   isForClustering(maxtracks)
      real * 8  ptrack(4,maxtracks),pj(4,maxjets),ptot(4)
      integer   jetvec(maxtracks),itrackhep(maxtracks)
      integer   ntracks,njets
      integer   j,k,mu
      real * 8  r,palg,ptmin,pp,tmp
      integer sonofid
      external sonofid
C - Initialize arrays and counters for output jets
      kt = 0
      eta = 0
      rap = 0
      phi = 0
      pjet = 0
      jetvechep = 0

      ptrack = 0
      ntracks=0
      pj = 0
      jetvec = 0

C - Extract final state particles to feed to jet finder
      do j=1,nhep
         if(.not.isForClustering(j)) cycle
         if(ntracks.eq.maxtracks) then
            write(*,*) 'analyze: need to increase maxtracks!'
            write(*,*) 'ntracks: ',ntracks
            call exit(-1)
         endif
         ntracks=ntracks+1
         ptrack(:,ntracks) = phep(1:4,j)
         itrackhep(ntracks)=j
      enddo
      if (ntracks.eq.0) then
         mjets=0
         return
      endif
cDEL      if(palg.eq.11) then
cDELc better to boost everything to the total CM of the tracks
cDEL         ptot = 0
cDELc         print*, 'ntracks in buildjets for epem',ntracks
cDEL         do j=1,ntracks
cDEL            ptot = ptot + ptrack(:,j)
cDEL         enddo
cDEL         call boost2reson4(ptot,ntracks,ptrack,ptrack)
cDEL      endif
C --------------- C
C - Run FastJet - C
C --------------- C
C - R = 0.7   radius parameter
C - f = 0.75  overlapping fraction
      r     = 0.5d0
      ptmin = 0d0
      njets=mjets
      call fastjetppgenkt(ptrack,ntracks,r,palg,ptmin,pjet,njets,jetvec)
      mjets=min(mjets,njets)
      if(njets.eq.0) return
c check consistency
      do k=1,ntracks
         if(jetvec(k).gt.0) then
            do mu=1,4
               pj(mu,jetvec(k))=pj(mu,jetvec(k))+ptrack(mu,k)
            enddo
         endif
      enddo
      tmp=0
      do j=1,mjets
         do mu=1,4
            tmp=tmp+abs(pj(mu,j)-pjet(mu,j))
         enddo
      enddo
      if(tmp.gt.1d-4) then
         write(*,*) ' bug!'
      endif
C --------------------------------------------------------------------- C
C - Computing arrays of useful kinematics quantities for hardest jets - C
C --------------------------------------------------------------------- C
      do j=1,mjets
         kt(j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
         pp = sqrt(kt(j)**2+pjet(3,j)**2)
         eta(j)=0.5d0*log((pp+pjet(3,j))/(pp-pjet(3,j)))
         rap(j)=0.5d0*log((pjet(4,j)+pjet(3,j))/(pjet(4,j)-pjet(3,j)))
         phi(j)=atan2(pjet(2,j),pjet(1,j))
      enddo
      jetvechep = 0
      do j=1,ntracks
         jetvechep(itrackhep(j))=jetvec(j)
      enddo
      end


