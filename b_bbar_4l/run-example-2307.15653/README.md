# Instructions to reproduce the samples in arXiv:2307.15653

In this document we briefly describe how this beta version of the
POWHEG-BOX-RES/b_bbar_4l can be used to produce samples equivalent to those in
arXiv:2307.15653.

This code can be used to produce samples in the dileptonic channel (with both W
bosons decaying leptonically) as well as in the semileptonic channel (with one
of the W boson decaying leptonically and the other decaying hadronically).
However, in order to produce a semileptonic sample a dileptonic sample has to
be produced first.

In the next two sections we describe how to produce the dileptonic sample and
the semileptonic sample (starting from the dileptonic sample). 

## Scope of this version

This is a beta version of the new bb4l code. By using it you acknowledge that
you understand that running the code in a mode not described here and/or with
values of flags different from what is in the `powheg.input` in this directory
may lead to unphysical results which we may not have time to provide any
feedback on.

## The dileptonic sample

In order to produce the dileptonic sample, we use the usual POWHEG BOX RES
setup with the `pwhg_main` program.  In the following we describe our setup,
which relies on the `manyseed 1` mode. If you want to run in `manyseed 0` mode,
be sure to scale the numbers of calls correspondingly.

### Physics inputs

All the relevant inputs are read from `powheg.input` in this directory.

In our samples from arXiv:2307.15653, the following inputs were used, which
deviate from some of the original `bb4l` defaults:
```
twidth     1.32733      ! default: calculated
zwidth     2.5108       ! default: 2.441
wwidth     2.10134      ! default: 2.04807 
```
In the release candidate version also the values of zwidth and wwidth will be
calculated, similarly to the twidth.

To reproduce the results of the technical studies in arXiv:2307.1565 a fixed
renormalisation and factorization scale choice equal to the value of the input
top mass should be used:
```
fixedscale 1

```
However, it is not our intention to recommend this scale choice for comparisons
against data. A suitable, resonance history dependent, dynamical scale choice is
available under the choice 
```
fixedscale 0
```
which is the default.

We rely on the usual `for_reweighting 1` trick in which the virtual corrections
are initially switched off and the subtraction is modified. In this setup, the
virtual corrections are not used at all until events have been generated, in
stage 4,  at which point the weight of each event is recalculated with the
virtual correction switched on and the subtraction modifications undone on the
fly. The final sample then features two weights, with the 0th weight in
`XWGTUP` being unphysical (without the virtual and modified subtraction) and
the 1st weight in the `<weight>` block. The sample is unweighted, in any
optional following reweighting steps, with respect to `XWGTUP`. With
`for_reweighting 0,` the sample will be generated with the virtual correction
switched on in each step and will also be included in `XWGTUP`.


### 4 to 5FNS matching bug

During the implementation of the semileptonic decay channel, we discovered a
bug in the 4>5FNS flavour matching prescription, which was originally
controlled by the `MSbarscheme 1` flag.  We corrected this bug and replaced the
`MSbarscheme` flag by the following series of technical flags:
```
fourToFiveMatch 1
fourToFiveMatchAS 0
st_nlight_default 5
st_nlight_as 5
st_nlight_ssv 4
```
These settings __should not be changed__. They are indispensable in order to
guarantee the correct 4>5FNS flavour matching, i.e. when bb4l which implements
a four-flavour calculation is used together with five-flavour PDFs.  In the
release candidate version those flags will be removed and the matching will be
switched on and off automatically depending on the flavour scheme of the PDF.

### Improved resonance history projectors

In the original bb4l, there are two resonance histories: 1.) tt~, 2.) Zbb~,
where the first resonance history stands in also for the single top resonance
histories.

In the new bb4l, we implemented a more reliable treatment of WWbb production
based three RHs: 1.) tt~, 2.) tW+, 3.) t~W-. This is achieved by
```
RHWithSingleTops 1
```
Note that for reliable sampling in single top resonance histories, one must
switch off adaptive sampling in the integer sampling:
```
dontAdaptWind 1
```

The new bb4l also implements improved resonance history projectors based on
Born matrix elements and resonance aware real to born phase space mappings.
Those are needed for a realistic separation of resonance history. They are
enabled with `RHStrategy 1` And are currently available only for when
`RHWithSingleTops 1` (which is the recommended choice of resonance histories).

### The inverse width correction 

The code should be run with the inverse width correction described in section 3
of arXiv:2307.15653 switched on
```
widthCorrection 5
```
Since the width correction is implemented only within the reweighting framework, this 
flag will only work if and only if the `for_reweighting 1` mode is enabled.

If you run with `for_rewrighting 0`, then you need to switch on the width correction as
an extra weight as follows:
```
rwl_file '-'
<initrwgt>
<weight id='1'> widthCorrection=5 </weight>
</initrwgt>
```

### Numerical instabilities

Note that a vital component of the matrix element improved resonance histories
currently suffers from a numerical instability upon which the code exits. This
instability will be addressed in the release candidate version of the code. It
manifests itself in a fraction of unfinished runs at all stages of the
`pwhg_main` run. If that happens in stages 1 to 3, the result of this runs will
simply be ignored as unfinished runs are not used further. This happening in
stage 4 will simply lead to an incomplete event file which is however safe to
be used in the further stages. So the only symptom one needs to worry about is
loss of statistics. 

### Stage 1

In stage 1 POWHEG BOX calculates the adaptive sampling for the integration in
stage 2. We run this stage on 200 cores and three xgrid iterations. Make sure
to suitably adapt the value of `ncall1` if you plan to run on a different
number of cores. 
```
ncall1 160000 ! value for one core
```
Also note that `itmx1` is ignored in `manyseed 1` mode and each xgrid iteration
must be run manually in a sequential order.  The parameter `xgriditeration`
controls the however manyeth xgrid iteration to run.

Here are average runtime on our machines per core per xgrid iteration are:
- xg1: 1.4h
- xg2: 1.4h
- xg3: 1.4h

### Stage 2

In stage 2 POWHEG BOX calculates the differential NLO cross sections. We run
this stage, as stage 1, on 200 cores.  With the following setup 
```
ncall2 200000 ! value for one core
itmx2   3
foldcsi   1
foldy     1
foldphi   1
```
we arrive at total cross section estimate with 1.2 permil accuracy and negative
fraction of about 20%. (For us the fraction of negative weighted events is not
an issue, if it is for you then increase the values for `foldcsi`, `foldy` and
`foldphi`. In our experience the code responds well to this and the negative
fractions can be brought down to 5% without too much effort).

Average run time per core is 4.9h.

### Stage 3

Stage 3 is usually used for the calculation of the radiation upper bound. Since
we run in the `storemintupb 1` mode stage 3 also estimates the btilde upper
bound from all the calls to the matrix element in stage 2. This is a must,
because the value of `nubound` needs to be adapted to the number of legs and
values for `nubound` suitable for radiation are too small for a reliable btilde
upper bound whereas values of `nubound` suitable for btilde upper  bound are an
overkill for the radiation upper bound.  The btilde upper bound is estimated on
a single core, while all the other cores wait for the results.  We run stage 3
also on 200 cores with
```
nubound 20000
```

Run time for the core calculating the upper bound is 14h while the average run time 
for the remaining core (not counting the waiting time) is 0.5h.

### Stage 4

In stage 4 events are generated and stored in .lhe files. If you intend to
redecay these events using the `pwhg_semileptonic` program, in order to obtain
a sample in the semileptonic channel, you need to run with
```
lhefuborn 1
```
which triggers writing out of underlying born kinematics in the .lhe file. Such
files are backward compatible within the POWHEG framework, but may not be
compatible with other LHE readers.

Also note that only events generated in
```
allrad 1
```
are meant to be redecayed. Even though, it is also possible to redecay events
with `allrad 0`, this option should be avoided. 

The example run relies on the `ub_excess_correct` feature. When this is enabled
instead of throwing away btilde configurations that violated the upper bound,
which can lead to distortions in some regions of phase space, the events are
kept with a weight proportional to the ratio of the (weight)/(upper bound) *
(nominal weight). This means that the sample is no longer perfectly unweighted
and the nominal weights will no longer add up to the total cross section. In
order to correct for that, one needs to run the  `FindReweightFromCounters.py`
python script which calculates the values `ub_btilde_corr` and `ub_remn_corr`
which should multiply the weights of all the btilde and remnant events,
respectively, before the events are passed on to the shower. If the values are
1 the samples didn't encounter any btlide events that violated the upper bound.
In my sample these values deviate from unity only by a couple-few permil. 

The speed with which the events are generated strongly depends on the btilde
upper bound and in our runs varies anywhere between 500-5000 events per hour. 
We typically generate in batches of 10k events.

## The semileptonic sample

Any bb4l dileptonic sample generated with the `lhefuborn 1` flag, can in
principle be recast or redecayed into a semileptonic sample. This is 
done with the `pwhg_semileptonic` program.

The inputs are read from the `powheg.input` file, just like in the case of the
`pwhg_main` program. For the moment only `manyseed 1` mode is supported.

The `pwhg_semileptonic` program only features 2 stages: stage 3 and stage 4
numbered in analogy with the stages of the `pwhg_main` program. 

The `pwhg_semileptonic` program will only redecay one of the leptonically
decaying W bosons, thereby converting it into an hadronically decaying W boson.
Choosing 
```
whichW -1
```
yields an hadronically decaying W- boson, while `whichW 1` yields an hadronically
decaying W+ boson.

### Stage 3

In stage 3 the radiation upper bound is calculated. We run stage 3 on 200 cores with 
```
nubound 10000
```
We recommend to provide custom numbers of calls for the calculation of the
`btildeborn` contribution:
```
ncall1btlbrn 1000000
itmx1btlbrn 5
ncall2btlbrn 1000000
itmx2btlbrn 5
```

The program is run similarly to `pwhg_main`, just set `parallelstage 3` in
`powheg.input`, and run the program `./pwhg_semileptonic` and the program will
ask you which random seed from `pwgseeds.dat` to use.

Average runtime for this stage is about 2.1h.

### Stage 4

In stage 4 dilepton events from bb4l stored in `pwgevents-????.lhe` are read
in, redecayed into semileptonic events and stored in
`pwgevents-????-semileptonic.lhe`.

The program is run similarly to `pwhg_main`, just set `parallelstage 4` in
`powheg.input`, and run the program `./pwhg_semileptonic` and the program will
prompt you regarding the name of the event file that should be redecayed:
```
echo pwgevents-0001.lhe | ./pwhg_semileptonic
```

Average speed for redecaying events is about 12k per hour. 

### Reweighting of redecayed sample

Reweighting of the redecayed sample is currently not implemented, if you need you
can always add more weights in the original dileptonic sample and then redecay
it again. The computational cost of redecaying does not depend on the number of
weights.
