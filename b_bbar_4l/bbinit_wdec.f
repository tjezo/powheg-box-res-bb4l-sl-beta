      subroutine setstage2init
      implicit none
      real * 8 powheginput
      if(powheginput("#stage2init").eq.1d0) then
         call resetrandom
      endif
      end

      subroutine gen_btilde(mcalls,icalls)
      implicit none
      include 'nlegborn.h'
      integer mcalls,icalls
      real * 8 xx(ndiminteg)      
      integer ind
      real * 8 btilde
      external btilde
      call genwrapper('btilde',1,mcalls,icalls,xx,ind)
      end

      subroutine gen_sigremnant
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'cgengrids.h'
      real * 8 xx(ndiminteg)
      integer mcalls,icalls,ind
      logical savelogical
      real * 8 sigremnant
      external sigremnant
c communicate file to load upper bound data
      savelogical=flg_fastbtlbound
      flg_fastbtlbound=.false.
      call genwrapper('remn',1,mcalls,icalls,xx,ind)
      flg_fastbtlbound=savelogical
      end

      subroutine gen_sigregular
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'cgengrids.h'
      real * 8 xx(ndiminteg)
      integer mcalls,icalls,ind
      logical savelogical
      real * 8 sigremnant
      external sigremnant
c communicate file to load upper bound data
      savelogical=flg_fastbtlbound
      flg_fastbtlbound=.false.
      call genwrapper('reg',1,mcalls,icalls,xx,ind)
      flg_fastbtlbound=savelogical
      end

      function mergelabels(lab1,lab2,lab3,lab4)
c puts together up to 4 labels, separating them with '-'.
c Empty labels are ignored.
      character * 40 mergelabels
      character *(*) lab1,lab2,lab3,lab4
      integer where
      mergelabels=' '
      if(lab1.ne.' '.and.lab1.ne.'none') then
         mergelabels=adjustl(lab1)
         where=index(mergelabels,' ')
         if(where.eq.0) goto 999
      endif
      if(lab2.ne.' '.and.lab2.ne.'none') then
         mergelabels(where:)='-'//adjustl(lab2)
         where=index(mergelabels,' ')
         if(where.eq.0) goto 999
      endif
      if(lab3.ne.' '.and.lab3.ne.'none') then
         mergelabels(where:)='-'//adjustl(lab3)
         where=index(mergelabels,' ')
         if(where.eq.0) goto 999
      endif
      if(lab4.ne.' '.and.lab4.ne.'none') then
         mergelabels(where:)='-'//adjustl(lab4)
         where=index(mergelabels,' ')
         if(where.eq.0) goto 999
      endif
c get rid of hiphen before extension
      where=index(mergelabels,'-.',.true.)
      if(where.ne.0) then
         mergelabels(where:)=mergelabels(where+1:)
      endif
      return
 999  write(*,*) ' mergelabels: strings too long'
      call pwhg_exit(-1)
      end

      subroutine setrandomseed(index)
      implicit none
      integer index
      include 'pwhg_rnd.h'
      integer itmp,j
      real * 8 random
      external random
      if(index.lt.0) then
         itmp = 0
      else
         call resetrandom
         do j=1,index
            itmp = random()*1d9
         enddo
      endif
      call setrandom(rnd_initialseed+itmp,rnd_i1,rnd_i2)
      end

      subroutine setparallellabel(parallelstages,xgriditeration,parlabel)
      implicit none
      integer parallelstages,xgriditeration
      character *(*) parlabel
      parlabel = ' '
      if(parallelstages.gt.0) then
         if(parallelstages.eq.1) then
            write(parlabel,'(a,i3)') 'xg',xgriditeration
            parlabel(3:)=adjustl(parlabel(3:))
         else
            write(parlabel,'(a,i1)') 'st',parallelstages
         endif
      endif
      end
