c     Analysis from 1312.0546 + Extension (exclusive (b)jet bins)

      subroutine init_hist
      implicit none
      include  'LesHouches.h'
      include 'pwhg_math.h'
      integer j,l
      character * 20 distprefix

      call inihists


      call bookupeqbins('total_no_cut',1d0,0d0,1d0)

      call bookupeqbins('total_0j_incl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_0j_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_1j_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2j_incl'  ,1d0,0d0,1d0)

      call bookupeqbins('total_0b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_1b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2b_incl'  ,1d0,0d0,1d0)

      call bookupeqbins('total_1j0b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2j0b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_1j1b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2j1b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2j2b_excl'  ,1d0,0d0,1d0)
      call bookupeqbins('total_2j2b_incl'  ,1d0,0d0,1d0)

      call bookupeqbins('0j_excl_pt_thresh'  ,10d0,5d0,405d0)
      call bookupeqbins('1j_excl_pt_thresh'  ,10d0,5d0,405d0)
      call bookupeqbins('0b_excl_pt_thresh'  ,10d0,5d0,405d0)
      call bookupeqbins('1b_excl_pt_thresh'  ,10d0,5d0,405d0)
      call bookupeqbins('1j0b_excl_pt_thresh',10d0,5d0,405d0)
      call bookupeqbins('2j0b_excl_pt_thresh',10d0,5d0,405d0)
      call bookupeqbins('1j1b_excl_pt_thresh',10d0,5d0,405d0)
      call bookupeqbins('2j1b_excl_pt_thresh',10d0,5d0,405d0)
      call bookupeqbins('2j2b_excl_pt_thresh',10d0,5d0,405d0)


      do j=1,12
        select case(j)
          case(1)
            distprefix='0j_incl'
          case(2)
            distprefix='0j_excl'
          case(3)
            distprefix='0b_excl'
          case(4)
            distprefix='1j_excl'
          case(5)
            distprefix='1b_excl'
          case(6)
            distprefix='1j0b_excl'
          case(7)
            distprefix='2j0b_excl'
          case(8)
            distprefix='1j1b_excl'
          case(9)
            distprefix='2j1b_excl'
          case(10)
            distprefix='2j2b_excl'
          case(11)
            distprefix='2j_incl'
          case(12)
            distprefix='2b_incl'
        end select
        call bookupeqbins(trim(distprefix)//'_pt_e+'      ,20d0,0d0,400d0)
        call bookupeqbins(trim(distprefix)//'_pt_l1'      ,20d0,0d0,400d0)
        call bookupeqbins(trim(distprefix)//'_pt_l2'      ,20d0,0d0,400d0)
        call bookupeqbins(trim(distprefix)//'_pt_miss'    ,20d0,0d0,400d0)
        call bookupeqbins(trim(distprefix)//'_phi_e+_mu-' ,pi/20,0d0,pi+1d-8)
        call bookupeqbins(trim(distprefix)//'_m_e+_mu-'   ,10d0,0d0,300d0)
      end do

      call bookupeqbins('1j_excl_Ht' ,50d0,0d0,1200d0)
      call bookupeqbins('1j_excl_Httot' ,50d0,0d0,1200d0)
      call bookupeqbins('1j_excl_pt_j'  ,20d0,0d0,400d0)

      call bookupeqbins('1b_excl_Ht', 50d0,0d0,1200d0)
      call bookupeqbins('1b_excl_Httot', 50d0,0d0,1200d0)
      call bookupeqbins('1b_excl_pt_b' , 20d0,0d0,400d0)

      call bookupeqbins('1j1b_excl_pt_l1_l2_ptmiss' ,20d0,0d0,400d0)
      call bookupeqbins('1j1b_excl_pt_l1_l2_ptmiss_j1' ,20d0,0d0,400d0)
      call bookupeqbins('1j1b_excl_dr_l1_j1' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('1j1b_excl_dr_l2_j1' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('1j1b_excl_m_l1_j1' ,40d0,0d0,600d0)
      call bookupeqbins('1j1b_excl_m_l2_j1' ,40d0,0d0,600d0)

      call bookupeqbins('2j1b_excl_pt_l1_l2_ptmiss' ,20d0,0d0,400d0)
      call bookupeqbins('2j1b_excl_pt_l1_l2_ptmiss_j1' ,20d0,0d0,400d0)
      call bookupeqbins('2j1b_excl_dr_l1_j1' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('2j1b_excl_dr_l2_j1' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('2j1b_excl_m_l1_j1' ,40d0,0d0,600d0)
      call bookupeqbins('2j1b_excl_m_l2_j1' ,40d0,0d0,600d0)
      call bookupeqbins('2j1b_excl_dr_l1_j2' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('2j1b_excl_dr_l2_j2' ,6d0/20d0,0d0,6d0)
      call bookupeqbins('2j1b_excl_m_l1_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j1b_excl_m_l2_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j1b_excl_m_j1_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j1b_excl_pt_j1'   ,20d0,0d0,400d0)
      call bookupeqbins('2j1b_excl_pt_j2'   ,20d0,0d0,400d0)
      call bookupeqbins('2j1b_excl_pt_j1_j2' ,20d0,0d0,400d0)

      call bookupeqbins('2j2b_excl_pt_l1_l2_ptmiss' ,20d0,0d0,400d0)
      call bookupeqbins('2j2b_excl_pt_l1_l2_ptmiss_j1',20d0,0d0,400d0)
      call bookupeqbins('2j2b_excl_dr_l1_j1',6d0/20d0,0d0,6d0)
      call bookupeqbins('2j2b_excl_dr_l2_j1',6d0/20d0,0d0,6d0)
      call bookupeqbins('2j2b_excl_m_l1_j1' ,20d0,0d0,600d0)
      call bookupeqbins('2j2b_excl_m_l2_j1' ,20d0,0d0,600d0)
      call bookupeqbins('2j2b_excl_dr_l1_j2',6d0/20d0,0d0,6d0)
      call bookupeqbins('2j2b_excl_dr_l2_j2',6d0/20d0,0d0,6d0)
      call bookupeqbins('2j2b_excl_m_l1_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j2b_excl_m_l2_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j2b_excl_m_j1_j2' ,40d0,0d0,600d0)
      call bookupeqbins('2j2b_excl_pt_j1'   ,20d0,0d0,400d0)
      call bookupeqbins('2j2b_excl_pt_j2'   ,20d0,0d0,400d0)
      call bookupeqbins('2j2b_excl_pt_j1_j2',20d0,0d0,400d0)

      call bookupeqbins('2j_incl_Ht' ,50d0,0d0,1200d0)
      call bookupeqbins('2j_incl_Httot' ,50d0,0d0,1200d0)
      call bookupeqbins('2j_incl_pt_j1' ,20d0,0d0,400d0)
      call bookupeqbins('2j_incl_pt_j2' ,20d0,0d0,400d0)

      call bookupeqbins('2b_incl_Ht'     ,50d0,0d0,1200d0)
      call bookupeqbins('2b_incl_Httot'  ,50d0,0d0,1200d0)
      call bookupeqbins('2b_incl_pt_b1'  ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_b2'  ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_bb'  ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_m_bb'   ,40d0,0d0,600d0)
      call bookupeqbins('2b_incl_phi_bb' ,pi/20,0d0,pi+1d-8)
      call bookupeqbins('2b_incl_m_wp_b1' ,10d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_wm_b1' ,10d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_wp_b2' ,10d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_wm_b2' ,10d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_ep_b1',40d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_em_b1',40d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_ep_b2',40d0,0d0,600d0)
      call bookupeqbins('2b_incl_m_em_b2',40d0,0d0,600d0)
      call bookupeqbins('2b_incl_pt_wp_b1' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_wm_b1' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_wp_b2' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_wm_b2' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_ep_b1' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_em_b1' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_ep_b2' ,20d0,0d0,400d0)
      call bookupeqbins('2b_incl_pt_em_b2' ,20d0,0d0,400d0)


      end

      subroutine analysis(dsig0)
      implicit none
      include 'hepevt.h'
      include 'pwhg_math.h' 
      include 'LesHouches.h'
      character * 6 whcprg      
      common/cwhcprg/whcprg
      integer jpref
      character * 20 prefix(18)
      character * 20 distprefix
      common/ccccprefix/jpref,prefix
      data whcprg/'NLO   '/
      integer, parameter :: dsigdim = 10
      real * 8  dsig0,dsig(dsigdim)
      logical asyanal
      common/casyanal/asyanal
      integer   ihep                ! HEPEVT index.
      real * 8 p_top(4),p_atop(4),p_wp(4),p_wm(4),p_lwp(4),p_lwm(4),
     1         p_nuwp(4),p_nuwm(4),p_j1(4),p_j2(4),p_b1(4),p_b2(4),
     2         ptzmf(4),plzmf(4), pbhadtm(4), pbhadtp(4), p_top_rec(4),
     3         p_l1(4),p_l2(4),ptmp(4)
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer mjets,njets,mbjets,nbjets,nabxjets,jetvec(maxtracks),njetsx,nbjetsx
      integer nbhadrons, nabhadrons
      integer bhadfromtop,bhadfromatop
      real * 8 ptbhadfromtop,ptbhadfromatop
      logical   isForClustering(maxtracks)
      real * 8 j_kt(maxjets),j_eta(maxjets),j_rap(maxjets),
     1     j_phi(maxjets),j_p(4,maxjets)
      integer i,j,tmp,n,id,i_top,i_atop,
     1     i_wp,i_wm,i_lwp,i_lwm,i_nuwp,i_nuwm,i_bjet,i_abjet,jhep,
     1     i_part
      integer i_first_nuwp,i_first_lwp,i_first_nuwm,i_first_lwm  
      real * 8 mtop,mtb,mwp,mwm,mb,mab,p_bmax,e_bmax,xb, p_abmax,e_abmax
     $     ,xab,ewp,pwp,ewm,pwm,xw,dy,deta,delphi,dr,cth1,cth2
      real * 8 :: y,eta,pt,mass,ht,httot
      real * 8 :: ptlwp,ptlwm,ptl1,ptl2,ptmiss
      real * 8 :: delphi_e_mu, m_e_mu, m_b_b, pt_b_b
      real * 8 :: m_ep_b1, m_em_b1, m_em_b2, m_ep_b2
      real * 8 :: m_wp_b1, m_wm_b1, m_wm_b2, m_wp_b2
      real * 8 :: pt_ep_b1,pt_em_b1,pt_ep_b2,pt_em_b2
      real * 8 :: pt_wm_b1,pt_wp_b1,pt_wm_b2,pt_wp_b2
      real * 8 :: delphi_b_b, dr_l1_j1, dr_l2_j1,dr_l1_j2,dr_l2_j2
      real * 8 :: m_j1_j2, m_l1_j1, m_l1_j2, m_l2_j1, m_l2_j2
      real * 8 :: ptj1, ptj2, ptb1, ptb2
      real * 8 :: pt_j1_j2, pt_l1_l2_ptmiss, pt_l1_l2_ptmiss_j1
      integer jets(maxjets),bjets(maxjets),bhadrons(maxtracks),abhadrons(maxtracks)
      integer allbjets(maxjets)
      real * 8 prodvec2,powheginput
      integer sonofhep,sonofid,binhadron,hardest
      external sonofhep,sonofid,binhadron,hardest
      logical  isbhadron,comesfrom,comesfrom2
      external isbhadron,comesfrom,comesfrom2
      logical fill
      integer in_jet
      external in_jet
      real * 8, save ::  rparam,ptmisscut,ptjcut,ptlcut,etajcut,etalcut
      real * 8 :: ptjthresh
      integer ngenerations,inotfound,iprodrad
      common/cngenerations/ngenerations
      character * 2 digit(20)
      data digit/'01','02','03','04','05','06','07','08','09','10',
     1           '11','12','13','14','15','16','17','18','19','20'/
      integer id1,id2
      logical, save :: ini=.true.,flghvq=.false.

      real * 8, save ::  nlowhich
      logical, save :: nores, noresNLO, noresLHE
      real * 8, save :: tmass

      integer,parameter :: maxleplen = 100
      integer i_lwps(maxleplen),i_lwplen,i_lwms(maxleplen),i_lwmlen,
     1   i_nuwps(maxleplen),i_nuwplen,i_nuwms(maxleplen),i_nuwmlen


      if(ini) then
         rparam = 0.5d0 ! ok
         ptmisscut = 20d0 ! ok
         ptjcut = 30 ! ok
         ptlcut = 20 ! ok
         etajcut = 2.5d0 ! ok
         etalcut = 2.5d0 ! ok

         flghvq = powheginput("#qmass") > 0
         tmass = powheginput("#qmass")
         ini = .false.

         nlowhich = powheginput("#nlowhich")
         nores = powheginput("#nores").eq.1
         noresNLO = nores.and.whcprg.eq.'NLO'
         noresLHE = nores.and..not.noresNLO
      endif

c Make sure LH tops are within +- 15 GeV from the nominal top mass
      if(flghvq) then
         if(abs(pup(5,3)-tmass) > 15 .or. abs(pup(5,4)-tmass) > 15)
     1        then
            return
         endif
      endif

      call multi_plot_setup(dsig0,dsig,dsigdim)

      i_wp = 0
      i_wm = 0
      i_lwp = 0
      i_lwm = 0
      i_nuwp = 0
      i_nuwm = 0
      if (noresLHE) then
        i_first_nuwp = 0
        i_first_lwp = 0
        i_first_nuwm = 0
        i_first_lwm = 0
        i_nuwplen = 0
        i_lwplen = 0
        i_nuwmlen = 0
        i_lwmlen = 0
      endif


! get particles from event record
       if (.not.nores) then
! W's from MC truth
         do jhep=1,nhep
            id=abs(idhep(jhep))
            if(idhep(jhep).eq.24) i_wp = jhep
            if(idhep(jhep).eq.-24) i_wm = jhep
         enddo

         if(i_wp == 0 .or. i_wm == 0) then
            write(*,*) ' no w: i_wp, i_wm ', i_wp,i_wm
            write(*,*) ' discarding event'
            return
         endif
      endif   

      IsForClustering = .false.
      do jhep=1,nhep
         if(noresLHE) then
            if(i_first_nuwp == 0.and.idhep(jhep) == 12) then
               i_first_nuwp = jhep
            elseif(i_first_lwp == 0.and.idhep(jhep) == -11) then
               i_first_lwp = jhep
            elseif(i_first_nuwm == 0.and.idhep(jhep) == -14) then
               i_first_nuwm = jhep
            elseif(i_first_lwm == 0.and.idhep(jhep) == 13) then
               i_first_lwm = jhep
            endif
         endif
!     for jets, using only final state particles excluding leptons
         if(isthep(jhep) == 1) then
            if(abs(idhep(jhep)).lt.11.or.abs(idhep(jhep)).gt.16) then
               IsForClustering(jhep) = .true.
            else
! these are leptons; see if they come from the W's or from production in
!   the case of noresNLO (i_wp=i_wm=0)
               if(noresNLO) then
                  if(jmohep(1,jhep) == 0) then
                     if(idhep(jhep) == 12) then
                        i_nuwp = jhep
                     elseif(idhep(jhep) == -11) then
                        i_lwp = jhep
                     elseif(idhep(jhep) == -14) then
                        i_nuwm = jhep
                     elseif(idhep(jhep) == 13) then
                        i_lwm = jhep
                     endif
                  endif
!   or they come from first leptons in the LHE record 
               else if (noresLHE) then
                  if(idhep(jhep) == 12) then
                     i_nuwplen = i_nuwplen + 1
                     i_nuwps(i_nuwplen) = jhep
                     if (comesfrom2(i_first_nuwp,jhep)) then
                       i_nuwp = jhep
                     endif
                  elseif(idhep(jhep) == -11) then
                     i_lwplen = i_lwplen + 1
                     i_lwps(i_lwplen) = jhep
                     if(comesfrom2(i_first_lwp,jhep)) then
                        i_lwp = jhep
                     endif
                  elseif(idhep(jhep) == -14) then
                     i_nuwmlen = i_nuwmlen + 1
                     i_nuwms(i_nuwmlen) = jhep
                     if(comesfrom2(i_first_nuwm,jhep)) then
                        i_nuwm = jhep
                     endif
                  elseif(idhep(jhep) == 13) then
                     i_lwmlen = i_lwmlen + 1
                     i_lwms(i_lwmlen) = jhep
                     if(comesfrom2(i_first_lwm,jhep)) then
                        i_lwm = jhep
                     endif
                  endif
               else
                  if(comesfrom(i_wp,jhep)) then
                     if(mod(idhep(jhep),2) == 0) then
                        i_nuwp = jhep
                     else
                        i_lwp = jhep
                     endif
                  elseif(comesfrom(i_wm,jhep)) then
                     if(mod(idhep(jhep),2) == 0) then
                        i_nuwm = jhep
                     else
                        i_lwm = jhep
                     endif
                  endif
               endif
            endif
         endif
      enddo

      if (noresLHE) then
         if(i_nuwp .ne. hardest(i_nuwplen,i_nuwps)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'nuwp son if the first nuwp in the event record:', i_nuwp
           i_nuwp = hardest(i_nuwplen,i_nuwps)
           write(*,*) 'haradest nuwp:', i_nuwp
           write(*,*) 'hardest used per default'
         endif
         if(i_lwp .ne. hardest(i_lwplen,i_lwps)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'lwp son if the first lwp in the event record:', i_lwp
           i_lwp = hardest(i_lwplen,i_lwps)
           write(*,*) 'haradest lwp:', i_lwp
           write(*,*) 'hardest used per default'
         endif
         if(i_nuwm .ne. hardest(i_nuwmlen,i_nuwms)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'nuwm son if the first nuwm in the event record:', i_nuwm
           i_nuwm = hardest(i_nuwmlen,i_nuwms)
           write(*,*) 'haradest nuwm:', i_nuwm
           write(*,*) 'hardest used per default'
         endif
         if(i_lwm .ne. hardest(i_lwmlen,i_lwms)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'lwm son if the first lwm in the event record:', i_lwm
           i_lwm = hardest(i_lwmlen,i_lwms)
           write(*,*) 'haradest lwm:', i_lwm
           write(*,*) 'hardest used per default'
         endif
      endif

      if (nores) then
         if(i_nuwp == 0 .or. i_lwp == 0 .or. i_nuwm == 0 .or. i_lwm == 0) then
            write(*,*) ' no leptons: i_nuwp,i_lwp,i_nuwm,i_lwm ',
     1           i_nuwp,i_lwp,i_nuwm,i_lwm
            if (noresLHE) write(*,*) 'i_first_nuwp,i_first_lwp,
     1           i_first_nuwm,i_first_lwm ',
     1           i_first_nuwp,i_first_lwp,i_first_nuwm,i_first_lwm

            write(*,*) ' discarding event'
            return
         endif
      endif

! check for leptons
      if(i_lwp.gt.0) then
         p_lwp=phep(1:4,i_lwp)
      else
         write(*,*) ' No lwp, discard event'
         return
      endif
      if(i_lwm.gt.0) then
         p_lwm=phep(1:4,i_lwm)
      else
         write(*,*) ' No lwm, discard event'
         return
      endif
      if(i_nuwp.gt.0) then
         p_nuwp=phep(1:4,i_nuwp)
      else
         write(*,*) ' No nuwp, discard event'
         return
      endif
      if(i_nuwm.gt.0) then
         p_nuwm=phep(1:4,i_nuwm)
      else
         write(*,*) ' No nuwm, discard event'
         return
      endif

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      call filld('total_no_cut',0.5d0,dsig)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c Lepton cuts & observables
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      call getyetaptmass(p_lwp,y,eta,pt,mass)
      ptlwp = pt
      if(abs(eta).gt.etalcut.or.pt.lt.ptlcut) return            ! cut

      call getyetaptmass(p_lwm,y,eta,pt,mass)
      ptlwm = pt
      if(abs(eta).gt.etalcut.or.pt.lt.ptlcut) return            ! cut

      if (ptlwp > ptlwm) then
        p_l1=p_lwp
        p_l2=p_lwm
      else
        p_l1=p_lwm
        p_l2=p_lwp
      end if

      ptl1 =  sqrt(p_l1(1)**2+p_l1(2)**2)
      ptl2 =  sqrt(p_l2(1)**2+p_l2(2)**2)

      ptmiss = sqrt((p_nuwp(1)+p_nuwm(1))**2+(p_nuwp(2)+p_nuwm(2))**2)
      if(ptmiss.lt.ptmisscut) return                            ! cut

      ptmp = p_l1 + p_l2 + p_nuwp + p_nuwm
      pt_l1_l2_ptmiss = sqrt(ptmp(1)**2+ptmp(2)**2)

      if (.not.nores) then
         p_wp=phep(1:4,i_wp)
         p_wm=phep(1:4,i_wm)
      else
         p_wp=phep(1:4,i_nuwp)+phep(1:4,i_lwp)
         p_wm=phep(1:4,i_nuwm)+phep(1:4,i_lwm)
      endif        


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cc Jet Clustering
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      mjets = maxjets
      call buildjets(mjets,rparam,j_kt,j_eta,j_rap,j_phi,j_p,jetvec,
     1     isForClustering)

! tag b hadrons
      nbhadrons = 0
      nabhadrons = 0
      do j=1,nhep
         if(IsForClustering(j).and.isbhadron(idhep(j))) then
            if(binhadron(idhep(j)).eq.5) then
              nbhadrons = nbhadrons+1 
              bhadrons(nbhadrons) = j
            elseif(binhadron(idhep(j)).eq.-5) then
              nabhadrons = nabhadrons+1 
              abhadrons(nabhadrons) = j
            endif
         endif
      enddo


c tag b-jets; do not descriminate b and bbar jets
      mbjets = 0
      allbjets=0
      loopjets: do j=1,mjets
        do i=1,nbhadrons
          if (j == jetvec(bhadrons(i))) then
            mbjets = mbjets+1
            allbjets(mbjets) = j
            cycle loopjets
          end if
        end do
        do i=1,nabhadrons
          if (j == jetvec(abhadrons(i))) then
            mbjets = mbjets+1
            allbjets(mbjets) = j
            cycle loopjets    
          end if
        end do
      end do loopjets


c jet cuts
      njets = 0
      jets=0
      do j=1,mjets
        if (j_kt(j).lt.ptjcut.or.abs(j_eta(j)).gt.etajcut) cycle
        njets = njets+1
        jets(njets) = j
      end do

      nbjets = 0
      bjets=0
      do j=1,mbjets
        if (j_kt(allbjets(j)).lt.ptjcut.or.abs(j_eta(allbjets(j))).gt.etajcut) cycle
        nbjets = nbjets+1
        bjets(nbjets) = j
      end do


c     fill jet-bin rate distributions
      call filld('total_0j_incl',0.5d0,dsig)

      if (njets==0) call filld('total_0j_excl',0.5d0,dsig)
      if (njets==1) call filld('total_1j_excl',0.5d0,dsig)
      if (njets>1)  call filld('total_2j_incl',0.5d0,dsig)

      if (nbjets==0)  call filld('total_0b_excl',0.5d0,dsig)
      if (nbjets==1)  call filld('total_1b_excl',0.5d0,dsig)
      if (nbjets>1)   call filld('total_2b_incl',0.5d0,dsig)

      if (nbjets==0.and.njets==1) call filld('total_1j0b_excl',0.5d0,dsig)
      if (nbjets==0.and.njets==2) call filld('total_2j0b_excl',0.5d0,dsig)
      if (nbjets==1.and.njets==1) call filld('total_1j1b_excl',0.5d0,dsig)
      if (nbjets==1.and.njets==2) call filld('total_2j1b_excl',0.5d0,dsig)
      if (nbjets==2.and.njets==2) call filld('total_2j2b_excl',0.5d0,dsig)
      if (nbjets>1.and.njets>1)   call filld('total_2j2b_incl',0.5d0,dsig)


c fill jet tresholds distributions
      do n=1,40
        ptjthresh = n*10
        njetsx = 0
        nbjetsx = 0
        do j=1,mjets
          if (j_kt(j).lt.ptjthresh.or.abs(j_eta(j)).gt.etajcut) cycle
          njetsx = njetsx+1
        end do
        do j=1,mbjets
          if (j_kt(allbjets(j)).lt.ptjthresh.or.abs(j_eta(allbjets(j))).gt.etajcut) cycle
          nbjetsx = nbjetsx+1
        end do
        if (njetsx  == 0) call filld('0j_excl_pt_thresh',ptjthresh,dsig)
        if (njetsx  == 1) call filld('1j_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx == 0) call filld('0b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx == 1) call filld('1b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx==0.and.njetsx==1) call filld('1j0b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx==0.and.njetsx==2) call filld('2j0b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx==1.and.njetsx==1) call filld('1j1b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx==1.and.njetsx==2) call filld('2j1b_excl_pt_thresh',ptjthresh,dsig)
        if (nbjetsx==2.and.njetsx==2) call filld('2j2b_excl_pt_thresh',ptjthresh,dsig)
      end do

c calculate observables
      ht = 0
      do j=1,njets
         ht = ht + j_kt(jets(j))
      enddo
      httot = ht + ptlwp + ptlwm + ptmiss

      call getdydetadphidr(p_lwp,p_lwm,dy,deta,delphi_e_mu,dr)

      ptmp = p_lwp+p_lwm
      m_e_mu = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)


      if (njets>0) then
        p_j1=j_p(:,jets(1))
        ptj1=sqrt(p_j1(1)**2+p_j1(2)**2)

        ptmp = p_l1+p_j1
        m_l1_j1  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_l2+p_j1
        m_l2_j1  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_l1 + p_l2 + p_nuwp + p_nuwm + p_j1
        pt_l1_l2_ptmiss_j1 = sqrt(ptmp(1)**2+ptmp(2)**2)

        call getdydetadphidr(p_l1,p_j1,dy,deta,delphi,dr_l1_j1)
        call getdydetadphidr(p_l2,p_j1,dy,deta,delphi,dr_l2_j1)
      end if

      if (njets>1) then
        p_j2=j_p(:,jets(2))
        ptj2=sqrt(p_j1(2)**2+p_j1(2)**2)

        ptmp = p_l1+p_j2
        m_l1_j2  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_l2+p_j2
        m_l2_j2  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_j1+p_j2
        m_j1_j2  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)
        pt_j1_j2 = sqrt(ptmp(1)**2+ptmp(2)**2)

        call getdydetadphidr(p_l1,p_j2,dy,deta,delphi,dr_l1_j2)
        call getdydetadphidr(p_l2,p_j2,dy,deta,delphi,dr_l2_j2)
      end if


      if (nbjets>0) then
        p_b1=j_p(:,bjets(1))
        ptb1=sqrt(p_b1(2)**2+p_b1(2)**2)

        ptmp = p_wp+p_b1
        pt_wp_b1 = sqrt(ptmp(1)**2+ptmp(2)**2)
        m_wp_b1  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_wm+p_b1
        pt_wm_b1 = sqrt(ptmp(1)**2+ptmp(2)**2)
        m_wm_b1  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_lwp+p_b1
        pt_ep_b1 =  sqrt(ptmp(1)**2+ptmp(2)**2)
        m_ep_b1  =  sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_lwm+p_b1
        pt_em_b1 =  sqrt(ptmp(1)**2+ptmp(2)**2)
        m_em_b1  =  sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)
      end if


      if (nbjets>1) then
        p_b2=j_p(:,bjets(2))
        ptb2=sqrt(p_b1(2)**2+p_b1(2)**2)

        call getdydetadphidr(p_b1,p_b2,dy,deta,delphi_b_b,dr)

        ptmp = p_b1+p_b2
        m_b_b =  sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)
        pt_b_b = sqrt((p_b1(1)+p_b2(1))**2+(p_b1(2)+p_b2(2))**2)

        ptmp = p_wp+p_b2
        pt_wp_b2 = sqrt(ptmp(1)**2+ptmp(2)**2)
        m_wp_b2  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_wm+p_b2
        pt_wm_b2 = sqrt(ptmp(1)**2+ptmp(2)**2)
        m_wm_b2  = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_lwp+p_b2
        pt_ep_b2 =  sqrt(ptmp(1)**2+ptmp(2)**2)
        m_ep_b2 = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)

        ptmp = p_lwm+p_b2
        pt_em_b2 =  sqrt(ptmp(1)**2+ptmp(2)**2)
        m_em_b2 = sqrt(ptmp(4)**2-ptmp(1)**2-ptmp(2)**2-ptmp(3)**2)
      end if

c fill kinematic distributions

      do j=1,12
        fill = .false.
        select case(j)
          case(1)
            distprefix='0j_incl'
            fill = .true.
          case(2)
            distprefix='0j_excl'
            if (njets==0) fill = .true.
          case(3)
            distprefix='0b_excl'
            if (nbjets==0) fill = .true.
          case(4)
            distprefix='1j_excl'
            if (njets==1) fill = .true.
          case(5)
            distprefix='1b_excl'
            if (nbjets==1) fill = .true.
          case(6)
            distprefix='1j0b_excl'
            if (nbjets==0.and.njets==1) fill = .true.
          case(7)
            distprefix='2j0b_excl'
            if (nbjets==0.and.njets==2) fill = .true.
          case(8)
            distprefix='1j1b_excl'
            if (nbjets==1.and.njets==1) fill = .true.
          case(9)
            distprefix='2j1b_excl'
            if (nbjets==1.and.njets==2) fill = .true.
          case(10)
            distprefix='2j2b_excl'
            if (nbjets==2.and.njets==2) fill = .true.
          case(11)
            distprefix='2j_incl'
            if (njets>1) fill = .true.
          case(12)
            distprefix='2b_incl'
            if (nbjets>1) fill = .true.
        end select
        if (fill) then
          call filld(trim(distprefix)//'_pt_e+'      ,ptlwp, dsig)
          call filld(trim(distprefix)//'_pt_l1'      ,ptl1, dsig)
          call filld(trim(distprefix)//'_pt_l2'      ,ptl2, dsig)
          call filld(trim(distprefix)//'_pt_miss'    ,ptmiss ,dsig)
          call filld(trim(distprefix)//'_phi_e+_mu-' ,delphi_e_mu,dsig)
          call filld(trim(distprefix)//'_m_e+_mu-'   ,m_e_mu, dsig)
        end if
      end do


      ! 1jet exclusive
      if (njets==1) then
        call filld('1j_excl_Ht' ,ht,dsig)
        call filld('1j_excl_Httot' ,httot,dsig)
        call filld('1j_excl_pt_j' ,ptj1,dsig)
      end if

      ! 1b exclusive
      if (nbjets==1) then
        call filld('1b_excl_Ht' ,ht,dsig)
        call filld('1b_excl_Httot' ,httot,dsig)
        call filld('1b_excl_pt_b' ,ptb1,dsig)
      end if

      !1j1b
      if (nbjets==1.and.njets==1) then
        call filld('1j1b_excl_pt_l1_l2_ptmiss', pt_l1_l2_ptmiss, dsig)
        call filld('1j1b_excl_pt_l1_l2_ptmiss_j1', pt_l1_l2_ptmiss_j1, dsig)
        call filld('1j1b_excl_dr_l1_j1', dr_l1_j1, dsig)
        call filld('1j1b_excl_dr_l2_j1', dr_l2_j1, dsig)
        call filld('1j1b_excl_m_l1_j1', m_l1_j1, dsig)
        call filld('1j1b_excl_m_l2_j1', m_l2_j1, dsig)
      end if

      !2j1b
      if (nbjets==1.and.njets==2) then
        call filld('2j1b_excl_pt_l1_l2_ptmiss', pt_l1_l2_ptmiss, dsig)
        call filld('2j1b_excl_pt_l1_l2_ptmiss_j1', pt_l1_l2_ptmiss_j1, dsig)
        call filld('2j1b_excl_dr_l1_j1', dr_l1_j1, dsig)
        call filld('2j1b_excl_dr_l2_j1', dr_l2_j1, dsig)
        call filld('2j1b_excl_dr_l1_j2', dr_l1_j2, dsig)
        call filld('2j1b_excl_dr_l2_j2', dr_l2_j2, dsig)
        call filld('2j1b_excl_m_l1_j1', m_l1_j1, dsig)
        call filld('2j1b_excl_m_l2_j1', m_l2_j1, dsig)
        call filld('2j1b_excl_m_l1_j2', m_l1_j2, dsig)
        call filld('2j1b_excl_m_l2_j2', m_l2_j2, dsig)
        call filld('2j1b_excl_m_j1_j2', m_j1_j2, dsig)
        call filld('2j1b_excl_pt_j1_j2', pt_j1_j2, dsig)
        call filld('2j1b_excl_pt_j1', ptj1, dsig)
        call filld('2j1b_excl_pt_j2', ptj2, dsig)
      end if

      !2b2j
      if (nbjets==2.and.njets==2) then
        call filld('2j2b_excl_pt_l1_l2_ptmiss', pt_l1_l2_ptmiss, dsig)
        call filld('2j2b_excl_pt_l1_l2_ptmiss_j1', pt_l1_l2_ptmiss_j1, dsig)
        call filld('2j2b_excl_dr_l1_j1', dr_l1_j1, dsig)
        call filld('2j2b_excl_dr_l2_j1', dr_l2_j1, dsig)
        call filld('2j2b_excl_dr_l1_j2', dr_l1_j2, dsig)
        call filld('2j2b_excl_dr_l2_j2', dr_l2_j2, dsig)
        call filld('2j2b_excl_m_l1_j1', m_l1_j1, dsig)
        call filld('2j2b_excl_m_l2_j1', m_l2_j1, dsig)
        call filld('2j2b_excl_m_l1_j2', m_l1_j2, dsig)
        call filld('2j2b_excl_m_l2_j2', m_l2_j2, dsig)
        call filld('2j2b_excl_m_j1_j2', m_j1_j2, dsig)
        call filld('2j2b_excl_pt_j1_j2', pt_j1_j2, dsig)
        call filld('2j2b_excl_pt_j1', ptj1, dsig)
        call filld('2j2b_excl_pt_j2', ptj2, dsig)
      end if


      ! 2j inclusive
      if (njets > 1) then
        call filld('2j_incl_Ht' ,ht,dsig)
        call filld('2j_incl_Httot' ,httot,dsig)
        call filld('2j_incl_pt_j1' ,ptj1,dsig)
        call filld('2j_incl_pt_j2' ,ptj2,dsig)
      end if

      ! 2b inclusive
      if (nbjets > 1) then
        call filld('2b_incl_Ht' ,ht,dsig)
        call filld('2b_incl_Httot' ,httot,dsig)
        call filld('2b_incl_pt_b1' , ptb1,dsig)
        call filld('2b_incl_pt_b2' , ptb2,dsig)
        call filld('2b_incl_m_bb' ,m_b_b,dsig)
        call filld('2b_incl_pt_bb' ,pt_b_b,dsig)
        call filld('2b_incl_phi_bb' ,delphi_b_b,dsig)
        call filld('2b_incl_m_wp_b1',m_wp_b1,dsig)
        call filld('2b_incl_m_wm_b1',m_wm_b1,dsig)
        call filld('2b_incl_m_wp_b2',m_wp_b2,dsig)
        call filld('2b_incl_m_wm_b2',m_wm_b2,dsig)
        call filld('2b_incl_m_ep_b1',m_ep_b1,dsig)
        call filld('2b_incl_m_em_b1',m_em_b1,dsig)
        call filld('2b_incl_m_ep_b2',m_ep_b2,dsig)
        call filld('2b_incl_m_em_b2',m_em_b2,dsig)
        call filld('2b_incl_pt_wp_b1',pt_wp_b1,dsig)
        call filld('2b_incl_pt_wm_b1',pt_wm_b1,dsig)
        call filld('2b_incl_pt_wp_b2',pt_wp_b2,dsig)
        call filld('2b_incl_pt_wm_b2',pt_wm_b2,dsig)
        call filld('2b_incl_pt_ep_b1',pt_ep_b1,dsig)
        call filld('2b_incl_pt_em_b1',pt_em_b1,dsig)
        call filld('2b_incl_pt_ep_b2',pt_ep_b2,dsig)
        call filld('2b_incl_pt_em_b2',pt_em_b2,dsig)
      end if

      end subroutine


      function in_jet(i_part,jetvec)
      implicit none
      include 'hepevt.h'
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=20)
      integer in_jet,jetvec(maxtracks),i_part
      integer j
      integer sonofhep
      external sonofhep
      do j=1,nhep
         if(jetvec(j).ne.0) then
            if(sonofhep(i_part,j).lt.1d5) then
               in_jet = jetvec(j)
               return
            endif
         endif
      enddo
      in_jet = 0
      end


      function prodvec2(vec1,vec2)
      implicit none
      real * 8 prodvec2,vec1(4),vec2(4)
      prodvec2=vec1(4)*vec2(4)-vec1(1)*vec2(1)
     1 -vec1(2)*vec2(2)-vec1(3)*vec2(3)
      end


      subroutine getyetaptmass(p,y,eta,pt,mass)
      implicit none
      real * 8 p(4),y,eta,pt,mass,pv
      real *8 tiny
      parameter (tiny=1.d-5)
      y=0.5d0*log((p(4)+p(3))/(p(4)-p(3)))
      pt=sqrt(p(1)**2+p(2)**2)
      pv=sqrt(pt**2+p(3)**2)
      if(pt.lt.tiny)then
         eta=sign(1.d0,p(3))*1.d8
      else
         eta=0.5d0*log((pv+p(3))/(pv-p(3)))
      endif
      mass=sqrt(abs(p(4)**2-pv**2))
      end

      subroutine yetaptmassplot(p,dsig,prefix)
      implicit none
      real * 8 p(4),dsig
      character *(*) prefix
      real * 8 y,eta,pt,m
      call getyetaptmass(p,y,eta,pt,m)
      call filld(prefix//'_y',y,dsig)
      call filld(prefix//'_eta',eta,dsig)
      call filld(prefix//'_pt',pt,dsig)
      call filld(prefix//'_m',m,dsig)
      end

      function islept(j)
      implicit none
      logical islept
      integer j
      if(abs(j).ge.11.and.abs(j).le.16) then
         islept = .true.
      else
         islept = .false.
      endif
      end

      function phepDot(p_A,p_B)
      implicit none
      real * 8  phepDot
      real * 8  p_A(4),p_B(4)
      phepDot=p_A(4)*p_B(4)-p_A(1)*p_B(1)
     1       -p_A(2)*p_B(2)-p_A(3)*p_B(3)
      end

c     calculate the separation in the lego plot between the two momenta
c     p1 and p2 in azi and pseudorapidity
      function rsepn_p(p1,p2)
      implicit none
      real * 8 pi,pi2
      parameter(pi = 3.141592653589793D0, pi2 = 9.869604401089358D0)
      real * 8 rsepn_p,p1(0:3),p2(0:3)
      real * 8 eta1,phi1,eta2,phi2
      real * 8 delphi
      real * 8 pseudorapidity,azi
      external pseudorapidity,azi

      phi1 = azi(p1)   
      phi2 = azi(p2)
      eta1 = pseudorapidity(p1)
      eta2 = pseudorapidity(p2)

      delphi = abs(phi1-phi2)
      if (delphi.gt.pi) then
         delphi = 2*pi-delphi
      endif
      if (delphi.lt.0 .or. delphi.gt.pi) then
         print*,' problem in rsepn. delphi = ',delphi
      endif
      rsepn_p = sqrt( (eta1-eta2)**2 + delphi**2 )
      end

      function azi(p)
      implicit none
      real * 8 pi,pi2
      parameter(pi = 3.141592653589793D0, pi2 = 9.869604401089358D0)
      real * 8 azi,p(0:3)
      azi = atan(p(2)/p(1))
      if (p(1).lt.0d0) then
         if (azi.gt.0d0) then               
            azi = azi - pi
         else
            azi = azi + pi
         endif
      endif    
      end

      function pseudorapidity(p)
      implicit none
      real * 8 p(0:3),pseudorapidity
      real * 8 mod, costh
      mod = sqrt(p(1)**2+p(2)**2+p(3)**2)
      costh = p(3)/mod
      pseudorapidity=0.5*log((1+costh)/(1-costh))
      end


      subroutine buildjets(mjets,r,kt,eta,rap,phi,pjet,jetvechep,
     1                                               isForClustering)
c     arrays to reconstruct jets
      implicit  none
      include  'hepevt.h'
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer   mjets,jetvechep(maxtracks)
      real * 8  kt(maxjets),eta(maxjets),rap(maxjets),
     1     phi(maxjets),pjet(4,maxjets)
      logical   isForClustering(maxtracks)
      real * 8  ptrack(4,maxtracks),pj(4,maxjets)
      integer   jetvec(maxtracks),itrackhep(maxtracks)
      integer   ntracks,njets
      integer   j,k,mu
      real * 8  r,palg,ptmin,pp,tmp
      logical sonofid
      external sonofid
C - Initialize arrays and counters for output jets
      kt = 0
      eta = 0
      rap = 0
      phi = 0
      pjet = 0
      jetvechep = 0

      ptrack = 0
      ntracks=0
      pj = 0
      jetvec = 0

C - Extract final state particles to feed to jet finder
      do j=1,nhep
         if(.not.isForClustering(j)) cycle
         if(ntracks.eq.maxtracks) then
            write(*,*) 'analyze: need to increase maxtracks!'
            write(*,*) 'ntracks: ',ntracks
            call exit(-1)
         endif
         ntracks=ntracks+1
         ptrack(:,ntracks) = phep(1:4,j)
         itrackhep(ntracks)=j
      enddo
      if (ntracks.eq.0) then
         mjets=0
         return
      endif
C --------------- C
C - Run FastJet - C
C --------------- C
C - R = r     radius parameter
C - f = 0.75  overlapping fraction
      palg  = -1 ! -1 := anti-kt
      ptmin = 0d0
      njets=mjets  
      call fastjetppgenkt(ptrack,ntracks,r,palg,ptmin,pjet,njets,jetvec)
      mjets=min(mjets,njets)
      if(njets.eq.0) return
c check consistency
      do k=1,ntracks
         if(jetvec(k).gt.0) then
            do mu=1,4
               pj(mu,jetvec(k))=pj(mu,jetvec(k))+ptrack(mu,k)
            enddo
         endif
      enddo
      tmp=0
      do j=1,mjets
         do mu=1,4
            tmp=tmp+abs(pj(mu,j)-pjet(mu,j))
         enddo
      enddo
      if(tmp.gt.1d-4) then
         write(*,*) ' bug!'
      endif
C --------------------------------------------------------------------- C
C - Computing arrays of useful kinematics quantities for hardest jets - C
C --------------------------------------------------------------------- C
      do j=1,mjets
         kt(j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
         pp = sqrt(kt(j)**2+pjet(3,j)**2)
         eta(j)=0.5d0*log((pp+pjet(3,j))/(pp-pjet(3,j)))
         rap(j)=0.5d0*log((pjet(4,j)+pjet(3,j))/(pjet(4,j)-pjet(3,j)))
         phi(j)=atan2(pjet(2,j),pjet(1,j))
      enddo
      jetvechep = 0
      do j=1,ntracks
         jetvechep(itrackhep(j))=jetvec(j)
      enddo
      end

      function sonofid(m,k)
c if k'th particle in hep common block
c is son of a particle with idhep=m returns
c how far back (along the jmohep sequence) the ancestor is.
c It looks back for no more than ngenerations levels.
c In case of failure it returns 1 000 000.
      implicit none
      integer, intent(in):: m,k
      integer sonofid,sonofid0
c      print*, 'sonofid ',m,k
      sonofid = sonofid0(m,k,0)
      end

      function sonofidn(m,k)
c if k'th particle in hep common block
c is son of a particle with idhep=m returns
c how far back (along the jmohep sequence) the ancestor is.
c It looks back for no more than ngenerations levels.
c In case of failure it returns 1 000 000.
      implicit none
      integer, intent(in):: m,k
      integer sonofidn,sonofidn0
c      print*, 'sonofid ',m,k
      sonofidn = sonofidn0(m,k,0)
      end


      recursive function sonofid0(m,k,level) result(result)
      implicit none
      integer, intent(in):: m,k,level
      integer result
      include  'hepevt.h'
      integer k1,k2,r1,r2
      integer ngenerations
      common/cngenerations/ngenerations
c      print*, 'sonofid0 ',k
      if(level.gt.ngenerations.or.k.eq.0) then
         result = 1000000
         return
      endif
      if(idhep(k).eq.m) then
         result = level
         return
      endif
      k1 = jmohep(1,k)
      k2 = jmohep(2,k)
      r1 = sonofid0(m,k1,level+1)
      r2 = sonofid0(m,k2,level+1)
      result = min(r1,r2)
      end


      recursive function sonofidn0(m,k,level) result(result)
      implicit none
      integer, intent(in):: m,k,level
      integer result
      include  'hepevt.h'
      integer k1,k2,r1,r2
      integer ngenerations
      common/cngenerations/ngenerations
c      print*, 'sonofid0 ',k
      write(*,*) k
      if(level.gt.ngenerations.or.k.eq.0) then
         result = 1000000
         write(*,*) ' return ',result
         return
      endif
      if(idhep(k).eq.m) then
         result = level
         write(*,*) ' return ',level
         return
      endif
      k1 = jmohep(1,k)
      k2 = jmohep(2,k)
      r1 = sonofidn0(m,k1,level+1)
      r2 = sonofidn0(m,k2,level+1)
      result = min(r1,r2)
      end

      function sonofhep(m,k)
      implicit none
      integer, intent(in):: m,k
      integer sonofhep,sonofhep0
      sonofhep = sonofhep0(m,k,0)
      end

      recursive function sonofhep0(m,k,level) result(result)
      implicit none
      integer, intent(in):: m,k,level
      integer result
      include  'hepevt.h'
      integer k1,k2,r1,r2
      integer ngenerations
      common/cngenerations/ngenerations
      if(level.gt.ngenerations) then
         result = 1000000
         return
      endif
      if(k.eq.m) then
         result = level
         return
      endif
      k1 = jmohep(1,k)
      k2 = jmohep(2,k)
      r1 = sonofhep0(m,k1,level+1)
      r2 = sonofhep0(m,k2,level+1)
      result = min(r1,r2)
      end

      function isbhadron(idhep)
! all b hadrons have a 5 either as third (mesons) or fourth (barions) digit
      implicit none
      logical isbhadron
      integer idhep
      integer idigit
      if(abs(idhep).eq.5.or.idigit(3,idhep).eq.5
     1     .or.idigit(4,idhep).eq.5) then
         isbhadron = .true.
      else
         isbhadron = .false.
      endif
      end

      function binhadron(idhep)
      implicit none
      integer binhadron
      integer idhep
      integer idigit
! all b hadrons have a 5 either a third (mesons) or fourth (barions) digit
      if(abs(idhep).eq.5) then
         binhadron = idhep
      elseif(idigit(4,idhep).eq.5) then
         binhadron = sign(5,idhep)
      elseif(idigit(4,idhep).eq.0.and.idigit(3,idhep).eq.5
! This line is to avoid to count b bbar resonances as having definite flavour
     1        .and.idigit(2,idhep).ne.5 ) then
         binhadron = - sign(5,idhep)
      else
         binhadron = 0
      endif
      end

      function idigit(ipos,inum)
      implicit none
      integer idigit,ipos,inum
      if(ipos.eq.0) then
         write(*,*) ' error: idigit(ipos.inum), ipos<=0!'
         call exit(-1)
      endif
      idigit = int(mod(abs(inum),10**ipos)/10**(ipos-1))
      end

      subroutine getdydetadphidr(p1,p2,dy,deta,dphi,dr)
      implicit none
      include 'pwhg_math.h' 
      real * 8 p1(*),p2(*),dy,deta,dphi,dr
      real * 8 y1,eta1,pt1,mass1,phi1
      real * 8 y2,eta2,pt2,mass2,phi2
      call getyetaptmass(p1,y1,eta1,pt1,mass1)
      call getyetaptmass(p2,y2,eta2,pt2,mass2)
      dy=y1-y2
      deta=eta1-eta2
      phi1=atan2(p1(1),p1(2))
      phi2=atan2(p2(1),p2(2))
      dphi=abs(phi1-phi2)
      dphi=min(dphi,2d0*pi-dphi)
      dr=sqrt(deta**2+dphi**2)
      end

      subroutine deltaplot(p1,p2,dsig,prefix,postfix)
      implicit none
      real * 8 p1(4),p2(4),dsig
      character *(*) prefix,postfix
      real * 8 dy,deta,delphi,dr
      call getdydetadphidr(p1,p2,dy,deta,delphi,dr)
      call filld(prefix//'-dy'//postfix,dy,dsig)
      call filld(prefix//'-deta'//postfix,deta,dsig)
      call filld(prefix//'-delphi'//postfix,delphi,dsig)
      call filld(prefix//'-dr'//postfix,dr,dsig)
      end

      subroutine printhep
      implicit none
      include 'hepevt.h'
      integer j,k
      write(*,101)
      do j=1,nhep
         write(*,100)j,isthep(j),idhep(j),jmohep(1,j),
     1        jmohep(2,j),jdahep(1,j),jdahep(2,j), (phep(k,j),k=1,5)
      enddo
 100  format(i4,    2x, i5,2x,   i5,2x, i4,1x,   i4,2x,   i4,1x,    i4,
     1     2x,5(d10.4,1x))
 101  format('ihep',2x,'ist',4x,'id',5x,'mo1',2x,'mo2',3x,'da1',2x,'da2'
     1     ,3x,'moms')
      end
  
      function comesfrom2(id,k)
      implicit none
      logical comesfrom2
      integer id,k
      include  'hepevt.h'
      integer kcurr, klast
      if(k.eq.id) then
         comesfrom2 = .true.
         return
      endif
      kcurr = k
      do while (.true.)
         klast = kcurr
         kcurr = jmohep(1,kcurr)
         if(kcurr.eq.id) then
            comesfrom2 = .true.
            return
         endif
         if(kcurr.gt.klast.or.kcurr.eq.0) exit
      enddo
      comesfrom2=.false.
      end

      function hardest(nparticles, particles)
      implicit none
      include 'hepevt.h'
      integer hardest
      integer nparticles, particles(*)
      real * 8 maxhardness, pt
      integer j, mptr
      maxhardness = 0d0
      do j=1,nparticles
        mptr = particles(j)
        pt=sqrt(phep(1,mptr)**2+phep(2,mptr)**2)
        if (pt.gt.maxhardness) then
          hardest = mptr
          maxhardness = pt
        endif
      enddo
      end
