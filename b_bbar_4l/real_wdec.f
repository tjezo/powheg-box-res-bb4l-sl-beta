      subroutine setreal(p,rflav,amp2)
c Wrapper subroutine to call the OL real emission matrix
c elements and set the event-by-event couplings constant
      use openloops_powheg, only: openloops_real
      implicit none
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal+1
      real * 8 p(0:3,nlegs)
      integer rflav(nlegs)
      real * 8 amp2,amp2ol
      logical openloopsreal,openloopsvirtual
      common/copenloopsreal/openloopsreal,openloopsvirtual

c      print*,'real old:',rflav
c     gg channel
      if (rflav(1) == rflav(2)) then
c         do nothing
c     qqbar channel
      else if (rflav(1) == -rflav(2)) then
         if (MOD(rflav(1),2) == 0) then
            rflav(1) = SIGN(2,rflav(1))
            rflav(2) = SIGN(2,rflav(2))
         else
            rflav(1) = SIGN(1,rflav(1))
            rflav(2) = SIGN(1,rflav(2))
         endif 
c     gq channel
      else
         amp2 = 0
         return
      endif
c      print*,'real new:',rflav
      call openloops_real(p,rflav,amp2,approx='pole_ww_wrad')
      end

      subroutine regularcolour_lh
      write(*,*) ' regularcolour_lh: there are no regulars in this process'
      write(*,*) ' exiting ...'
      call exit(-1)
      end
