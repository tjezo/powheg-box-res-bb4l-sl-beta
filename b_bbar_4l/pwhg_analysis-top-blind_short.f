c  The next subroutines, open some histograms and prepare them 
c      to receive data 
c  You can substitute these  with your favourite ones
c  init   :  opens the histograms
c  topout :  closes them
c  pwhgfill  :  fills the histograms with data

      subroutine init_hist
      implicit none
      include  'LesHouches.h'
      include 'pwhg_math.h'
      integer j,l
      logical asyanal
      common/casyanal/asyanal
      character * 20 prefix
      integer nbins
      parameter (nbins=11)
      real * 8 pT_tt_bins(nbins+1)
      data pT_tt_bins/  0d0, 10d0, 25d0, 50d0,100d0,
     1     150d0,200d0,250d0,300d0,400d0,600d0, 900d0/          
      real * 8 m_tt_bins(nbins+1)
      data m_tt_bins/ 320d0,360d0,400d0,450d0,500d0,
     1     550d0,600d0,650d0,700d0,800d0,900d0,1000d0/          
      character * 2 digit(20)
      data digit/'01','02','03','04','05','06','07','08','09','10',
     1           '11','12','13','14','15','16','17','18','19','20'/
      integer lenocc
      real * 8 powheginput
      external lenocc,powheginput
      character *5 suffix(3)
      common/csuffix/suffix
      integer icut
      integer ncuts
      common /ncuts/ncuts

      call inihists

      suffix(1)='-cut1'
      suffix(2)='-cut2'
      suffix(3)='-cut3'
      ncuts = 1 ! set to 3 if you want all the cuts

      do icut=1,ncuts
c       total cross section
        call bookupeqbins('xsec'//suffix(icut),1d0,0d0,1d0)

c       b-jet related observables
        call bookupeqbins('jb-profile'//suffix(icut),0.02d0,0d0,2d0)
        call bookupeqbins('jb-m'//suffix(icut),0.5d0,0d0,50d0)
        call bookupeqbins('jb-pt'//suffix(icut),2d0,0d0,500d0)

c       monte carlo truth top related observables (averaged over top, anti-top)
        call bookupeqbins('t-m'//suffix(icut),2d0,0d0,400d0)
        call bookupeqbins('t-pt'//suffix(icut),2d0,0d0,400d0)
        call bookupeqbins('t-m-zoom'//suffix(icut),0.2d0,150d0,200d0)

c       reconstructed top related observables (averaged over top, anti-top)
        call bookupeqbins('wB-m'//suffix(icut),2d0,0d0,400d0)
        call bookupeqbins('wjb-m'//suffix(icut),2d0,0d0,400d0)
        call bookupeqbins('wjb-m-zoom'//suffix(icut),0.2d0,150d0,200d0)

c       reconstructed top-pair related observales
        call bookupeqbins('wwjbjb-pt'//suffix(icut),2d0,0d0,500d0)
        call bookupeqbins('wwjbjb-m'//suffix(icut),2d0,0d0,500d0)

c       lepton-bjet system related observables
        call bookupeqbins('ljb-m'//suffix(icut),2d0,0d0,400d0)
        call bookupeqbins('lB-m'//suffix(icut),2d0,0d0,400d0)

c       B hadron related observables (averaged over b and bbar hadrons)
        call bookupeqbins('B-frag'//suffix(icut),0.01d0,0d0,1d0)
        call bookupeqbins('B-ptdec'//suffix(icut),1d0,0d0,90d0)
        call bookupeqbins('B-frag-MCtruth'//suffix(icut),0.01d0,0d0,1d0)
        call bookupeqbins('B-ptdec-MCtruth'//suffix(icut),1d0,0d0,90d0)
      enddo
      end


      subroutine analysis(dsig0)
      implicit none
      include 'hepevt.h'
      include 'pwhg_math.h' 
      include 'LesHouches.h'
      character * 6 whcprg      
      common/cwhcprg/whcprg
      integer jpref
      character * 20 prefix(18)
      common/ccccprefix/jpref,prefix
      data whcprg/'NLO   '/
      real * 8  dsig0,dsig(10)
      logical asyanal
      common/casyanal/asyanal
      integer   ihep                ! HEPEVT index.
      real * 8 p_top(4),p_atop(4),p_wp(4),p_wm(4),p_lwp(4),p_lwm(4),
     1         p_nuwp(4),p_nuwm(4),p_b(4),p_bb(4),y,eta,pt,mass,delphi,
     2         ptzmf(4),plzmf(4),ppp(4),pbhadfromtop(4),
     3         pbhadtp(4),pbhadtm(4),p_b1(4),p_b2(4),p_bjet1(4),p_bjet2(4)
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer mjets,jetvec(maxtracks)
      integer bhadfromtop,bhadfromatop
      real * 8 ptbhadfromtop,ptbhadfromatop
      logical   isForClustering(maxtracks)
      real * 8 j_kt(maxjets),j_eta(maxjets),j_rap(maxjets),
     1     j_phi(maxjets),j_p(4,maxjets)
      integer j,i1,i2,id,i_bfromtop,i_abfromatop,
     1     i_wp,i_wm,i_lwp,i_lwm,i_nuwp,i_nuwm,i_bjet,i_abjet,jhep,
     1     i_part,njets20,njets30,njets40,i_top_MCtruth,i_atop_MCtruth
      integer i_first_nuwp,i_first_lwp,i_first_nuwm,i_first_lwm
      real * 8 mtop,mtb,mwp,mwm,mb,mbb,p_bmax,e_bmax,xb,
     1     p_bbmax,e_bbmax,xbb,ewp,pwp,ewm,pwm,xw,
     2     dy,deta,dphi,dr,cth1,cth2,ptmiss,ptmiss2(2)
      real * 8 ptop(4),patop(4),p_top_MCtruth(4),p_atop_MCtruth(4)
      integer jcth1
      real * 8 w(4),pb(4),ptb, ptbb
      real * 8 prodvec2,massvec2,powheginput
      integer sonofhep,sonofid,binhadron
      logical isbhadron,comesfrom,comesfrom2
      external sonofhep,sonofid,isbhadron,binhadron
      integer in_jet
      external in_jet
      integer i_part0,i_topjet,i_atopjet,i_partjet
      real * 8 ptop0(4),patop0(4),ppart0(4),ptot0(4)
      real * 8 ptopjet(4),patopjet(4),ppartjet(4),ptotjet(4)
      real * 8 mt_ratio,mtb_ratio,mttb_ratio,
     1     ptt_ratio,pttb_ratio,parton_dm,part_de,top_de,atop_de,
     1     deincrease,rmomtop,rmomatop,rmomjet,masswindow
      integer ngenerations,inotfound,iprodrad
      common/cngenerations/ngenerations
      character * 2 digit(20)
      data digit/'01','02','03','04','05','06','07','08','09','10',
     1           '11','12','13','14','15','16','17','18','19','20'/
      integer id1,id2
      real * 8 dot3,avec3

      integer ntpdec,ntmdec,tpiddec(8),tmiddec(8)
      real * 8 tpdecsc,tmdecsc,tppdec(4,8),tmpdec(4,8)
      common/ctptmdec/tpdecsc,tmdecsc,tppdec,tmpdec,
     1     ntpdec,ntmdec,tpiddec,tmiddec
      real * 8 ptpw,phepdot

      integer nlowhich

      logical nores, noresNLO, noresLHE

      character *5 suffix(3)
      common/csuffix/suffix
      logical cut2,cut3

      logical ini
      data ini/.true./
      save ini
      real *8 cut2_ptb,cut2_mbb,cut3_ptbj,cut3_etabj,cut3_ptl,cut3_etal,cut3_ptmiss,ptbcut
      save cut2_ptb,cut2_mbb,cut3_ptbj,cut3_etabj,cut3_ptl,cut3_etal,
     1     cut3_ptmiss,nlowhich,masswindow,nores

      integer,parameter :: maxleplen = 100;
      integer i_lwps(maxleplen),i_lwplen,i_lwms(maxleplen),i_lwmlen,
     1   i_nuwps(maxleplen),i_nuwplen,i_nuwms(maxleplen),i_nuwmlen
      integer hardest
      logical valid_bjet,valid_abjet
     
      integer ncuts
      common/ncuts/ncuts

      logical topMCtruth
      integer, save :: nofEventsWithoutTop=0
 
      if(ini) then
         cut2_ptb = 30
         cut2_mbb = 30

         cut3_ptbj  = 30
         cut3_etabj = 2.5
         cut3_ptl   = 20
         cut3_etal  = 2.5
         cut3_ptmiss = 20 

         nlowhich = powheginput("#nlowhich")
         masswindow = powheginput("#masswindow")
         nores = powheginput("#nores").eq.1.or.powheginput("#stripres").eq.1
         noresNLO = nores.and.whcprg.eq.'NLO'
         noresLHE = nores.and..not.noresNLO

         ini=.false.
      endif

      call multi_plot_setup(dsig0,dsig,10)

      i_wp = 0
      i_wm = 0
      i_lwp = 0
      i_lwm = 0
      i_nuwp = 0
      i_nuwm = 0
      i_bfromtop = 0
      i_abfromatop = 0
      i_top_MCtruth = 0
      i_atop_MCtruth = 0
      if (noresLHE) then
        i_first_nuwp = 0
        i_first_lwp = 0
        i_first_nuwm = 0
        i_first_lwm = 0
        i_nuwplen = 0
        i_lwplen = 0
        i_nuwmlen = 0
        i_lwmlen = 0
      endif
      topMCtruth = .false. 

      if (.not.nores) then
c W's from MC truth
         do jhep=1,nhep
            id=abs(idhep(jhep))
            if(idhep(jhep).eq.24) i_wp = jhep
            if(idhep(jhep).eq.-24) i_wm = jhep
            if(idhep(jhep).eq.6) i_top_MCtruth = jhep
            if(idhep(jhep).eq.-6) i_atop_MCtruth = jhep            
         enddo
         if (i_top_MCtruth > 0 .and. i_atop_MCtruth > 0) then
            p_top_MCtruth = phep(1:4,i_top_MCtruth)
            p_atop_MCtruth = phep(1:4,i_atop_MCtruth)
            topMCtruth = .true.
         else
            nofEventsWithoutTop = nofEventsWithoutTop + 1
            print*,'Warning, the histograms starting with "t-" and ending with "-MCtruth" '//
     1       'assume the existence of top quark in the event. This event does not contain '//
     2       'a top quark and will be excluded from these histograms.'
            print*,'# of events without top quarks so far: ', nofEventsWithoutTop
         endif

   
         if(i_wp == 0 .or. i_wm == 0) then
            write(*,*) ' no w: i_wp, i_wm ', i_wp,i_wm
            write(*,*) ' discarding event'
            return
         endif
      endif

      IsForClustering = .false.
      do jhep=1,nhep
         if(noresLHE) then
            if(i_first_nuwp == 0.and.idhep(jhep) == 12) then
               i_first_nuwp = jhep
            elseif(i_first_lwp == 0.and.idhep(jhep) == -11) then
               i_first_lwp = jhep
            elseif(i_first_nuwm == 0.and.idhep(jhep) == -14) then
               i_first_nuwm = jhep
            elseif(i_first_lwm == 0.and.idhep(jhep) == 13) then
               i_first_lwm = jhep
            endif
         endif
c     for jets, using only final state particles excluding leptons
         if(isthep(jhep) == 1) then
            if(abs(idhep(jhep)).lt.11.or.abs(idhep(jhep)).gt.16) then
               IsForClustering(jhep) = .true.
            else
c these are leptons; see if they come from the W's or from production in
c   the case of noresNLO (i_wp=i_wm=0)
               if(noresNLO) then
                  if(jmohep(1,jhep) == 0) then
                     if(idhep(jhep) == 12) then
                        i_nuwp = jhep
                     elseif(idhep(jhep) == -11) then
                        i_lwp = jhep
                     elseif(idhep(jhep) == -14) then
                        i_nuwm = jhep
                     elseif(idhep(jhep) == 13) then
                        i_lwm = jhep
                     endif
                  endif
c   or they come from first leptons in the LHE record 
               else if (noresLHE) then
                  if(idhep(jhep) == 12) then
                     i_nuwplen = i_nuwplen + 1
                     i_nuwps(i_nuwplen) = jhep
                     if (comesfrom2(i_first_nuwp,jhep)) then
                       i_nuwp = jhep                     
                     endif
                  elseif(idhep(jhep) == -11) then
                     i_lwplen = i_lwplen + 1
                     i_lwps(i_lwplen) = jhep
                     if(comesfrom2(i_first_lwp,jhep)) then
                        i_lwp = jhep
                     endif                     
                  elseif(idhep(jhep) == -14) then
                     i_nuwmlen = i_nuwmlen + 1
                     i_nuwms(i_nuwmlen) = jhep
                     if(comesfrom2(i_first_nuwm,jhep)) then
                        i_nuwm = jhep
                     endif
                  elseif(idhep(jhep) == 13) then
                     i_lwmlen = i_lwmlen + 1
                     i_lwms(i_lwmlen) = jhep
                     if(comesfrom2(i_first_lwm,jhep)) then
                        i_lwm = jhep
                     endif
                  endif
               else
                  if(comesfrom(i_wp,jhep)) then
                     if(mod(idhep(jhep),2) == 0) then
                        i_nuwp = jhep
                     else
                        i_lwp = jhep
                     endif
                  elseif(comesfrom(i_wm,jhep)) then
                     if(mod(idhep(jhep),2) == 0) then
                        i_nuwm = jhep
                     else
                        i_lwm = jhep
                     endif
                  endif
               endif
            endif
         endif
      enddo

      if (noresLHE) then
         if(i_nuwp .ne. hardest(i_nuwplen,i_nuwps)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'nuwp son if the first nuwp in the event record:', i_nuwp
           i_nuwp = hardest(i_nuwplen,i_nuwps)
           write(*,*) 'haradest nuwp:', i_nuwp
           write(*,*) 'hardest used per default'
         endif
         if(i_lwp .ne. hardest(i_lwplen,i_lwps)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'lwp son if the first lwp in the event record:', i_lwp
           i_lwp = hardest(i_lwplen,i_lwps)
           write(*,*) 'haradest lwp:', i_lwp
           write(*,*) 'hardest used per default'
         endif
         if(i_nuwm .ne. hardest(i_nuwmlen,i_nuwms)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'nuwm son if the first nuwm in the event record:', i_nuwm
           i_nuwm = hardest(i_nuwmlen,i_nuwms)
           write(*,*) 'haradest nuwm:', i_nuwm
           write(*,*) 'hardest used per default'
         endif
         if(i_lwm .ne. hardest(i_lwmlen,i_lwms)) then
           write(*,*) 'the two identification of leptons do not lead to the same result:'
           write(*,*) 'lwm son if the first lwm in the event record:', i_lwm
           i_lwm = hardest(i_lwmlen,i_lwms)
           write(*,*) 'haradest lwm:', i_lwm
           write(*,*) 'hardest used per default'
         endif
      endif

      if (nores) then
         if(i_nuwp == 0 .or. i_lwp == 0 .or. i_nuwm == 0 .or. i_lwm == 0) then
            write(*,*) ' no leptons: i_nuwp,i_lwp,i_nuwm,i_lwm ',
     1           i_nuwp,i_lwp,i_nuwm,i_lwm
            if (noresLHE) write(*,*) '   i_first_nuwp,i_first_lwp,i_first_nuwm,i_first_lwm ',
     1           i_first_nuwp,i_first_lwp,i_first_nuwm,i_first_lwm

            write(*,*) ' discarding event'
            return
         endif
      endif

      mjets = maxjets
      call buildjets(mjets,-1d0,j_kt,j_eta,j_rap,j_phi,j_p,jetvec,
     1     isForClustering)

c 
      bhadfromtop = 0
      bhadfromatop = 0
      ptbhadfromtop = 0
      ptbhadfromatop = 0
      do j=1,nhep
         if(IsForClustering(j).and.isbhadron(idhep(j))) then
            if(binhadron(idhep(j)).eq.5) then
c     Look for hardest (largest pt) hadron with a b quark content.
c     Store in bhadfromtop, ptbhadfromtop
               if(bhadfromtop.ne.0) then
c     write(*,*) ' a top with more than one b son'
                  call getyetaptmass(phep(1:4,j),y,eta,pt,mass)
                  if(pt.gt.ptbhadfromtop) then
                     bhadfromtop = j
                     ptbhadfromtop = pt
                  endif
               else
                  bhadfromtop = j
                  call getyetaptmass(phep(1:4,j),y,eta,pt,mass)
                  ptbhadfromtop = pt
               endif
            elseif(binhadron(idhep(j)).eq.-5) then
c same for bbar
               if(bhadfromatop.ne.0) then
c                  write(*,*) ' a top with more than one b son'
                  call getyetaptmass(phep(1:4,j),y,eta,pt,mass)
                  if(pt.gt.ptbhadfromatop) then
                     bhadfromatop = j
                     ptbhadfromatop = pt
                  endif
               else
                  bhadfromatop = j
                  call getyetaptmass(phep(1:4,j),y,eta,pt,mass)
                  ptbhadfromatop = pt
               endif
            endif
         endif
      enddo

      if(bhadfromtop == 0 .or. bhadfromatop == 0) then
         write(*,*) ' Bhadrons from t and tbar:',bhadfromtop,bhadfromatop
         write(*,*) ' discard event'
         return
      endif
      
      i_bjet  = jetvec(bhadfromtop)
      i_abjet = jetvec(bhadfromatop)

      if((i_bjet <=0 .or. i_bjet > mjets)  .or. i_abjet <= 0 .or. i_abjet > mjets) then
         write(*,*) ' B jets:',i_bjet,i_abjet
         write(*,*) ' njets:',mjets
         write(*,*) ' discard event'
         return
      endif

      if(i_bjet > 0) then
         if( j_kt(i_bjet) > 25 .and. abs(j_eta(i_bjet)) < 4.5 ) then
            valid_bjet = .true.
         else
            valid_bjet = .false.
         endif
      else
         valid_bjet = .false.
      endif
      
      if(i_abjet > 0) then
         if( j_kt(i_abjet) > 25 .and. abs(j_eta(i_abjet)) < 4.5 ) then
            valid_abjet = .true.
         else
            valid_abjet = .false.
         endif
      else
         valid_abjet = .false.
      endif
      
      
      pbhadtp = phep(1:4,bhadfromtop)
      pbhadtm = phep(1:4,bhadfromatop)
      
      if (.not.nores) then
         p_wp=phep(1:4,i_wp)
         p_wm=phep(1:4,i_wm)
      else
         p_wp=phep(1:4,i_nuwp)+phep(1:4,i_lwp)
         p_wm=phep(1:4,i_nuwm)+phep(1:4,i_lwm)
      endif        

      p_top  = p_wp + j_p(:,i_bjet)
      p_atop = p_wm + j_p(:,i_abjet)



      if(i_lwp.gt.0) then
         p_lwp=phep(1:4,i_lwp)
      else
         write(*,*) ' No lwp, discard event'
         return
      endif
      if(i_lwm.gt.0) then
         p_lwm=phep(1:4,i_lwm)
      else
         write(*,*) ' No lwm, discard event'
         return
      endif
      if(i_nuwp.gt.0) then
         p_nuwp=phep(1:4,i_nuwp)
      else
         write(*,*) ' No nuwp, discard event'
         return
      endif
      if(i_nuwm.gt.0) then
         p_nuwm=phep(1:4,i_nuwm)
      else
         write(*,*) ' No nuwm, discard event'
         return
      endif

      p_b  = pbhadtp
      p_bb = pbhadtm

      if(masswindow.gt.0) then
         call getyetaptmass(p_top,y,eta,pt,mass)
         if(mass.lt.172.5-masswindow.or.mass.gt.172.5+masswindow) return
         call getyetaptmass(p_atop,y,eta,pt,mass)
         if(mass.lt.172.5-masswindow.or.mass.gt.172.5+masswindow) return
      endif


ccccccccccccccccccccccc

c     We have everything ready to fill histograms.
c     First, impose cuts by assigning the logical variables

c-------------
c     away from regions close to collinear bs
      cut2=.true.
      call getyetaptmass(p_b,y,eta,ptb,mass)
      cut2=cut2.and.(ptb.gt.cut2_ptb)
      call getyetaptmass(p_bb,y,eta,ptbb,mass)
      cut2=cut2.and.(ptbb.gt.cut2_ptb)
      call getyetaptmass(p_b+p_bb,y,eta,pt,mass)
      cut2=cut2.and.(mass.gt.cut2_mbb)

c-------------
c    Denner-like cuts
      cut3=.true.
      cut3=cut3.and.(i_bjet.gt.0).and.(i_abjet.gt.0).and.(i_bjet.
     $     ne.i_abjet)
      if(cut3) then
         if(j_kt(i_bjet).lt.cut3_ptbj.or.abs(j_eta(i_bjet)).gt.cut3_etabj)
     $        cut3=.false.
         if(j_kt(i_abjet).lt.cut3_ptbj.or.abs(j_eta(i_abjet)).gt.cut3_etabj)
     $        cut3=.false.

         if(i_lwp.gt.0) then
            call getyetaptmass(p_lwp,y,eta,pt,mass)
            if(abs(eta).gt.cut3_etal.or.pt.lt.cut3_ptl) cut3=.false.
         endif

         if(i_lwm.gt.0) then
            call getyetaptmass(p_lwm,y,eta,pt,mass)
            if(abs(eta).gt.cut3_etal.or.pt.lt.cut3_ptl) cut3=.false.
         endif
         
         ptmiss2 = 0
         if(i_nuwm.gt.0) then
            ptmiss2 = ptmiss2 + p_nuwm(1:2)
         endif
         if(i_nuwp.gt.0) then
            ptmiss2 = ptmiss2 + p_nuwp(1:2)
         endif
         ptmiss = sqrt(ptmiss2(1)**2+ptmiss2(2)**2)

         if(ptmiss.lt.cut3_ptmiss) cut3=.false.

      endif

ccccccccccccccccccccccccc
      
c     total cross section
      call fillplotcuts('xsec',0.5d0,dsig,.true.,cut2,cut3)

c     b-jet related observables
      call getyetaptmass(j_p(:,i_bjet),y,eta,ptb,mass)
      call fillplotcuts('jb-pt',ptb,dsig,.true.,cut2,cut3)
      call fillplotcuts('jb-m',mass,dsig,.true.,cut2,cut3)
      call getyetaptmass(j_p(:,i_abjet),y,eta,ptbb,mass)
      call fillplotcuts('jb-pt',ptbb,dsig,.true.,cut2,cut3)
      call fillplotcuts('jb-m',mass,dsig,.true.,cut2,cut3)
      if(valid_bjet) then
c we normally require a minimum transverse momentum for the b jet
c     Plot the total energy flowing at a given Delta R from the b jet
         do jhep=1,nhep
            if( isForClustering(jhep) ) then
               call getyetaptmass(phep(1:4,jhep),y,eta,pt,mass)
               call getdydetadphidr(phep(1:4,jhep),j_p(:,i_bjet),dy,deta,delphi,dr)
               call fillplotcuts('jb-profile',dr,dsig*pt/j_kt(i_bjet),.true.,cut2,cut3)
            endif
         enddo         
      endif
      if(valid_abjet) then
         do jhep=1,nhep
            if( isForClustering(jhep) ) then
               call getyetaptmass(phep(1:4,jhep),y,eta,pt,mass)
               call getdydetadphidr(phep(1:4,jhep),j_p(:,i_abjet),dy,deta,delphi,dr)
               call fillplotcuts('jb-profile',dr,dsig*pt/j_kt(i_abjet),.true.,cut2,cut3)
            endif
         enddo
      endif


c     monte carlo truth top related observables (averaged over top, anti-top)
      if (topMCtruth) then
         call getyetaptmass(p_top_MCtruth,y,eta,pt,mass)
         call fillplotcuts('t-pt',pt,dsig,.true.,cut2,cut3)
         call fillplotcuts('t-m',mass,dsig,.true.,cut2,cut3)
         call fillplotcuts('t-m-zoom',mass,dsig,.true.,cut2,cut3)
         call getyetaptmass(p_atop_MCtruth,y,eta,pt,mass)
         call fillplotcuts('t-pt',pt,dsig,.true.,cut2,cut3)
         call fillplotcuts('t-m',mass,dsig,.true.,cut2,cut3)
         call fillplotcuts('t-m-zoom',mass,dsig,.true.,cut2,cut3)
      endif

c     reconstructed top related observables (averaged over top, anti-top)
      call getyetaptmass(p_wp+pbhadtp,y,eta,pt,mass)
      call fillplotcuts('wB-m',mass,dsig,.true.,cut2,cut3)
      call getyetaptmass(p_wm+pbhadtm,y,eta,pt,mass)
      call fillplotcuts('wB-m',mass,dsig,.true.,cut2,cut3)
      if(i_bjet.ne.0) then
         call getyetaptmass(p_wp+j_p(:,i_bjet),y,eta,pt,mass)
         call fillplotcuts('wjb-m',mass,dsig,.true.,cut2,cut3)
         call fillplotcuts('wjb-m-zoom',mass,dsig,.true.,cut2,cut3)
      endif
      if(i_abjet.ne.0) then
         call getyetaptmass(p_wm+j_p(:,i_abjet),y,eta,pt,mass)
         call fillplotcuts('wjb-m',mass,dsig,.true.,cut2,cut3)
         call fillplotcuts('wjb-m-zoom',mass,dsig,.true.,cut2,cut3)
      endif

c     reconstructed top-pair related observales
      call getyetaptmass(p_top+p_atop,y,eta,pt,mass)
      call fillplotcuts('wwjbjb-pt',pt,dsig,.true.,cut2,cut3)
      call fillplotcuts('wwjbjb-m',mass,dsig,.true.,cut2,cut3)

c     lepton-bjet system related observables
      call getyetaptmass(p_lwp+pbhadtp,y,eta,pt,mass)
      call fillplotcuts('lB-m',mass,dsig,.true.,cut2,cut3)
      call getyetaptmass(p_lwm+pbhadtm,y,eta,pt,mass)
      call fillplotcuts('lB-m',mass,dsig,.true.,cut2,cut3)
      if(i_bjet.ne.0) then
         call getyetaptmass(p_lwp+j_p(:,i_bjet),y,eta,pt,mass)
         call fillplotcuts('ljb-m',mass,dsig,.true.,cut2,cut3)
      endif
      if(i_abjet.ne.0) then
         call getyetaptmass(p_lwm+j_p(:,i_abjet),y,eta,pt,mass)
         call fillplotcuts('ljb-m',mass,dsig,.true.,cut2,cut3)
      endif
      
c     B hadron related observables (averaged over b and bbar hadrons)
c     frag: p_top.p_b/(p_top.p_b_max)
      mtop=sqrt(p_top(4)**2-p_top(1)**2-p_top(2)**2-p_top(3)**2)
      mwp=sqrt(p_wp(4)**2-p_wp(1)**2-p_wp(2)**2-p_wp(3)**2)
      ppp=phep(1:4,bhadfromtop)
      pbhadfromtop=ppp
      mb=sqrt(abs(ppp(4)**2-ppp(1)**2-ppp(2)**2-ppp(3)**2))
      p_bmax=sqrt((mtop**2-(mb+mwp)**2)*(mtop**2-(mb-mwp)**2))/(2*mtop)
      e_bmax=sqrt(p_bmax**2+mb**2)
      xb=(p_top(4)*ppp(4)-p_top(1)*ppp(1)
     1     -p_top(2)*ppp(2)-p_top(3)*ppp(3))/(mtop*e_bmax)
      call fillplotcuts('B-frag',xb,dsig,.true.,cut2,cut3)
c     ptdec: transverse momentum of the b (in the top rest frame) wrt the w axis
c     w is the w momentum in the top frame
      call boost2reson4(p_top,1,p_wp,w)
c     pb is the b momentum in the top frame
      call boost2reson4(p_top,1,pbhadfromtop,pb)
      ptb=sqrt( abs( (pb(1)**2+pb(2)**2+pb(3)**2)
     1     -(pb(1)*w(1)+pb(2)*w(2)+pb(3)*w(3))**2/
     1     (w(1)**2+w(2)**2+w(3)**2)))
      call fillplotcuts('B-ptdec',ptb,dsig,.true.,cut2,cut3)
c     frag: p_top.p_bb/(p_top.p_bb_max)
      mtb=sqrt(p_atop(4)**2-p_atop(1)**2-p_atop(2)**2-p_atop(3)**2)
      mwm=sqrt(p_wm(4)**2-p_wm(1)**2-p_wm(2)**2-p_wm(3)**2)
      ppp=phep(1:4,bhadfromatop)
      pbhadfromtop=ppp
      mbb=sqrt(abs(ppp(4)**2-ppp(1)**2-ppp(2)**2-ppp(3)**2))
      p_bbmax=sqrt((mtb**2-(mbb+mwm)**2)*(mtb**2-(mbb-mwm)**2))/(2*mtb)
      e_bbmax=sqrt(p_bbmax**2+mbb**2)
      xbb=(p_atop(4)*ppp(4)-p_atop(1)*ppp(1)
     1     -p_atop(2)*ppp(2)-p_atop(3)*ppp(3))/(mtb*e_bbmax)
      call fillplotcuts('B-frag',xbb,dsig,.true.,cut2,cut3)
c     ptdec: transverse momentum of the b (in the top rest frame) wrt the w axis
      call boost2reson4(p_atop,1,p_wm,w)
      call boost2reson4(p_atop,1,pbhadfromtop,pb)
      ptb=sqrt( abs( (pb(1)**2+pb(2)**2+pb(3)**2)
     1     -(pb(1)*w(1)+pb(2)*w(2)+pb(3)*w(3))**2/
     1     (w(1)**2+w(2)**2+w(3)**2)) )
      call fillplotcuts('B-ptdec',ptb,dsig,.true.,cut2,cut3)

c     B hadron from MCtruth related observables (averaged over b and bbar hadrons)
      if (topMCtruth) then
c        frag: p_top.p_b/(p_top.p_b_max)
         mtop=sqrt(p_top_MCtruth(4)**2-p_top_MCtruth(1)**2-p_top_MCtruth(2)**2-p_top_MCtruth(3)**2)
         mwp=sqrt(p_wp(4)**2-p_wp(1)**2-p_wp(2)**2-p_wp(3)**2)
         ppp=phep(1:4,bhadfromtop)
         pbhadfromtop=ppp
         mb=sqrt(abs(ppp(4)**2-ppp(1)**2-ppp(2)**2-ppp(3)**2))
         p_bmax=sqrt((mtop**2-(mb+mwp)**2)*(mtop**2-(mb-mwp)**2))/(2*mtop)
         e_bmax=sqrt(p_bmax**2+mb**2)
         xb=(p_top_MCtruth(4)*ppp(4)-p_top_MCtruth(1)*ppp(1)
     1        -p_top_MCtruth(2)*ppp(2)-p_top_MCtruth(3)*ppp(3))/(mtop*e_bmax)
         call fillplotcuts('B-frag-MCtruth',xb,dsig,.true.,cut2,cut3)
c        ptdec: transverse momentum of the b (in the top rest frame) wrt the w axis
         call boost2reson4(p_top_MCtruth,1,p_wp,w)
         call boost2reson4(p_top_MCtruth,1,pbhadfromtop,pb)
         ptb=sqrt( abs( (pb(1)**2+pb(2)**2+pb(3)**2)
     1        -(pb(1)*w(1)+pb(2)*w(2)+pb(3)*w(3))**2/
     1        (w(1)**2+w(2)**2+w(3)**2)))
         call fillplotcuts('B-ptdec-MCtruth',ptb,dsig,.true.,cut2,cut3)
c        frag: p_top.p_bb/(p_top.p_bb_max)
         mtb=sqrt(p_atop_MCtruth(4)**2-p_atop_MCtruth(1)**2-p_atop_MCtruth(2)**2-p_atop_MCtruth(3)**2)
         mwm=sqrt(p_wm(4)**2-p_wm(1)**2-p_wm(2)**2-p_wm(3)**2)
         ppp=phep(1:4,bhadfromatop)
         pbhadfromtop=ppp
         mbb=sqrt(abs(ppp(4)**2-ppp(1)**2-ppp(2)**2-ppp(3)**2))
         p_bbmax=sqrt((mtb**2-(mbb+mwm)**2)*(mtb**2-(mbb-mwm)**2))/(2*mtb)
         e_bbmax=sqrt(p_bbmax**2+mbb**2)
         xbb=(p_atop_MCtruth(4)*ppp(4)-p_atop_MCtruth(1)*ppp(1)
     1        -p_atop_MCtruth(2)*ppp(2)-p_atop_MCtruth(3)*ppp(3))/(mtb*e_bbmax)
         call fillplotcuts('B-frag-MCtruth',xbb,dsig,.true.,cut2,cut3)
c        ptdec: transverse momentum of the b (in the top rest frame) wrt the w axis
         call boost2reson4(p_atop_MCtruth,1,p_wm,w)
         call boost2reson4(p_atop_MCtruth,1,pbhadfromtop,pb)
         ptb=sqrt( abs( (pb(1)**2+pb(2)**2+pb(3)**2)
     1        -(pb(1)*w(1)+pb(2)*w(2)+pb(3)*w(3))**2/
     1        (w(1)**2+w(2)**2+w(3)**2)) )
         call fillplotcuts('B-ptdec-MCtruth',ptb,dsig,.true.,cut2,cut3)
      endif

      end

      subroutine getyetaptmass(p,y,eta,pt,mass)
      implicit none
      real * 8 p(4),y,eta,pt,mass,pv
      real *8 tiny
      parameter (tiny=1.d-5)
      y=0.5d0*log((p(4)+p(3))/(p(4)-p(3)))
      pt=sqrt(p(1)**2+p(2)**2)
      pv=sqrt(pt**2+p(3)**2)
      if(pt.lt.tiny)then
         eta=sign(1.d0,p(3))*1.d8
      else
         eta=0.5d0*log((pv+p(3))/(pv-p(3)))
      endif
      mass=sqrt(abs(p(4)**2-pv**2))
      end

      subroutine fillplotcuts(plotname,x,dsig,cut1,cut2,cut3)
      implicit none
      real *8 x,dsig(*)
      logical cut1,cut2,cut3
      character *(*) plotname
      character *5 suffix(3)
      common/csuffix/suffix
      integer ncuts
      common/ncuts/ncuts
      if(cut1) call filld(plotname//suffix(1),x,dsig)
      if(cut2.and.ncuts>=2) call filld(plotname//suffix(2),x,dsig)
      if(cut3.and.ncuts>=3) call filld(plotname//suffix(3),x,dsig)
      end

      function phepDot(p_A,p_B)
      implicit none
      real * 8  phepDot
      real * 8  p_A(4),p_B(4)
      phepDot=p_A(4)*p_B(4)-p_A(1)*p_B(1)
     1       -p_A(2)*p_B(2)-p_A(3)*p_B(3)
      end

      subroutine buildjets(mjets,palg,kt,eta,rap,phi,pjet,jetvechep,
     1                                               isForClustering)
c     arrays to reconstruct jets
      implicit  none
      include  'hepevt.h'
      integer   maxtracks,maxjets
      parameter (maxtracks=nmxhep,maxjets=nmxhep)
      integer   mjets,jetvechep(maxtracks)
      real * 8  kt(maxjets),eta(maxjets),rap(maxjets),
     1     phi(maxjets),pjet(4,maxjets)
      logical   isForClustering(maxtracks)
      real * 8  ptrack(4,maxtracks),pj(4,maxjets),ptot(4)
      integer   jetvec(maxtracks),itrackhep(maxtracks)
      integer   ntracks,njets
      integer   j,k,mu
      real * 8  r,palg,ptmin,pp,tmp
      integer sonofid
      external sonofid
C - Initialize arrays and counters for output jets
      kt = 0
      eta = 0
      rap = 0
      phi = 0
      pjet = 0
      jetvechep = 0

      ptrack = 0
      ntracks=0
      pj = 0
      jetvec = 0

C - Extract final state particles to feed to jet finder
      do j=1,nhep
         if(.not.isForClustering(j)) cycle
         if(ntracks.eq.maxtracks) then
            write(*,*) 'analyze: need to increase maxtracks!'
            write(*,*) 'ntracks: ',ntracks
            call exit(-1)
         endif
         ntracks=ntracks+1
         ptrack(:,ntracks) = phep(1:4,j)
         itrackhep(ntracks)=j
      enddo
      if (ntracks.eq.0) then
         mjets=0
         return
      endif
      if(palg.eq.11) then
c better to boost everything to the total CM of the tracks
         ptot = 0
c         print*, 'ntracks in buildjets for epem',ntracks
         do j=1,ntracks
            ptot = ptot + ptrack(:,j)
         enddo
         call boost2reson4(ptot,ntracks,ptrack,ptrack)
      endif
C --------------- C
C - Run FastJet - C
C --------------- C
C - R = 0.7   radius parameter
C - f = 0.75  overlapping fraction
      r     = 0.5d0
      ptmin = 0d0
      njets=mjets
      call fastjetppgenkt(ptrack,ntracks,r,palg,ptmin,pjet,njets,jetvec)
      mjets=min(mjets,njets)
      if(njets.eq.0) return
c check consistency
      do k=1,ntracks
         if(jetvec(k).gt.0) then
            do mu=1,4
               pj(mu,jetvec(k))=pj(mu,jetvec(k))+ptrack(mu,k)
            enddo
         endif
      enddo
      tmp=0
      do j=1,mjets
         do mu=1,4
            tmp=tmp+abs(pj(mu,j)-pjet(mu,j))
         enddo
      enddo
      if(tmp.gt.1d-4) then
         write(*,*) ' bug!'
      endif
C --------------------------------------------------------------------- C
C - Computing arrays of useful kinematics quantities for hardest jets - C
C --------------------------------------------------------------------- C
      do j=1,mjets
         kt(j)=sqrt(pjet(1,j)**2+pjet(2,j)**2)
         pp = sqrt(kt(j)**2+pjet(3,j)**2)
         eta(j)=0.5d0*log((pp+pjet(3,j))/(pp-pjet(3,j)))
         rap(j)=0.5d0*log((pjet(4,j)+pjet(3,j))/(pjet(4,j)-pjet(3,j)))
         phi(j)=atan2(pjet(2,j),pjet(1,j))
      enddo
      jetvechep = 0
      do j=1,ntracks
         jetvechep(itrackhep(j))=jetvec(j)
      enddo
      end


      function isbhadron(idhep)
      implicit none
      logical isbhadron
      integer idhep
      integer idigit
c all b hadrons have a 5 either a third (mesons) or fourth (barions) digit
      if(abs(idhep).eq.5.or.idigit(3,idhep).eq.5
     1     .or.idigit(4,idhep).eq.5) then
         isbhadron = .true.
      else
         isbhadron = .false.
      endif
      end

      function binhadron(idhep)
      implicit none
      integer binhadron
      integer idhep
      integer idigit
c all b hadrons have a 5 either a third (mesons) or fourth (barions) digit
      if(abs(idhep).eq.5) then
         binhadron = idhep
      elseif(idigit(4,idhep).eq.5) then
         binhadron = sign(5,idhep)
      elseif(idigit(4,idhep).eq.0.and.idigit(3,idhep).eq.5
c This line is to avoid to count b bbar resonances as having definite flavour
     1        .and.idigit(2,idhep).ne.5 ) then
         binhadron = - sign(5,idhep)
      else
         binhadron = 0
      endif
      end

      function idigit(ipos,inum)
      implicit none
      integer idigit,ipos,inum
      if(ipos.eq.0) then
         write(*,*) ' error: idigit(ipos.inum), ipos<=0!'
         call exit(-1)
      endif
      idigit = int(mod(abs(inum),10**ipos)/10**(ipos-1))
      end

      subroutine getdydetadphidr(p1,p2,dy,deta,dphi,dr)
      implicit none
      include 'pwhg_math.h' 
      real * 8 p1(*),p2(*),dy,deta,dphi,dr
      real * 8 y1,eta1,pt1,mass1,phi1
      real * 8 y2,eta2,pt2,mass2,phi2
      call getyetaptmass(p1,y1,eta1,pt1,mass1)
      call getyetaptmass(p2,y2,eta2,pt2,mass2)
      dy=y1-y2
      deta=eta1-eta2
      phi1=atan2(p1(1),p1(2))
      phi2=atan2(p2(1),p2(2))
      dphi=abs(phi1-phi2)
      dphi=min(dphi,2d0*pi-dphi)
      dr=sqrt(deta**2+dphi**2)
      end

      function comesfrom2(id,k)
      implicit none
      logical comesfrom2
      integer id,k
      include  'hepevt.h'
      integer kcurr, klast
      if(k.eq.id) then
         comesfrom2 = .true.
         return
      endif
      kcurr = k
      do while (.true.)
         klast = kcurr
         kcurr = jmohep(1,kcurr)
         if(kcurr.eq.id) then
            comesfrom2 = .true.
            return
         endif
         if(kcurr.gt.klast.or.kcurr.eq.0) exit
      enddo 
      comesfrom2=.false.
      end

      function hardest(nparticles, particles)
      implicit none
      include 'hepevt.h'
      integer hardest
      integer nparticles, particles(*)
      real * 8 maxhardness, pt
      integer j, mptr
      maxhardness = 0d0
      do j=1,nparticles
        mptr = particles(j)
        pt=sqrt(phep(1,mptr)**2+phep(2,mptr)**2)
        if (pt.gt.maxhardness) then
          hardest = mptr
          maxhardness = pt
        endif
      enddo
      end
