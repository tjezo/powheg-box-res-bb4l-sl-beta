      subroutine setborn(p,bflav,born,bornjk,bmunu)
c Wrapper subroutine to call OL Borns
      use openloops_powheg, only: openloops_born
      implicit none
      include 'nlegborn.h'
      include 'pwhg_math.h'
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 p(0:3,nlegs),bornjk(nlegs,nlegs)
      integer bflav(nlegs), bflavlep(nlegs), m, n
      real * 8 bmunu(0:3,0:3,nlegs),born
      real * 8 OLborn
      real * 8 OLborn_tt, OLborn_tw, OLborn_tbw, OLborn_no
      real * 8 OLbornjk(nlegs,nlegs), OLbmunu(0:3,0:3,nlegs)
      logical openloopsreal,openloopsvirtual
      common/copenloopsreal/openloopsreal,openloopsvirtual
c     gg channel
      if (bflav(1) == bflav(2)) then
c     qqbar channel
      else if (bflav(1) == -bflav(2)) then
         if (MOD(bflav(1),2) == 0) then
            bflav(1) = SIGN(2,bflav(1))
            bflav(2) = SIGN(2,bflav(2))
         else
            bflav(1) = SIGN(1,bflav(1))
            bflav(2) = SIGN(1,bflav(2))
         endif 
      endif
c     get the spin correlation from the leptonic OL matrix elements
      bflavlep = bflav
      bflavlep(5) = 13
      bflavlep(6) =-14
      call openloops_born(p,bflavlep,OLborn,OLbornjk,OLbmunu)
      OLbmunu = OLbmunu/OLborn
c     get the born amplitude from the semileptonic OL matrix elements
c     in which the colour and spin-correlation seem broken
      call openloops_born(p,bflav,OLborn,approx='pole_ww')
c     and restore the spin correlations 
      OLbmunu = OLbmunu*OLborn
c     and color correlations
      OLbornjk = 0
      OLbornjk(5,6) = + CF*OLborn
      OLbornjk(6,5) = + CF*OLborn
      OLbornjk(5,5) = - CF*OLborn
      OLbornjk(6,6) = - CF*OLborn
      born=OLborn
      bornjk=OLbornjk
      bmunu=OLbmunu
      end subroutine setborn

      subroutine borncolour_lh
      end

      subroutine finalize_lh
c reshuffle momenta to set the masses of final state particles neglected in the ME
c      call lhefinitemasses
      call check_leshouches
      end
