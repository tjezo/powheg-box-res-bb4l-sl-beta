      subroutine setvirtual(p,vflav,virtual)
c Virtual needs to be provided by the user and put here
      implicit none
      include 'nlegborn.h'
      include 'pwhg_st.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 p(0:3,nlegs)
      integer vflav(nlegs)
      real * 8 virtual,virtualol
      real * 8 powheginput
      external powheginput
      logical openloopsreal,openloopsvirtual
      common/copenloopsreal/openloopsreal,openloopsvirtual
      real * 8 bmunu(0:3,0:3,nlegs),born
      real * 8 bornjk(nlegs,nlegs)      
      real * 8 alphas4, alphas5, pwhg_alphas
      external pwhg_alphas
      include 'FourToFiveMatching.h'

      call openloops_virtual(p,vflav,virtualol)
      virtual = virtualol

      if (fourToFiveMatching) then
         call setborn(p,vflav,born,bornjk,bmunu)
         if (vflav(1).ne.0.and.vflav(2).ne.0) then
            if (fourToFiveMatchingAlphaS) then
               virtual = virtual - 4d0/3*TF*log(st_muren2/ph_bmass**2)*born
            else 
               ! do nothing 
            endif
         elseif  (vflav(1).eq.0.and.vflav(2).eq.0) then
            if (fourToFiveMatchingAlphaS) then
               virtual = virtual + 4d0/3*TF*log(st_mufact2/st_muren2)*born
            else 
               virtual = virtual + 4d0/3*TF*log(st_mufact2/ph_bmass**2)*born
            endif
         else
            write(*,*) "We should not be here!!"
            call pwhg_exit(-1)
         endif
      endif
      end
