ccc      subroutine computetotgen(totabs)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      real * 8 totabs
ccc      integer j
ccc      totabs=0
ccc      do j=1,mintw_nptrs
ccc         totabs = totabs + mintw_ptrs(j)%totabs
ccc      enddo
ccc      end
ccc
ccc      subroutine storeradarray(results)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      real * 8 results(*)
ccc      real * 8 totabs,totn
ccc      integer iid,k
ccc      do iid = 1,mintw_nptrs
ccc         if(mintw_currid.eq.mintw_ptrs(iid)%id) exit
ccc      enddo
ccc      do k=1,mintw_ptrs(iid)%nchannels
ccc         mintw_ptrs(iid)%results(k) = results(k)
ccc         if(results(k).lt.0) then
ccc            mintw_ptrs(iid)%signs(k) = -1
ccc         else
ccc            mintw_ptrs(iid)%signs(k) = 1
ccc         endif
ccc         if(k.eq.1) then
ccc            mintw_ptrs(iid)%cumulant(k) = abs(results(k))
ccc         else
ccc            mintw_ptrs(iid)%cumulant(k) = mintw_ptrs(iid)%cumulant(k-1) 
ccc     1           + abs(results(k))
ccc         endif
ccc      enddo
cccc if the sum is zero, no need to normalize, the event will never
cccc be generated
ccc      if(mintw_ptrs(iid)%cumulant(mintw_ptrs(iid)%nchannels)>0) then
ccc         mintw_ptrs(iid)%cumulant = mintw_ptrs(iid)%cumulant/
ccc     1        mintw_ptrs(iid)%cumulant(mintw_ptrs(iid)%nchannels)
ccc      endif
ccc      end
ccc
ccc
ccc      subroutine chooseid(r,id)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      real * 8 r
ccc      character *(*) id
ccc      real * 8 totabs,totn
ccc      integer j
ccc      totabs=0
ccc      do j=1,mintw_nptrs
ccc         totabs = totabs + mintw_ptrs(j)%totabs
ccc      enddo
ccc      totn = 0
ccc      do j=1,mintw_nptrs
ccc         totn = totn + mintw_ptrs(j)%totabs/totabs
ccc         if(totn.gt.r) then
ccc            id = mintw_ptrs(j)%id
ccc            return
ccc         endif
ccc      enddo
ccc      end
ccc
ccc      subroutine choosesubproc(id,iproc,isign)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      character *(*) id
ccc      integer iproc,isign
ccc      real * 8 random,r
ccc      procedure () :: random
ccc      integer iid
ccc      do iid = 1,mintw_nptrs
ccc         if(id.eq.mintw_ptrs(iid)%id) exit
ccc      enddo
ccc      r = random()
ccc      do iproc = 1,mintw_ptrs(iid)%nchannels
ccc         if(r.lt.mintw_ptrs(iid)%cumulant(iproc)) exit
ccc      enddo
cccc just in case the total cumulant is slightly below 1
ccc      iproc = min(iproc,mintw_ptrs(iid)%nchannels)
ccc      isign = mintw_ptrs(iid)%signs(iproc)
ccc      end
ccc
ccc
ccc      subroutine loadmintupbwrapper(id)
ccc      implicit none
ccc      character *(*) id
ccc      character * 20 postfix
ccc      integer icurrent
ccc      include 'pwhg_mintw.h'
ccc      call getcurrentptrindex(id,icurrent)
ccc      call getpostfix(id,postfix)
ccc      if(icurrent.lt.0) then
ccc         write(*,*)' loadmintupbwrapper: id ',trim(id),' not found. Exiting ...'
ccc         call exit(-1)
ccc      endif
ccc      call loadmintupb(
ccc     1     mintw_ptrs(icurrent)%ndim,
ccc     2     trim(postfix)//'upb',
ccc     3     mintw_ptrs(icurrent)%nind,
ccc     4     mintw_ptrs(icurrent)%yindmax,
ccc     5     mintw_ptrs(icurrent)%yindmaxrat,
ccc     6     mintw_ptrs(icurrent)%ymax,
ccc     7     mintw_ptrs(icurrent)%ymaxrat)
ccc      end
ccc
ccc
ccc      subroutine mintwrapper(id,imode,doregrid,iunstat)
ccc      implicit none
ccc      character *(*) id
ccc      integer imode,iunstat
ccc      logical doregrid
cccc end arg list
ccc      integer ndim,nind,nchannels
ccc      include 'pwhg_mintw.h'
ccc      include 'nlegborn.h'
ccc      include 'pwhg_flst.h'
ccc      include 'pwhg_flg.h'
ccc      integer icurrent,ncall,itmx,j,kdim,kint,ibtilde
ccc      integer parallelstage,xgriditeration
ccc      character * 20 postfix
ccc      real * 8 powheginput
ccc      procedure () :: btilde, sigremnant, sigregular
ccc      procedure (), pointer :: fun => NULL()
ccc      procedure () :: powheginput
ccc      mintw_currid = id
ccc      
ccc      if(id.eq.'btilde'.or.id.eq.'btildeborn') then
ccc         fun => btilde
ccc         nind = flst_nbornresgroup
ccc         ndim = ndiminteg
ccc         if(id.eq.'btildeborn') ndim = ndim - 3
ccc         nchannels = flst_nborn
ccc      elseif(id.eq.'remn') then
ccc         fun => sigremnant
ccc         nind = flst_nbornresgroup
ccc         ndim = ndiminteg
ccc         nchannels = flst_nalr
ccc      elseif(id.eq.'reg') then
ccc         fun => sigregular
ccc         nind = flst_nregularresgroup
ccc         ndim = ndiminteg
ccc         nchannels = flst_nregular
ccc      endif
ccc
ccc      if(imode.eq.0) then
ccc         write(*,*) ' mintwrapper: Computing the importance sampling'
ccc         write(*,*) ' grid for '//id
ccc      elseif(imode.eq.1) then
ccc         write(*,*) ' mintwrapper: Computing cross section'
ccc         write(*,*) ' and upper bounding envelopes for '//id         
ccc      endif
ccc         
ccc
ccc      call getparallelparms(parallelstage,xgriditeration)
ccc
ccc      call getcurrentptrindex(id,icurrent)
ccc      if(icurrent.lt.0) then
ccc         mintw_nptrs = mintw_nptrs + 1
ccc         icurrent = mintw_nptrs
ccc         mintw_ptrs(icurrent)%id = id
ccc         call allocate_mintw(mintw_ptrs(icurrent),ndim,nind,nchannels)
ccc      endif
ccc
ccc      call getpostfix(id,postfix)
ccc
ccc      do j=1,ndim
ccc         mintw_ptrs(icurrent)%ifold(j)=1
ccc      enddo
ccc
ccc      call resettotals(mintw_ptrs(icurrent))
ccc
ccc      if(imode.eq.0) then
ccc         if(id.eq.'remn' .and. flg_samexgridasbtilde) then
ccc            call getcurrentptrindex('btilde',ibtilde)
ccc            mintw_ptrs(icurrent)%xint   =  mintw_ptrs(ibtilde)%xint   
ccc            mintw_ptrs(icurrent)%xgrid  =  mintw_ptrs(ibtilde)%xgrid  
ccc            mintw_ptrs(icurrent)%xacc   =  mintw_ptrs(ibtilde)%xacc   
ccc            mintw_ptrs(icurrent)%nhits  =  mintw_ptrs(ibtilde)%nhits  
ccc            mintw_ptrs(icurrent)%wind   =  mintw_ptrs(ibtilde)%wind   
ccc            mintw_ptrs(icurrent)%accind =  mintw_ptrs(ibtilde)%accind 
ccc            mintw_ptrs(icurrent)%indhits=  mintw_ptrs(ibtilde)%indhits
ccc            return
ccc         else
cccc     It is computing the importance sampling grids. The grids must be initialized
cccc     to constant spacing in the first iteration. In parallel mode, each iteration
cccc     is invoked separately; so, only the first parallel mode iteration should be
cccc     initialized. It enters here only if imode=0, that means either parallestage
cccc     equal 1 or 3 (when building a born envelope). If parallelstage is 1, should
cccc     only do it for xgriditeration 1, thus the following logic
ccc            if(parallelstage.ne.1.or.xgriditeration.eq.1) then
ccc               do kdim=1,ndim
ccc                  do kint=0,nintervals
ccc                     mintw_ptrs(icurrent)%xgrid(kint,kdim)=dble(kint)/nintervals
ccc                  enddo
ccc               enddo
ccc               do j=1,nind
ccc                  mintw_ptrs(icurrent)%wind(j) = dble(j-1) / nind
ccc               enddo
ccc               mintw_ptrs(icurrent)%wind(nind+1) = 1
ccc            endif
ccc
ccc            ncall = powheginput("#ncall1"//trim(postfix))
ccc            if(ncall.lt.0) then
ccc               ncall = powheginput("#ncall1")
ccc            endif
ccc            mintw_ptrs(icurrent)%ncall = ncall
ccc         
ccc            itmx = powheginput("#itmx1"//trim(postfix))
ccc            if(itmx.lt.0) then
ccc               itmx = powheginput("itmx1")
ccc            endif
ccc            mintw_ptrs(icurrent)%itmx = itmx
ccc         endif
ccc      elseif(imode.eq.1) then
ccc         if(flg_storemintupb) call startstoremintupb(trim(postfix)//'upb')
cccc basically reset random number generator, if optionally required
ccc         call setstage2init
ccc         if(id.eq.'btilde') then
ccc            mintw_ptrs(icurrent)%ifold(ndim-2) = powheginput("foldcsi")
ccc            mintw_ptrs(icurrent)%ifold(ndim-1) = powheginput("foldy")
ccc            mintw_ptrs(icurrent)%ifold(ndim)   = powheginput("foldphi")
ccc         endif
cccc     reset accumulated values in btilde function
ccc
ccc         ncall = powheginput("#ncall2"//trim(postfix))
ccc         if(ncall.lt.0) then
ccc            ncall = powheginput("#ncall2")
ccc         endif
ccc         mintw_ptrs(icurrent)%ncall = ncall
ccc         
ccc         itmx = powheginput("#itmx2"//trim(postfix))
ccc         if(itmx.lt.0) then
ccc            itmx = powheginput("itmx2")
ccc         endif
ccc         mintw_ptrs(icurrent)%itmx = itmx
ccc      endif
ccc      
ccc      call mint(fun,
ccc     1     mintw_ptrs(icurrent)%ndim,
ccc     2     mintw_ptrs(icurrent)%ncall,
ccc     3     mintw_ptrs(icurrent)%itmx,
ccc     4     mintw_ptrs(icurrent)%ifold,
ccc     5     imode,doregrid,
ccc     6     mintw_ptrs(icurrent)%nind,
ccc     7     mintw_ptrs(icurrent)%wind,
ccc     7     mintw_ptrs(icurrent)%accind,
ccc     8     mintw_ptrs(icurrent)%indhits,
ccc     9     mintw_ptrs(icurrent)%yindmax,
ccc     1     mintw_ptrs(icurrent)%yindmaxrat,
ccc     2     mintw_ptrs(icurrent)%xgrid,
ccc     3     mintw_ptrs(icurrent)%xint,
ccc     4     mintw_ptrs(icurrent)%xacc,
ccc     5     mintw_ptrs(icurrent)%nhits,
ccc     6     mintw_ptrs(icurrent)%ymax,
ccc     7     mintw_ptrs(icurrent)%ymaxrat,
ccc     8     mintw_ptrs(icurrent)%ans,
ccc     9     mintw_ptrs(icurrent)%err)
ccc
ccc      if(imode == 0) then
cccc     In imode=0 we don't call writestat(iunstat), because tot* and etot* variables
cccc     are still undefined. Write instead the estimate for the absolute total
ccc         write(*,*) trim(id)//': estimated absolute value cross section:',
ccc     1        mintw_ptrs(icurrent)%ans,'+-',mintw_ptrs(icurrent)%err
ccc         write(iunstat,*) trim(id)//': estimated absolute value cross section:',
ccc     1        mintw_ptrs(icurrent)%ans,'+-',mintw_ptrs(icurrent)%err
ccc      elseif(imode == 1) then
ccc         if(flg_storemintupb) call stopstoremintupb
ccc         call finaltotals
cccc     The following will be written to iunstat by writestat(iunstat). No need to repeat it here.
ccc         write(*,*) id//' pos.   weights:', mintw_ptrs(icurrent)%totpos,' +- ',
ccc     1                                      mintw_ptrs(icurrent)%etotpos
ccc         write(*,*) id//' |neg.| weights:', mintw_ptrs(icurrent)%totneg,' +- ',
ccc     1                                      mintw_ptrs(icurrent)%etotneg
ccc         write(*,*) id//' total (pos.-|neg.|):', mintw_ptrs(icurrent)%tot,' +- ',
ccc     1                                           mintw_ptrs(icurrent)%etot
ccc      endif
ccc
ccc      end
ccc
ccc      subroutine mintwrapper_sign_result(id,iind,w)
ccc      implicit none
ccc      character *(*) id
ccc      integer iind
ccc      real * 8 res,w
ccc      call mintwrapper_result0(id,iind,res,w)
ccc      end
ccc      subroutine mintwrapper_result(id,iind,res)
ccc      implicit none
ccc      character *(*) id
ccc      integer iind
ccc      real * 8 res,w
ccc      call mintwrapper_result0(id,iind,res,w)
ccc      end
ccc      subroutine mintwrapper_result0(id,iind,res,w)
ccc      implicit none
ccc      character *(*) id
ccc      integer iind
ccc      real * 8 res,w
ccc      include 'pwhg_mintw.h'
ccc      integer icurrent
ccc      call getcurrentptrindex(id,icurrent)
ccc      if(icurrent.lt.0) then
ccc         write(*,*)' mintwrapper_result: arrays for ',id,' are not there!'
ccc         write(*,*)' exiting ...'
ccc         call exit(-1)
ccc      endif
ccc      if(iind > mintw_ptrs(icurrent)%nchannels) then
ccc         write(*,*)' mintwrapper_result: index ',iind,' out of bounds'
ccc         write(*,*)' exiting ...'
ccc         call exit(-1)
ccc      endif
ccc      res = mintw_ptrs(icurrent)%results(iind)
ccc      w = w * mintw_ptrs(icurrent)%signs(iind)
ccc      end
ccc
ccc      subroutine genwrapper(id,imode,mcalls,icalls,x,ind)
ccc      implicit none
ccc      character *(*) id
ccc      integer imode,mcalls,icalls,ind
ccc      real * 8 x(*)
ccc      include 'nlegborn.h'
ccc      include 'pwhg_mintw.h'
ccc      include 'pwhg_flg.h'
ccc      include 'pwhg_rad.h'
ccc      logical save_nlotest
ccc      integer icurrent
ccc      real * 8 gen_sigma,gen_sigma2,gen_isigma,gen_totev
cccc     Feedback from the integrator, used to provide an estimate of
cccc     the cross section while generating an event
ccc      real * 8 sigma, sigma2
ccc      integer isigma
ccc      common/gencommon/sigma,sigma2,isigma
ccc      procedure () :: btilde, sigremnant, sigregular
ccc      procedure (), pointer :: fun => NULL()
ccc      mintw_currid = id
ccc      if(id.eq.'btilde' .or.id.eq.'btildeborn' ) then
ccc         fun => btilde
ccc      elseif(id.eq.'remn') then
ccc         fun => sigremnant
ccc      elseif(id.eq.'reg') then
ccc         fun => sigregular
ccc      endif
ccc
ccc      flg_ingen = .true.
ccc      call getcurrentptrindex(id,icurrent)
ccc      if(icurrent.lt.0) then
ccc         write(*,*)' genwrapper: arrays for ',id,' are not there!'
ccc         write(*,*)' exiting ...'
ccc         call exit(-1)
ccc      endif
ccc      save_nlotest = flg_nlotest
ccc      flg_nlotest = .false.
ccc      call gen(
ccc     1   fun,
ccc     2   mintw_ptrs(icurrent)%ndim,
ccc     3   mintw_ptrs(icurrent)%xgrid,
ccc     4   mintw_ptrs(icurrent)%ymax,
ccc     5   mintw_ptrs(icurrent)%ymaxrat,
ccc     6   mintw_ptrs(icurrent)%nind,
ccc     7   mintw_ptrs(icurrent)%wind,
ccc     8   mintw_ptrs(icurrent)%yindmax,
ccc     9   mintw_ptrs(icurrent)%yindmaxrat,
ccc     1   mintw_ptrs(icurrent)%xmmm,
ccc     2   mintw_ptrs(icurrent)%xindmmm,
ccc     3   mintw_ptrs(icurrent)%ifold,
ccc     4   imode,mcalls,icalls,x,ind )
ccc
ccc      if (imode == 1) then
ccc        mintw_ptrs(icurrent)%gen_sigma  =
ccc     1       mintw_ptrs(icurrent)%gen_sigma  + sigma
ccc        mintw_ptrs(icurrent)%gen_sigma2 =
ccc     2       mintw_ptrs(icurrent)%gen_sigma2 + sigma2
ccc        mintw_ptrs(icurrent)%gen_isigma =
ccc     3       mintw_ptrs(icurrent)%gen_isigma + isigma
ccc        mintw_ptrs(icurrent)%gen_totev  =
ccc     4       mintw_ptrs(icurrent)%gen_totev  + rad_genubexceeded
ccc
ccc        gen_sigma  = mintw_ptrs(icurrent)%gen_sigma
ccc        gen_isigma = mintw_ptrs(icurrent)%gen_isigma
ccc        gen_sigma2 = mintw_ptrs(icurrent)%gen_sigma2
ccc        gen_totev  = mintw_ptrs(icurrent)%gen_totev
ccc
ccc        call setcnt(trim(id)//" cross section used:",
ccc     1        mintw_ptrs(icurrent)%totabs)
cccc     prevent division by zero if gen_isigma=0 
ccc        if (gen_isigma.ne.0) then
ccc           call setcnt(trim(id)//" cross section estimate:",
ccc     2         gen_sigma/gen_isigma)
ccc           call setcnt(trim(id)//" cross section estimate"//
ccc     3          " num. points:", dble(gen_isigma))
ccc           call setcnt(trim(id)//
ccc     1          " cross section error estimate:",
ccc     2          sqrt(((gen_sigma2/gen_isigma)-(gen_sigma/gen_isigma)**2)/
ccc     3          gen_isigma))
ccc        endif
ccc        if(flg_ubexcess_correct) then
ccc           call setcnt(trim(id)//
ccc     1          " bound violation correction factor:",
ccc     1          gen_totev/mcalls)
ccc        endif
ccc      endif
ccc      flg_nlotest = save_nlotest
ccc      flg_ingen = .false.
ccc      end
ccc
ccc      subroutine getpostfix(id,postfix)
ccc      character *(*) id,postfix
ccc      if(id.eq.'btilde') then
ccc         postfix='btl'
ccc      elseif(id.eq.'btildeborn') then
ccc         postfix='btlbrn'
ccc      elseif(id.eq.'remn') then
ccc         postfix='rm'
ccc      elseif(id.eq.'reg') then
ccc         postfix='reg'
ccc      endif
ccc      end
ccc
ccc      subroutine allocate_mintw(ptr,ndim,nind,nchannels)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      type(mintw_pointer) :: ptr
ccc      integer ndim,nind,nchannels
ccc      integer istat
ccc      ptr%ndim = ndim
ccc      ptr%nind = nind
ccc      allocate(ptr%wind(nind+1),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%accind(nind),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%yindmax(nind),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%yindmaxrat(nind),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%xgrid(0:nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%xacc(0:nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%ymax(nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%ymaxrat(nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%xmmm(nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%xindmmm(nind),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc
ccc      allocate(ptr%nchannels,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%nentries,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc      allocate(ptr%totj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totabsj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totposj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totnegj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotabsj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotposj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotnegj(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%results(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%cumulant(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%signs(nchannels),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc
ccc      allocate(ptr%tot,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totabs,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totpos,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%totneg,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etot,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotabs,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotpos,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%etotneg,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc
ccc      allocate(ptr%ifold(ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%indhits(nind),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      allocate(ptr%nhits(nintervals,ndim),stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc      ptr%nchannels = nchannels
ccc
ccc      ptr%gen_sigma  = 0
ccc      ptr%gen_sigma2 = 0
ccc      ptr%gen_isigma = 0
ccc      ptr%gen_totev  = 0
ccc      
ccc      return
ccc 999  continue
ccc      
ccc      write(*,*) 'mintwrapper: allocation failed, status=',istat
ccc      call exit(-1)
ccc      end
ccc
ccc      subroutine deallocate_mintw(ptr)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      type(mintw_pointer) :: ptr
ccc      integer istat
ccc      deallocate(ptr%wind,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%accind,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%yindmax,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%yindmaxrat,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%xgrid,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%xacc,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%ymax,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%ymaxrat,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%xmmm,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%xindmmm,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc      deallocate(ptr%nchannels,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%nentries,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc
ccc      deallocate(ptr%totj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totabsj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totposj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totnegj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotabsj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotposj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotnegj,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%results,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%cumulant,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%signs,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc      deallocate(ptr%tot,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totabs,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totpos,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%totneg,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etot,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotabs,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotpos,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%etotneg,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc
ccc      deallocate(ptr%ifold,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%indhits,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      deallocate(ptr%nhits,stat=istat)
ccc      if(istat.ne.0) goto 999
ccc      return
ccc 999  continue
ccc      write(*,*) 'mintwrapper: deallocation failed, status=',istat
ccc      call exit(-1)
ccc      end
ccc
ccc      subroutine getcurrentptrindex(id,icurrent)
ccc      implicit none
ccc      character *(*) id
ccc      integer icurrent
ccc      include 'pwhg_mintw.h'
ccc      integer j
ccc      do j=1,mintw_nptrs
ccc         if(id.eq.mintw_ptrs(j)%id) exit
ccc      enddo
ccc      if(j.le.mintw_nptrs) then
ccc         icurrent = j
ccc      else
ccc         icurrent = -1
ccc      endif
ccc      end

      
      subroutine storexgrids_wdec(id,xgriditeration)
      implicit none
      include 'pwhg_mintw.h'
      include 'pwhg_rnd.h'
      character *(*) id
      integer xgriditeration
      integer iun,icu,ndim
      character * 20 postfix
      character * 40 mergelabels
      character * 8 chnum
      character * 20 pwgprefix
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      call newunit(iun)
      call getpostfix(id,postfix)
      call getcurrentptrindex(id,icu)
      ndim = mintw_ptrs(icu)%ndim
      if(xgriditeration.gt.0) then
         write(chnum,'(i4)') xgriditeration
         chnum = '-xg'//trim(adjustl(chnum))//'-'
         open(unit=iun,
     1        file=mergelabels(pwgprefix(1:lprefix)//trim(chnum)//'xgrid',
     2        postfix,rnd_cwhichseed,'wdec.dat'),
     3        form='unformatted',status='unknown')
      else
         open(unit=iun,
     1        file=mergelabels(pwgprefix(1:lprefix)//'xgrid',
     2        postfix,rnd_cwhichseed,'wdec.dat'),
     3        form='unformatted',status='unknown')
      endif
      write(iun) mintw_ptrs(icu)%ndim,mintw_ptrs(icu)%nind
      write(iun) mintw_ptrs(icu)%xint
      write(iun) mintw_ptrs(icu)%xgrid
      write(iun) mintw_ptrs(icu)%xacc
      write(iun) mintw_ptrs(icu)%nhits
      write(iun) mintw_ptrs(icu)%wind
      write(iun) mintw_ptrs(icu)%accind
      write(iun) mintw_ptrs(icu)%indhits
      close(iun)
      end
      
      subroutine loadxgridsn_wdec(id,level,iret)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_mintw.h'
      include 'pwhg_rnd.h'
      include 'pwhg_par.h'
      character *(*) id
      integer level,iret
      integer iun,icu,ndim,nind,nchannels
      character * 20 postfix
      integer nfiles,jfile,jdim,jind,jfound,k,kdim
      character * 1 chjind
      character * 20 pwgprefix
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      character * 100 file
      character * 40 mergelabels
      character * 4 chseed
      character * 8 chnum
      logical lpresent0,lpresent
      integer ilevel,l
      integer parallelstages,xgriditeration
      character * 10 parlabel
      real * 8 powheginput
      type(mintw_pointer) :: ptr

      if(powheginput('use-old-grid').eq.0) then
         iret=1
         return
      endif

      if(id.eq.'btilde'.or.id.eq.'btildeborn') then
         nchannels = flst_nborn
      elseif(id.eq.'remn') then
         nchannels = flst_nalr
      elseif(id.eq.'reg') then
         nchannels = flst_nregular
      endif

      call newunit(iun)
      call getpostfix(id,postfix)
      call getcurrentptrindex(id,icu)
      if(icu.gt.0) then
         write(*,*) ' loadxgrids: grid with id=',trim(id),' already allocated!'
         write(*,*) ' exiting ...'
         call pwhg_exit(-1)
      endif

c first probe if there are files to load
      nfiles=par_maxseeds

      if(level.le.0) then
         file = mergelabels(pwgprefix(1:lprefix)//'xgrid',
     1        postfix,'none','wdec.dat')
         inquire(file= file,exist=lpresent0)
      else
         lpresent0 = .false.
      endif
      ilevel = 0
      do jfile=1,nfiles
         write(chseed,'(i4)') jfile
         call blanktozero(chseed)
         do l=1,par_maxxgriditerations
c with this statement, it will always exit with ilevel = level,
c or ilevel = 0 if none is found.
            if(level.gt.0.and.l.ne.level) cycle
            write(chnum,'(i4)') l
            chnum = '-xg'//trim(adjustl(chnum))//'-'
            file = mergelabels(pwgprefix(1:lprefix)//trim(chnum)//'xgrid',
     1           postfix,chseed,'wdec.dat')
            inquire(file= file,exist=lpresent)
            if(lpresent) ilevel = max(ilevel,l)
         enddo
      enddo

      if(ilevel.gt.0.and.lpresent0) then
         write(*,*) ' loadxgrids: there are parallel and single-run '//
     1     ' xgrid files ... exiting '
         call pwhg_exit(-1)
      endif

      if(ilevel.eq.0.and..not.lpresent0) then
         iret = -1
         return
      endif

      mintw_nptrs = mintw_nptrs + 1
      icu = mintw_nptrs
      mintw_ptrs(icu)%id = id

      if(ilevel.eq.0) then
         file = mergelabels(pwgprefix(1:lprefix)//'xgrid',
     1        postfix,'none','wdec.dat')
         open(unit=iun,file=file,form='unformatted',status='unknown')
         read(iun) ndim,nind
         call allocate_mintw(mintw_ptrs(icu),ndim,nind,nchannels)

         write(*,*) ' loadxgrids: loading ',file

         read(iun) mintw_ptrs(icu)%xint
         read(iun) mintw_ptrs(icu)%xgrid
         read(iun) mintw_ptrs(icu)%xacc
         read(iun) mintw_ptrs(icu)%nhits
         read(iun) mintw_ptrs(icu)%wind
         read(iun) mintw_ptrs(icu)%accind
         read(iun) mintw_ptrs(icu)%indhits
         close(iun)
         iret = 0
      else
         write(chnum,'(i4)') ilevel
         chnum = '-xg'//trim(adjustl(chnum))//'-'
         ndim = -1
         nind = -1
         jfound = 0
         do jfile=1,nfiles
            write(chseed,'(i4)') jfile
            call blanktozero(chseed)
            file = mergelabels(pwgprefix(1:lprefix)//trim(chnum)//'xgrid',
     1           postfix,chseed,'wdec.dat')
            inquire(file= file,exist=lpresent)
            if(lpresent) then
               jfound = jfound + 1
               open(unit=iun,file=file,form='unformatted',status='unknown')
               write(*,*) ' loadxgrids: loading ',file
               read(iun) jdim,jind
               if(ndim.lt.0) then
                  ndim = jdim
                  nind = jind
c At this point we know that there are files to load
                  call allocate_mintw(ptr,ndim,nind,0)
                  call allocate_mintw(mintw_ptrs(icu),ndim,nind,nchannels)
                  mintw_ptrs(icu)%xint    = 0
                  mintw_ptrs(icu)%xacc    = 0
                  mintw_ptrs(icu)%nhits   = 0
                  mintw_ptrs(icu)%accind  = 0
                  mintw_ptrs(icu)%indhits = 0
               elseif(ndim.ne.jdim.or.nind.ne.jind) then
                  write(*,*) ' loadxgrids: inconsistent grid in'
                  write(*,*) file
                  write(*,*) ' exiting ...'
                  call exit(-1)
               endif
               read(iun) ptr%xint
               read(iun) ptr%xgrid
               read(iun) ptr%xacc
               read(iun) ptr%nhits
               read(iun) ptr%wind
               read(iun) ptr%accind
               read(iun) ptr%indhits
               mintw_ptrs(icu)%xint    = mintw_ptrs(icu)%xint  + ptr%xint
               mintw_ptrs(icu)%xacc    = mintw_ptrs(icu)%xacc  + ptr%xacc
               mintw_ptrs(icu)%nhits   = mintw_ptrs(icu)%nhits + ptr%nhits
               mintw_ptrs(icu)%accind  = mintw_ptrs(icu)%accind  + ptr%accind
               mintw_ptrs(icu)%indhits = mintw_ptrs(icu)%indhits + ptr%indhits
               if(jfound.eq.1) then
                  mintw_ptrs(icu)%xgrid = ptr%xgrid
                  mintw_ptrs(icu)%wind = ptr%wind
               endif
            endif
         enddo
         if(jfound == 0) then
            write(*,*) ' loadxgrids: did not find any file for loading'
            write(*,*) ' exiting ...'
            call pwhg_exit(-1)
         else
            iret = 0
         endif
         mintw_ptrs(icu)%xint  = mintw_ptrs(icu)%xint/jfound
         call deallocate_mintw(ptr)
      endif

      call getparallelparms(parallelstages,xgriditeration)
      call setparallellabel(parallelstages,xgriditeration,parlabel)

      do jind=1,nind

         write(chjind,"(I1)") jind

         if(parallelstages.lt.0) then
            file=mergelabels(pwgprefix(1:lprefix),'xgrid-'//trim(postfix)//'-rh-'//chjind,
     1           rnd_cwhichseed,'.top')
         else
            file=mergelabels(pwgprefix(1:lprefix),trim(parlabel)//'-xgrid-'//trim(postfix)//'-rh-'//chjind,
     1           rnd_cwhichseed,'.top')
         endif

         call regridplotopen(file)

         do kdim=1,ndiminteg
            call regrid(mintw_ptrs(icu)%xacc(:,kdim,jind),mintw_ptrs(icu)%xgrid(:,kdim,jind),
     1           mintw_ptrs(icu)%nhits(:,kdim,jind),kdim,nintervals,jind)
         enddo
         if(jind.eq.1.and.nind.gt.1) then
            call regridind(nind,mintw_ptrs(icu)%accind,mintw_ptrs(icu)%indhits,
     1           mintw_ptrs(icu)%wind)
            iret = 0
         endif

         call regridplotclose

      enddo

      end

      subroutine storegrids_wdec(id,label)
      implicit none
      include 'pwhg_mintw.h'
      include 'pwhg_rnd.h'
      character *(*) id,label
      integer xgriditeration
      integer iun,icu,ndim
      character * 20 postfix
      character * 40 mergelabels
      character * 20 pwgprefix
      character * 8 chnum
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      call newunit(iun)
      call getpostfix(id,postfix)
      call getcurrentptrindex(id,icu)
      ndim = mintw_ptrs(icu)%ndim
      open(unit=iun,
     1     file=mergelabels(pwgprefix(1:lprefix)//trim(label),
     2     postfix,rnd_cwhichseed,'wdec.dat'),
     3     form='unformatted',status='unknown')

      write(iun) mintw_ptrs(icu)%ndim,mintw_ptrs(icu)%nind,mintw_ptrs(icu)%nchannels
      write(iun) mintw_ptrs(icu)%ifold
      write(iun) mintw_ptrs(icu)%xgrid
      write(iun) mintw_ptrs(icu)%wind
      write(iun) mintw_ptrs(icu)%yindmax
      write(iun) mintw_ptrs(icu)%yindmaxrat
      write(iun) mintw_ptrs(icu)%ymax
      write(iun) mintw_ptrs(icu)%ymaxrat

      write(iun) mintw_ptrs(icu)%nentries
      write(iun) mintw_ptrs(icu)%tot
      write(iun) mintw_ptrs(icu)%totabs
      write(iun) mintw_ptrs(icu)%totpos
      write(iun) mintw_ptrs(icu)%totneg
      write(iun) mintw_ptrs(icu)%etot
      write(iun) mintw_ptrs(icu)%etotabs
      write(iun) mintw_ptrs(icu)%etotpos
      write(iun) mintw_ptrs(icu)%etotneg

      close(iun)
      end

      subroutine loadgrids_wdec(id,label,iret)
      implicit none
      character *(*) id,label
      integer iret
      include 'pwhg_rnd.h'
      include 'pwhg_mintw.h'
      include 'pwhg_par.h'
      type(mintw_pointer) :: ptr

      integer ndim,nind,nchannels,jfound,icu,iun
      integer ios
      character * 20 pwgprefix,postfix
      integer lprefix
      common/cpwgprefix/pwgprefix,lprefix
      character * 4 chseed, firstfound
      integer j,k,jfile,nfiles
      logical lpresent,manyfiles,filefound
      character * 40 mergelabels
      real * 8 powheginput
      external powheginput
      if(powheginput('use-old-grid').eq.0) then
         iret=1
         return
      endif
      iret=0
      call getpostfix(id,postfix)
      call newunit(iun)

      open(unit=iun,file=mergelabels(pwgprefix(1:lprefix)//trim(label),
     1     postfix,'none','-wdec.dat'),
     2     form='unformatted',status='old',iostat=ios)

      write(*,*) ' entering loadgrids('//trim(id)//', '//trim(label)//'),'

      if(ios.eq.0) then
         nfiles=1
         manyfiles=.false.
         write(*,*) ' Opened file '//
     1     mergelabels(pwgprefix(1:lprefix)//trim(label),
     1     postfix,'none','-wdec.dat')
      else
         nfiles=par_maxseeds
         manyfiles=.true.
      endif
c Try to open and merge a set of grid files, generated with different
c random seeds
      filefound=.false.
      jfound=0
      do jfile=1,nfiles
         if(manyfiles) then
            write(chseed,'(i4)') jfile
            call blanktozero(chseed)
            inquire(file=mergelabels(pwgprefix(1:lprefix)//trim(label),postfix,
     1           chseed,'wdec.dat'),exist=lpresent)
            if(.not.lpresent) cycle
            open(unit=iun,file=mergelabels(pwgprefix(1:lprefix)//trim(label),postfix,
     1           chseed,'wdec.dat'),
     2           form='unformatted',status='old',iostat=ios)
            if(ios.ne.0) then
               iret=-1
               return
            else
               write(*,*)
     1              ' Opened ',mergelabels(pwgprefix(1:lprefix)//trim(label),postfix,
     2           chseed,'wdec.dat')
            endif
         endif

         filefound=.true.

         jfound = jfound + 1

         read(iun,iostat=ios) ndim,nind,nchannels
         if(ios.ne.0) goto 999

         if(jfound.eq.1) then
            call allocate_mintw(ptr,ndim,nind,nchannels)
         endif

         read(iun,iostat=ios) ptr%ifold
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%xgrid
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%wind
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%yindmax
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%yindmaxrat
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%ymax
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%ymaxrat
         if(ios.ne.0) goto 998

         read(iun,iostat=ios) ptr%nentries
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%tot
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%totabs
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%totpos
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%totneg
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%etot
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%etotabs
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%etotpos
         if(ios.ne.0) goto 998
         read(iun,iostat=ios) ptr%etotneg
         if(ios.ne.0) goto 998

         if(jfound.eq.1) then
            call getcurrentptrindex(id,icu)
            if(icu.gt.0) then
               write(*,*) ' loadgrids: id '//trim(id)//' already there!'
               write(*,*) ' exiting ...'
               call exit(-1)
            else
               mintw_nptrs = mintw_nptrs + 1
               icu= mintw_nptrs
               mintw_ptrs(icu)%id = id
            endif
            call allocate_mintw(mintw_ptrs(icu),ndim,nind,nchannels)

            firstfound = chseed
            mintw_ptrs(icu)%ifold = ptr%ifold 
            mintw_ptrs(icu)%xgrid = ptr%xgrid 
            mintw_ptrs(icu)%wind = ptr%wind
            mintw_ptrs(icu)%yindmax = ptr%yindmax
            mintw_ptrs(icu)%yindmaxrat = ptr%yindmaxrat
            mintw_ptrs(icu)%ymax = ptr%ymax
            mintw_ptrs(icu)%ymaxrat = ptr%ymaxrat
            mintw_ptrs(icu)%nentries = ptr%nentries
            mintw_ptrs(icu)%tot = ptr%tot
            mintw_ptrs(icu)%totabs = ptr%totabs
            mintw_ptrs(icu)%totpos = ptr%totpos
            mintw_ptrs(icu)%totneg = ptr%totneg
            mintw_ptrs(icu)%etot = ptr%etot
            mintw_ptrs(icu)%etotabs = ptr%etotabs
            mintw_ptrs(icu)%etotpos = ptr%etotpos
            mintw_ptrs(icu)%etotneg = ptr%etotneg
         else
            if(.not.all(ptr%xgrid.eq.mintw_ptrs(icu)%xgrid)) then
               write(*,*) ' error loading grids: '
               write(*,*)  pwgprefix(1:lprefix)//label//'-'//
     1              rnd_cwhichseed//'-wdec.dat does not have the same importance'
               write(*,*) 'sampling grid as ',pwgprefix(1:lprefix)
     1              //label//'-'//firstfound//'-wdec.dat'
               write(*,*) ' exiting ...'
               call exit(-1)
            endif
            if(.not.all(ptr%ifold.eq.mintw_ptrs(icu)%ifold)) then
               write(*,*) ' error loading grids: '
               write(*,*)  pwgprefix(1:lprefix)//label//'-'//
     1              rnd_cwhichseed//'-wdec.dat does not have the same folding as'
               write(*,*) pwgprefix(1:lprefix)
     1              //label//'-'//firstfound//'-wdec.dat'
               write(*,*) ' exiting ...'
               call exit(-1)
            endif
            mintw_ptrs(icu)%ymax = max(mintw_ptrs(icu)%ymax,ptr%ymax)
            mintw_ptrs(icu)%ymaxrat = max(mintw_ptrs(icu)%ymaxrat,ptr%ymaxrat)
            mintw_ptrs(icu)%yindmax = max(mintw_ptrs(icu)%yindmax,ptr%yindmax)
            mintw_ptrs(icu)%yindmaxrat = max(mintw_ptrs(icu)%yindmaxrat,ptr%yindmaxrat)

            call combineerrors(mintw_ptrs(icu)%tot,mintw_ptrs(icu)%etot,ptr%tot,ptr%etot)
            call combineerrors(mintw_ptrs(icu)%totabs,mintw_ptrs(icu)%etotabs,ptr%totabs,ptr%etotabs)
            call combineerrors(mintw_ptrs(icu)%totpos,mintw_ptrs(icu)%etotpos,ptr%totpos,ptr%etotpos)
            call combineerrors(mintw_ptrs(icu)%totneg,mintw_ptrs(icu)%etotneg,ptr%totneg,ptr%etotneg)

         endif
         close(iun)
      enddo
      if(filefound) then
         call deallocate_mintw(ptr)
      else
         iret = -1
      endif
      return
 998  continue
      call deallocate_mintw(ptr)
 999  continue
      iret=-1
      
      contains
         subroutine combineerrors(tot,etot,t,e)
         implicit none
         real *  8 tot,etot,t,e,rjfound,rnentries
         rjfound = jfound
         rnentries = ptr%nentries
         etot = sqrt( (etot**2*(rjfound-1)**2+e**2)/rjfound**2
     1        + (rjfound-1)*(tot-t)**2/(rjfound**3*rnentries) )
         tot = (tot*(rjfound-1)+t)/rjfound
         end subroutine
      end
ccc      
ccc        
ccc
ccc      subroutine getparallelparms(parallelstage,xgriditeration)
ccc      implicit none
ccc      include 'pwhg_par.h' 
ccc      include 'pwhg_rnd.h' 
ccc      integer parallelstage,xgriditeration
ccc      logical ini
ccc      integer pstage,xiteration
ccc      data ini/.true./
ccc      save ini,pstage,xiteration
ccc      real * 8 powheginput
ccc      external powheginput
ccc      if(ini) then
ccc         pstage = powheginput('#parallelstage')
ccc         xiteration = powheginput('#xgriditeration')
ccc         if(pstage.gt.0) then
ccc            if(xiteration.gt.par_maxxgriditerations) then
ccc               write(*,*) ' getparallelparms:'
ccc               write(*,*) ' POWHEG is compiled with a maximum'
ccc               write(*,*) ' number of x-grid iterations=',
ccc     1              par_maxxgriditerations
ccc               write(*,*) ' increase the par_maxxgriditerations parameter'
ccc               write(*,*) ' in the pwhg_par.h file to use more.'
ccc               write(*,*) ' (BUT YOU SHOULD NOT NEED MORE THAN 3 or 4!'
ccc               write(*,*) ' increase ncall1 instead)'
ccc               write(*,*) ' exiting ...'
ccc               call pwhg_exit(-1)
ccc            endif
ccc            if(xiteration.lt.1) then
ccc               write(*,*) ' getparallelparms:'
ccc               write(*,*) ' xgriditeration=',xiteration,' not allowed,'
ccc               write(*,*) ' exiting ...'
ccc               call pwhg_exit(-1)
ccc            endif
ccc            if(rnd_cwhichseed.eq.'none') then
ccc               write(*,*) ' getparallelparms:'
ccc               write(*,*) ' with parallelstage also manyseeds '
ccc               write(*,*) ' must be set'
ccc               write(*,*) ' exiting ...'
ccc               call pwhg_exit(-1)
ccc            endif
ccc         endif
ccc         ini = .false.
ccc      endif
ccc      parallelstage = pstage
ccc      xgriditeration = xiteration
ccc      end
ccc
ccc               
ccc         
ccc      subroutine blanktozero(string)
ccc      character *(*) string
ccc      integer l,k
ccc      l=len(string)
ccc      do k=1,l
ccc         if(string(k:k).eq.' ') string(k:k)='0'
ccc      enddo
ccc      end
ccc
ccc
ccc      subroutine doregrid(ptr)
ccc      implicit none
ccc      include 'pwhg_mintw.h'
ccc      type(mintw_pointer) :: ptr
ccc      integer kdim
ccc      do kdim=1,ptr%ndim
ccc         call regrid(ptr%xacc(:,kdim),ptr%xgrid(:,kdim),
ccc     1        ptr%nhits(:,kdim),kdim,nintervals)
ccc      enddo
ccc      if(ptr%nind.gt.1) then
ccc         call regridind(ptr%nind,ptr%accind,ptr%indhits,ptr%wind)
ccc      endif
ccc      end
ccc
ccc      
ccc      subroutine resettotals(ptr)
ccc      implicit none
ccc      include 'nlegborn.h'
ccc      include 'pwhg_mintw.h'
ccc      include 'pwhg_flst.h'
ccc      type (mintw_pointer) :: ptr
ccc      real * 8, pointer :: tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg
ccc      real * 8, pointer :: totj(:),totabsj(:),totposj(:),totnegj(:),
ccc     1     etotj(:),etotabsj(:),etotposj(:),etotnegj(:)
ccc      integer,  pointer ::  nentries
ccc      integer, pointer ::  nchannels
ccc      common/cmintotals/totj,totabsj,totposj,totnegj,etotj,etotabsj,etotposj,etotnegj,
ccc     1     tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg,nentries,nchannels
ccc      integer j
ccc      totj => ptr%totj
ccc      totabsj => ptr%totabsj
ccc      totposj => ptr%totposj
ccc      totnegj => ptr%totnegj
ccc      etotj    => ptr%etotj
ccc      etotabsj => ptr%etotabsj
ccc      etotposj => ptr%etotposj
ccc      etotnegj => ptr%etotnegj
ccc
ccc      tot => ptr%tot
ccc      totabs => ptr%totabs
ccc      totpos => ptr%totpos
ccc      totneg => ptr%totneg
ccc      etot    => ptr%etot
ccc      etotabs => ptr%etotabs
ccc      etotpos => ptr%etotpos
ccc      etotneg => ptr%etotneg
ccc
ccc      nchannels => ptr%nchannels
ccc      nentries => ptr%nentries
ccc      
ccc      nentries=0
ccc      tot=0
ccc      etot=0
ccc      totabs=0
ccc      etotabs=0
ccc      totpos=0
ccc      etotpos=0
ccc      totneg=0
ccc      etotneg=0
ccc      do j=1,nchannels
ccc         totj(j)=0
ccc         etotj(j)=0
ccc         totabsj(j)=0
ccc         etotabsj(j)=0
ccc         totposj(j)=0
ccc         etotposj(j)=0
ccc         totnegj(j)=0
ccc         etotnegj(j)=0
ccc      enddo
ccc      end
ccc
ccc      subroutine adduptotals(results,n)
ccc      implicit none
ccc      include 'nlegborn.h'
ccc      integer n
ccc      real * 8 results(n)
ccc      real * 8, pointer :: tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg
ccc      real * 8, pointer :: totj(:),totabsj(:),totposj(:),totnegj(:),
ccc     1     etotj(:),etotabsj(:),etotposj(:),etotnegj(:)
ccc      integer,  pointer ::  nentries
ccc      integer, pointer ::  nchannels
ccc      common/cmintotals/totj,totabsj,totposj,totnegj,etotj,etotabsj,etotposj,etotnegj,
ccc     1     tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg,nentries,nchannels
ccc      real * 8 dtot,dtotabs,dtotpos,dtotneg
ccc      integer j
ccc      nentries=nentries+1
ccc      dtot=0
ccc      dtotabs=0
ccc      dtotpos=0
ccc      dtotneg=0
ccc      do j=1,n
ccc         dtot=dtot+results(j)
ccc         dtotabs=dtotabs+abs(results(j))
ccc         if(results(j).gt.0) then
ccc            dtotpos=dtotpos+results(j)
ccc         else
ccc            dtotneg=dtotneg-results(j)
ccc         endif
ccc      enddo
ccc      tot=tot+dtot
ccc      totabs=totabs+dtotabs
ccc      totpos=totpos+dtotpos
ccc      totneg=totneg+dtotneg      
ccc      etot=etot+dtot**2
ccc      etotabs=etotabs+dtotabs**2
ccc      etotpos=etotpos+dtotpos**2
ccc      etotneg=etotneg+dtotneg**2
cccc j contributions
ccc      do j=1,n
ccc         dtot=results(j)
ccc         dtotabs=abs(results(j))
ccc         if(results(j).gt.0) then
ccc            dtotpos=results(j)
ccc         else
ccc            dtotneg=-results(j)
ccc         endif
ccc         totj(j)=totj(j)+dtot
ccc         totabsj(j)=totabsj(j)+dtotabs
ccc         totposj(j)=totposj(j)+dtotpos
ccc         totnegj(j)=totnegj(j)+dtotneg     
ccc         etotj(j)=etotj(j)+dtot**2
ccc         etotabsj(j)=etotabsj(j)+dtotabs**2
ccc         etotposj(j)=etotposj(j)+dtotpos**2
ccc         etotnegj(j)=etotnegj(j)+dtotneg**2
ccc      enddo
ccc      end
ccc
ccc      subroutine finaltotals
ccc      implicit none
ccc      include 'nlegborn.h'
ccc      include 'pwhg_flst.h'
ccc      real * 8, pointer :: tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg
ccc      real * 8, pointer :: totj(:),totabsj(:),totposj(:),totnegj(:),
ccc     1     etotj(:),etotabsj(:),etotposj(:),etotnegj(:)
ccc      integer,  pointer ::  nentries
ccc      integer, pointer ::  nchannels
ccc      common/cmintotals/totj,totabsj,totposj,totnegj,etotj,etotabsj,etotposj,etotnegj,
ccc     1     tot,totabs,totpos,totneg,etot,etotabs,etotpos,etotneg,nentries,nchannels
ccc      integer n,j,k
ccc      character * 80 format
ccc      real * 8 tmp_totj,tmp_etotj,tmp_totabsj,
ccc     1     tmp_etotabsj,tmp_totposj,tmp_etotposj,
ccc     2     tmp_totnegj,tmp_etotnegj
ccc      real * 8 tmp
ccc      integer iun
ccc      character * 20 pwgprefix
ccc      integer lprefix
ccc      common/cpwgprefix/pwgprefix,lprefix
ccc      real * 8 powheginput
ccc      external powheginput
ccc      n=nentries
ccc
ccc      etot=sqrt((etot/n-(tot/n)**2)/n)
ccc      etotabs=sqrt((etotabs/n-(totabs/n)**2)/n)
ccc      etotpos=sqrt((etotpos/n-(totpos/n)**2)/n)
ccc      etotneg=sqrt((etotneg/n-(totneg/n)**2)/n)
ccc
ccc      tot=tot/n
ccc      totabs=totabs/n
ccc      totpos=totpos/n
ccc      totneg=totneg/n
cccc
ccc      if(powheginput('#ubsigmadetails').eq.1) then
ccc         call newunit(iun)
ccc         open(iun,file=pwgprefix(1:lprefix)//'ubsigma.dat')
ccc         format='(      (i8,1x),4(a,1x,d10.4,a,d7.1))'
ccc         write(format(2:4),'(i3)') nlegborn
ccc         tmp=0
ccc         do j=1,flst_nborn
ccc            tmp_totj=totj(j)/n
ccc            tmp_etotj=sqrt((etotj(j)/n-(totj(j)/n)**2)/n)
ccc            tmp_totabsj=totabsj(j)/n
ccc            tmp_etotabsj=sqrt((etotabsj(j)/n-(totabsj(j)/n)**2)/n)
ccc            tmp_totposj=totposj(j)/n
ccc            tmp_etotposj=sqrt((etotposj(j)/n-(totposj(j)/n)**2)/n)
ccc            tmp_totnegj=totnegj(j)/n
ccc            tmp_etotnegj=sqrt((etotnegj(j)/n-(totnegj(j)/n)**2)/n)
ccc            write(iun,format) (flst_born(k,j),k=1,nlegborn),
ccc     1           'tot:',tmp_totj,' +- ',tmp_etotj,
ccc     2           '; abs:',tmp_totabsj,' +- ',tmp_etotabsj,
ccc     3           '; pos:',tmp_totposj,' +- ',tmp_etotposj,
ccc     4           '; neg:',tmp_totnegj,' +- ',tmp_etotnegj
ccc            tmp=tmp+tmp_totj
ccc         enddo
ccc         write(iun,*) tmp
ccc      endif
ccc      end
ccc
ccc
ccc
ccc      subroutine writestat(iunstat)
ccc      implicit none
ccc      integer iunstat
ccc      include 'pwhg_mintw.h'
ccc      integer icurrent
ccc      real * 8 totpos,totneg,tot,etotpos,etotneg,etot
ccc      totpos = 0
ccc      totneg = 0
ccc      tot = 0
ccc      etotpos = 0
ccc      etotneg = 0
ccc      etot = 0
ccc      
ccc      do icurrent=1,mintw_nptrs
ccc         write(*,*) mintw_ptrs(icurrent)%id
ccc     1   //' pos.   weights:', mintw_ptrs(icurrent)%totpos,' +- ',
ccc     2        mintw_ptrs(icurrent)%etotpos
ccc         totpos = totpos + mintw_ptrs(icurrent)%totpos
ccc         etotpos = sqrt(etotpos**2 + mintw_ptrs(icurrent)%etotpos**2 )
ccc         write(*,*) mintw_ptrs(icurrent)%id
ccc     1   //' |neg.| weights:', mintw_ptrs(icurrent)%totneg,' +- ',
ccc     2        mintw_ptrs(icurrent)%etotneg
ccc         totneg = totneg + mintw_ptrs(icurrent)%totneg
ccc         etotneg = sqrt(etotneg**2 + mintw_ptrs(icurrent)%etotneg**2 )
ccc         write(*,*) mintw_ptrs(icurrent)%id
ccc     1   //' total (pos.-|neg.|):', mintw_ptrs(icurrent)%tot,' +- ',
ccc     2        mintw_ptrs(icurrent)%etot
ccc         tot = tot + mintw_ptrs(icurrent)%tot
ccc         etot = sqrt(etot**2 + mintw_ptrs(icurrent)%etot**2 )
ccc
ccc         write(iunstat,*) mintw_ptrs(icurrent)%id
ccc     1   //' pos.   weights:', mintw_ptrs(icurrent)%totpos,' +- ',
ccc     2        mintw_ptrs(icurrent)%etotpos
ccc         write(iunstat,*) mintw_ptrs(icurrent)%id
ccc     1   //' |neg.| weights:', mintw_ptrs(icurrent)%totneg,' +- ',
ccc     2        mintw_ptrs(icurrent)%etotneg
ccc         write(iunstat,*) mintw_ptrs(icurrent)%id
ccc     1   //' total (pos.-|neg.|):', mintw_ptrs(icurrent)%tot,' +- ',
ccc     2        mintw_ptrs(icurrent)%etot
ccc      enddo
ccc
ccc      write(*,*) 'grand total'
ccc     1     //' pos.   weights:', totpos,' +- ',
ccc     2     etotpos
ccc      write(*,*) 'grand total'
ccc     1     //' |neg.| weights:', totneg,' +- ',
ccc     2     etotneg
ccc      write(*,*) 'grand total'
ccc     1     //' total (pos.-|neg.|):', tot,' +- ',
ccc     2     etot
ccc      
ccc      write(iunstat,*) 'grand total'
ccc     1     //' pos.   weights:', totpos,' +- ',
ccc     2     etotpos
ccc      write(iunstat,*) 'grand total'
ccc     1     //' |neg.| weights:', totneg,' +- ',
ccc     2     etotneg
ccc      write(iunstat,*) 'grand total'
ccc     1     //' total (pos.-|neg.|):', tot,' +- ',
ccc     2     etot
ccc      
ccc      end
