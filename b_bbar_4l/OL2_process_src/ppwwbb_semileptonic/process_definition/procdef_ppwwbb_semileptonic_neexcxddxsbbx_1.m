
FeynArtsProcess = {F[1, {1}], -F[2, {1}]} -> {F[3, {2}], -F[4, {1}], F[4, {1}], -F[4, {2}], -F[4, {3}], F[4, {3}]};

SortExternal = True;

OpenLoopsModel = "SM";

CreateTopologiesOptions = {
  ExcludeTopologies -> {Snails, WFCorrectionCTs, TadpoleCTs, Loops[1 | 2 | 4 | 5 | 6 | 7 | 8]},
  Adjacencies -> {3, 4}
};

InsertFieldsOptions = {
  Model -> {"SMQCD", "SMQCDR2"},
  GenericModel -> "Lorentz",
  InsertionLevel -> {Particles},
  Restrictions -> {ExcludeParticles -> {S[2 | 3]}, NoQuarkMixing}
};

UnitaryGauge = True;

ColourCorrelations = False;

OTFColourCorrelations = Automatic;

SpinCorrelatedHardFactor = Automatic;

SubProcessName = Automatic;

SelectCoupling = MemberQ[{2, 4, 6}, Exponent[#1, eQED]] & ;

SelectInterference = {
  eQED -> {8}
};

SelectTreeDiagrams = HasResonance[V[3], {1, 2}][##1] && HasResonance[-V[3], {3, 6}][##1] & ;

SelectLoopDiagrams = True & ;

SelectCTDiagrams = True & ;

ReplaceOSw = False;

SetParameters = {
  ME -> 0,
  MM -> 0,
  CKMORDER -> 0,
  nc -> 3,
  nf -> 6,
  MU -> 0,
  MD -> 0,
  MS -> 0,
  MC -> 0,
  LeadingColour -> 0,
  POLSEL -> 1
};

ChannelMap = {};

Approximation = "pole_ww";

QED = 0;

ForceLoops = Automatic;

ForceLoopsInclude = Automatic;

NonZeroHels = Null;

OnTheFlyMode = Automatic;
