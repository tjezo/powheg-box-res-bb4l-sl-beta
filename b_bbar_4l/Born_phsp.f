      subroutine born_phsp(xborn)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      include 'pwhg_flst.h'
      include 'pwhg_physpar.h'
      include 'PhysPars.h'
      integer iborn,j
      integer flav(12),res(12)
      data flav/ 0,  0,  6, -6, 24, -24, -11, 12, 13, -14, 5, -5/
      data res / 0,  0,  0,  0,  3,   4,   5,  5,  6,   6, 3,  4/
      real * 8 p(0:3,12),cmp(0:3,12),masses(12),xborn(*)
      logical, save:: ini=.true., nores
      save flav,res,masses
      real * 8 powheginput

      if(ini) then
         if(powheginput("#nores") == 1) then
            nores = .true.
            do j=1,12
               masses(j) = physpar_phspmasses(flav(j))
            enddo
         else
            nores = .false.
         endif
         ini = .false.
      endif

      if(nores) then
         call genphasespace(xborn,12,flav,res,kn_beams,kn_jacborn,
     1     kn_xb1,kn_xb2,kn_sborn,cmp,p)

         kn_cmpborn(:,1:2) = cmp(:,1:2)
         kn_cmpborn(:,3:8) = cmp(:,7:12)
         
         kn_pborn(:,1:2) = p(:,1:2)
         kn_pborn(:,3:8) = p(:,7:12)

c     Make sure flst_ibornlength is set
         do iborn=1,flst_nborn
            if(flst_bornresgroup(iborn).eq.flst_ibornresgroup) exit
         enddo

         flst_ibornlength = flst_bornlength(iborn)
         flst_ireallength = flst_ibornlength + 1

         do j=1,flst_ibornlength
            kn_masses(j) = physpar_phspmasses(flst_born(j,iborn))
         enddo
         
         kn_minmass = 2*ph_bmass
      else
c     compute resonance weights for this group
         do iborn=1,flst_nborn
            if(flst_bornresgroup(iborn).eq.flst_ibornresgroup) exit
         enddo
         
         flst_ibornlength = flst_bornlength(iborn)
         flst_ireallength = flst_ibornlength + 1
         
         do j=1,flst_ibornlength
            kn_masses(j) = physpar_phspmasses(flst_born(j,iborn))
         enddo
         
         call genphasespace(xborn,flst_ibornlength,flst_born(:,iborn),
     1        flst_bornres(:,iborn),kn_beams,kn_jacborn,
     1        kn_xb1,kn_xb2,kn_sborn,kn_cmpborn,kn_pborn)
         
         kn_minmass = 2*ph_bmass
      endif
      end

      subroutine born_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end

      subroutine rmn_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end


      subroutine regular_suppression(fact)
      implicit none
      real * 8 fact
      call rmn_suppression(fact)
      end


      subroutine global_suppression(c,fact)
      implicit none
      character * 1 c
      real * 8 fact
      fact=1d0
      end



      subroutine set_fac_ren_scales(muf,mur)
      implicit none
      include 'PhysPars.h'
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'pwhg_kn.h'
      real * 8 muf,mur
      logical, save :: ini=.true., nores
      real *8 dotp,powheginput,mu0,mutb,mutr,muatb,muatr
      real *8 mubb,mubr,muabb,muabr
      real * 8 ptr(0:3), ptb(0:3), patb(0:3), patr(0:3)
      external dotp,powheginput
      integer scalechoice,j
      logical fixedscale,offshellscales
      save fixedscale,scalechoice,offshellscales
      if (ini) then
         if(powheginput("#fixedscale").le.0) then
            fixedscale=.false.
         else
            fixedscale=.true.
         endif
         write(*,*) '*****************************************'
         if(fixedscale) then
            write(*,*) ' mu0 = mt '
         else
            write(*,*) ' mu0= dynamic scale'
         endif
         write(*,*) ' with muf=mur=mu0'
         if(powheginput("#nores") == 1) then
            nores = .true.
         else
            nores = .false.
         endif
         ini=.false.
      endif
      if(fixedscale) then
         mu0 = ph_tmass
      else
         mutb = 0
         muatb = 0
         mubb = 0
         muabb = 0
         mutr = 0
         muatr = 0
         mubr = 0
         muabr = 0
         if (nores) then
            ptb  = kn_cmpborn(:,3)+kn_cmpborn(:,4)+kn_cmpborn(:,7)
            ptr  = kn_cmpreal(:,3)+kn_cmpreal(:,4)+kn_cmpreal(:,7)
            patb = kn_cmpborn(:,5)+kn_cmpborn(:,6)+kn_cmpborn(:,8)
            patr = kn_cmpreal(:,5)+kn_cmpreal(:,6)+kn_cmpreal(:,8)
            mutb  = sqrt( ptb(0)**2  - ptb(3)**2)
            mutr  = sqrt( ptr(0)**2  - ptr(3)**2)
            muatb = sqrt( patb(0)**2 - patb(3)**2)
            muatr = sqrt( patr(0)**2 - patr(3)**2)
         else 
            do j=3,flst_ibornlength
               if(flst_borncurr(j) == 6) then
                  mutb  =  sqrt( kn_cmpborn(0,j)**2
     1                         - kn_cmpborn(3,j)**2)
                  mutr  =  sqrt( kn_cmpreal(0,j)**2
     1                         - kn_cmpreal(3,j)**2)
               elseif(flst_borncurr(j) == -6) then
                  muatb =  sqrt( kn_cmpborn(0,j)**2
     1                         - kn_cmpborn(3,j)**2)
                  muatr =  sqrt( kn_cmpreal(0,j)**2
     1                         - kn_cmpreal(3,j)**2)
               elseif(flst_borncurr(j) == 5) then
                  mubb  =  sqrt( kn_cmpborn(0,j)**2
     1                         - kn_cmpborn(3,j)**2)
                  mubr  =  sqrt( kn_cmpreal(0,j)**2
     1                         - kn_cmpreal(3,j)**2)
               elseif(flst_borncurr(j) == -5) then
                  muabb =  sqrt( kn_cmpborn(0,j)**2
     1                         - kn_cmpborn(3,j)**2)
                  muabr =  sqrt( kn_cmpreal(0,j)**2
     1                         - kn_cmpreal(3,j)**2)
               endif
            enddo
         endif
         if(flg_btildepart.eq.'b'.or.flg_btildepart.eq.'c') then
            if (mutb > 0 .and. muatb > 0) then
               mu0 = sqrt(mutb*muatb)
            else if (mutb > 0 .and. muabb > 0) then
               mu0 = sqrt(mutb*muabb)
            else if (muatb > 0 .and. mubb > 0) then
               mu0 = sqrt(mubb*muatb)
            else 
               call no_scales
            endif
         elseif(flg_btildepart.eq.'r') then
            if (mutr > 0 .and. muatr > 0) then
               mu0 = sqrt(mutr*muatr)
            else if (mutr > 0 .and. muabr > 0) then
               mu0 = sqrt(mutr*muabr)
            else if (muatr > 0 .and. mubr > 0) then
               mu0 = sqrt(mubr*muatr)
            else 
               call no_scales
            endif
         else
c the only remaining possibility is 'R' for regulars. Shouldn't have any here ...
            write(*,*) ' set_fac_ren_scales: flg_btildepart=', flg_btildepart
            write(*,*) ' but there are no regulars here ... exiting ...'
            call exit(-1)
         endif
      endif
      muf = mu0
      mur = mu0
      contains
      subroutine no_scales
      write(*,*) " set_fac_ren_scales: didn't find a W!"
      write(*,*) "flavours:",flst_borncurr(1:flst_ibornlength)
      write(*,*) "flg_btildepart=",flg_btildepart
      write(*,*) "mutb, mutr, muatb, muatr, mubb, mubr, muabb, muabr ",
     1     mutb,mutr,muatb,muatr,mubb,mubr,muabb,muabr
      write(*,*) " exiting ..."
      call pwhg_exit(-1)
      end subroutine no_scales
      end

